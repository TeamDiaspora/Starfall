if E2Helper then
	E2Helper.Descriptions["plyPrint"] = "Prints a string to a player's Chat Box.\nFor Type: c"
	E2Helper.Descriptions["plyHint"] = "Displays a hint popup with message <text> for <duration> seconds (<duration> being clamped between 0.7 and 7)."
	E2Helper.Descriptions["plyPrintSimpleColor"] = "Prints a string to a player's Chat Box with the one desired color."
end

usermessage.Hook("wire_expression2_nielk1_plyPrintSimpleColor", function( um )
    local textLine = um:ReadString()
	local colorR = um:ReadChar()+128
	local colorG = um:ReadChar()+128
	local colorB = um:ReadChar()+128
	
    chat.AddText(Color(colorR, colorG, colorB, 255), textLine)
end)