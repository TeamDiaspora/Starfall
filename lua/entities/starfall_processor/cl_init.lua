include( "shared.lua" )

ENT.RenderGroup = RENDERGROUP_OPAQUE

function ENT:GetOverlayText()
    local state = self:GetNWInt("State", 1)
    local clientstr, serverstr

    if self.instance and not self.instance.error then
        clientstr = tostring(self.CPUus) .. "us. (" .. tostring(self.CPUpercent) .. "%)"
    else
        clientstr = "Errored"
    end

    if state == 1 then
        serverstr = tostring(self:GetNWInt("CPUus", 0)) .. "us. (" .. tostring(self:GetNWFloat("CPUpercent", 0)) .. "%)"
    elseif state == 2 then
        serverstr = "Errored"
    end

    if serverstr then
        return "- Starfall Processor -\n[ " .. (self.name or "Generic ( No-Name )") .. " ]\nServer CPU: " .. serverstr .. "\nClient CPU: " .. clientstr
    else
        return "(None)"
    end
end

function ENT:CodeSent(ply, files, mainfile)
    self.BaseClass.CodeSent(self, ply, files, mainfile)
    if not self.instance then return end
    self.name = nil

    if self.instance.ppdata.scriptnames and self.instance.mainfile and self.instance.ppdata.scriptnames[self.instance.mainfile] then
        self.name = tostring(self.instance.ppdata.scriptnames[self.instance.mainfile])
    end
end

function ENT:Draw ()
    self.BaseClass.Draw( self )
    self:DrawModel()
    if self:BeingLookedAtByLocalPlayer() then
        AddWorldTip( self:EntIndex(), self:GetOverlayText(), 0.5, self:GetPos(), self )
    end
end
