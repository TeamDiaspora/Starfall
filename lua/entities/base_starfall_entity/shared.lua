DEFINE_BASECLASS("base_gmodentity")
ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.Author = "Radon"
ENT.Contact = ""
ENT.Purpose = ""
ENT.Instructions = ""
ENT.Spawnable = false
ENT.AdminSpawnable = false

ENT.States = {
    Normal = 1,
    Error = 2,
    None = 3,
}

function ENT:Error(msg, traceback)
    if type(msg) == "table" then
        if msg.message then
            local line = msg.line
            local file = msg.file
            msg = (file and (file .. ":") or "") .. (line and (line .. ": ") or "") .. msg.message
        end
    end

    msg = tostring(msg)

    if SERVER then
        ErrorNoHalt("Processor of " .. self.owner:Nick() .. " errored: " .. msg .. "\n")
    end

    SF.AddNotify(self.owner, msg, NOTIFY_ERROR, 7, NOTIFYSOUND_ERROR1)

    if self.instance then
        self.instance:deinitialize()
        self.instance = nil
    end

    if traceback then
        if SERVER then
            self:SetNWInt("State", self.States.Error)
            SF.Print(self.owner, traceback)
        else
            print(traceback)
        end
    end

    return msg
end

function ENT:runScriptHook(hook, ...)
    if self.instance and not self.instance.error and self.instance.hooks[hook:lower()] then
        local ok, rt = self.instance:runScriptHook(hook, ...)

        if not ok then
            self:Error(rt)
        else
            return rt
        end
    end
end

function ENT:runScriptHookForResult(hook, ...)
    if self.instance and not self.instance.error and self.instance.hooks[hook:lower()] then
        local ok, rt = self.instance:runScriptHookForResult(hook, ...)

        if not ok then
            self:Error(rt)
        else
            return rt
        end
    end
end