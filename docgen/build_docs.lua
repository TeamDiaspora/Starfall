
require "luadoc"

local outputdir = arg[1] or "../doc/"
local sourcecode = arg[2] or "../lua/starfall"

luadoc.main({sourcecode}, {
	output_dir = outputdir,
	basepath = sourcecode,
	--template_dir = "luadoc/doclet/html/",
	nomodules = false,
	nofiles = true,
	verbose = true,
	taglet = "tagletsf",
	doclet = "doclet_dump",
})

luadoc.main({sourcecode}, {
	output_dir = outputdir,
	basepath = sourcecode,
	--template_dir = "luadoc/doclet/html/",
	nomodules = false,
	nofiles = true,
	verbose = true,
	taglet = "tagletsf",
	doclet = "doclet_json",
})

return luadoc.main({sourcecode}, {
	output_dir = outputdir,
	basepath = sourcecode,
	--template_dir = "luadoc/doclet/html/",
	nomodules = false,
	nofiles = true,
	verbose = true,
	taglet = "tagletsf",
	doclet = "docletsfhtml",
})