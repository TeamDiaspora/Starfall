-------------------------------------------------------------------------------
-- Shared Life Support functions
-------------------------------------------------------------------------------

--- Life Support Library, Does Space Stuff
-- @shared
local ls_library, _ = SF.Libraries.Register( "ls" )

--- Entity type
--@class class
--@name Entity
local ents_methods = SF.Entities.Methods
local ewrap, eunwrap = SF.Entities.Wrap, SF.Entities.Unwrap
local vunwrap = SF.UnwrapObject

--- This returns true if entity is a node(Ship core).
function ents_methods:IsNode()
    local ent = eunwrap(self)
    if IsValid(ent) and ent.IsNode then return true end
    return false
end

--- This returns true if entity is any LS device.
function ents_methods:IsLSDevice()
    local ent = eunwrap(self)
    if IsValid(ent) and ent.IsLSEnt then return true end
    return false
end

--- Gets the maximum amount of a resource in a network.
-- @param Resource The name of the resource as a string.
function ents_methods:LSCapacity(ResourceName)
    local ent = eunwrap(self)
    if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player and ent.IsLSEnt then
        return ent:GetMaxAmount(ResourceName)
    end
end

--- Gets the amount of a resource in a network
-- @param Resource The resource name as a string
function ents_methods:LSAmount(Resource)
    local ent = eunwrap(self)
    if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player and ent.IsLSEnt then 
        return ent:GetAmount(Resource)
    end
end

--- Returns the (normally) human readable name of the LS Device.
function ents_methods:LSDeviceName()
    local ent = eunwrap(self)
    if IsValid(ent) then
        return ent.GeneratorInfo and ent.GeneratorInfo:GetName() or (ent.PrintName or "") --mother of god i wrote a brandon code HELP ME IM BEING ASSIMILATED
    end
    return ""
end

--- Returns a table of Resource names that the network can store as strings.
-- @param TypeFilter If this is passed then it will filter the returned resources by type
function ents_methods:LSResourceList(Type)
    local ent = eunwrap(self)
    local FoundResources = {}
    if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player and ent.IsLSEnt then
        for StorageType, Storage in pairs(ent:GetStorage()) do
            if (not Type) or (StorageType == Type) then
                for Name in pairs(Storage:GetStored()) do
                    table.insert(FoundResources, Name)
                end
            end
        end
    end

    return FoundResources
end

--- Returns a table of all entities on a network.
-- must be a core.
function ents_methods:LSNetEntities()
    local ent = eunwrap(self)
    local ret = {}
    if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player and ent.IsNode then
        for k,v in pairs(ent:GetLinks()) do
            table.insert(ret,ewrap(v))
        end
    end

    return ret
end

--- Returns a table of the unique IDs of all logistics nodes on a network
-- Must be a core
function ents_methods:LogisticsNodes()
	local Ent = eunwrap(self)
	local Nodes = {}
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return Nodes end	--How dare you try to use an outdated version of SC2!

		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if IsValid(LogisticsManager) then
			for NodeID, Node in pairs(LogisticsManager:GetNodes()) do
				if Node:GetProtector() == Ent then
					table.insert(Nodes, NodeID)
				end
			end
		end
	end

	return Nodes
end

--- Gets the type of a logistics node
-- @param NodeID The unique ID of the logistics node to get the type from
function ents_methods:LogisticsNodeType(NodeID)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			return Node:GetType()
		end
	end
end

--- Add a demand to a logistics node
-- @param NodeID The unique ID of the logistics node to apply the demand to
-- @param DemandName The name of the demand group to add this demand to
-- @param ResourceName The name of the resource to add a demand for
-- @param ResourceQuantity The quantity of the resource to add a demand for
-- @param ResourcePriority The priority of the demand entry
function ents_methods:LogisticsSetDemand(NodeID, DemandName, ResourceName, ResourceQuantity, ResourcePriority)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			Node:AddDemand(DemandName, ResourceName, ResourceQuantity, ResourcePriority)
		end
	end
end

--- Add a supply to a logistics node
-- @param NodeID The unique ID of the logistics node to apply the supply to
-- @param SupplyName The name of the supply group to add this supply to
-- @param ResourceName The name of the resource to add a supply for
-- @param ResourceQuantity The quantity of the resource to add a supply for
-- @param ResourcePriority The priority of the supply entry
function ents_methods:LogisticsSetSupply(NodeID, SupplyName, ResourceName, ResourceQuantity, ResourcePriority)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			Node:AddSupply(SupplyName, ResourceName, ResourceQuantity, ResourcePriority)
		end
	end
end

--- Remove a demand from a logistics node
-- @param NodeID The unique ID of the logistics node to remove the demand from
-- @param DemandName The name of the demand group to remove this demand from
-- @param ResourceName The name of the resource to remove the demand for
function ents_methods:LogisticsRemoveDemand(NodeID, DemandName, ResourceName)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			Node:RemoveDemand(DemandName, ResourceName)
		end
	end
end

--- Remove a supply from a logistics node
-- @param NodeID The unique ID of the logistics node to remove the supply from
-- @param SupplyName The name of the supply group to remove this supply from
-- @param ResourceName The name of the resource to remove from the supply group's list
function ents_methods:LogisticsRemoveSupply(NodeID, SupplyName, ResourceName)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			Node:RemoveSupply(SupplyName, ResourceName)
		end
	end
end

--- Get a list of demands from the logistics node
-- @param NodeID The unique ID of the logistics node
-- @param GroupName The name of the demand group to list demands for
function ents_methods:LogisticsGetDemands(NodeID, GroupName)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			return SF.Sanitize(Node:GetDemand())
		end
	end
end

--- Get a list of supplies from the logistics node
-- @param NodeID The unique ID of the logistics node
-- @param GroupName The name of the supply group to list supplies for
function ents_methods:LogisticsGetSupplies(NodeID, GroupName)
	local Ent = eunwrap(self)
	if IsValid(Ent) and SF.Entities.GetOwner(Ent) == SF.instance.player and Ent.IsNode then
		if not GAMEMODE.GetLogisticsManager then return end
		local LogisticsManager = GAMEMODE:GetLogisticsManager()
		if not IsValid(LogisticsManager) then return end

		local Node = LogisticsManager:GetNodes()[NodeID]
		if IsValid(Node) and Node:GetProtector() == Ent then
			return SF.Sanitize(Node:GetSupply())
		end
	end
end

--- Returns the Node Radius (the max range that you can link a thing)
-- must be a core.
function ents_methods:LSNodeSize()
    local ent = eunwrap(self)
    if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player and ent.IsNode then
        return ent:GetNodeRadius()
    end
    return false
end

--- Consumes a Resource on a network.(adminge only)
-- @param Resource Name of the resource as a string.
-- @param Amount how much to drain (cannot be negative). 
function ents_methods:ConsumeResource(Resource, Amount)
    local ent = eunwrap(self)
    if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player and SF.instance.player.IsAdmin() and ent.IsLSEnt then
        ent:ConsumeResource(Resource, math.abs(Amount))
    end
end

--- Returns a table of the planets as entities.
-- With the keys being planet names, Mabye.
function ls_library.GetPlanets()
	local planets = {}
	for k,v in pairs(GAMEMODE.AtmosphereTable) do
		planets[k] = ewrap(v:GetCelestial():getEntity())
	end
	
	return planets
end

local function GetEnv(ent)
	if not IsValid(ent) then return nil end
	
	--Is an Environment
	if ent:GetClass() == "sc_planet" then
		return ent:GetCelestial():GetEnvironment()
		
	--Get Environment Reference
	else
		if ent.GetEnvironment then return ent:GetEnvironment() end
		
		return nil
	end
end

--- Returns the name of the LS Enviroment The entity is in.
-- returns Space if not in one.
-- if used on planet returns info about that planet.
function ents_methods:LSName()
    local ent = eunwrap(self)
    if IsValid(ent) then
        local env = GetEnv(ent)
        if env then return env:GetName() end
    end
    return "Space"
end

--- Gets the size of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
function ents_methods:LSSize()
    local ent = eunwrap(self)
	if not IsValid(ent) then return 0 end

	local env = GetEnv(ent)
	if env and env:GetName() ~= "Space" then
		return env:GetRadius()
	else
		return math.huge
	end
	return 0
end

--- Returns the temperature of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
function ents_methods:LSTemperature()
    local ent = eunwrap(self)
	if not IsValid(ent) then return 0 end
	
	local env = GetEnv(ent)
	if env then
		return env:GetTemperature()
	end
	
	return 0
end

--- Returns the gravity of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
function ents_methods:LSGravity()
    local ent = eunwrap(self)
	if not IsValid(ent) then return 0 end
	
	local env = GetEnv(ent)
	if env then
		return env:GetGravity()
	end
	
	return false
end

--- Returns the pressure of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
function ents_methods:LSPressure()
    local ent = eunwrap(self)
	if not IsValid(ent) then return 0 end
	
	local env = GetEnv(ent)
	if env then
		return env:GetPressure()
	end
	
	return 0
end

--- Returns the volume of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
function ents_methods:LSVolume()
    local ent = eunwrap(self)
	if not IsValid(ent)then return 0 end
	
	local env = GetEnv(ent)
	if env and env:GetName() ~= "Space" then
		return env:GetVolume()
	end

    return math.huge
end

--- Returns a table of resources of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
-- with resource names as strings. 
function ents_methods:LSEnvResources()
    local ent = eunwrap(self)
	if not IsValid(ent) then return {} end
	
	local env = GetEnv(ent)
	if env then
		local resources = env:GetResources()
		local resReturn = {}
		for k,v in pairs(resources) do
			table.insert(resReturn, v:GetName())
		end
		
		return resReturn
	end
	
	return {}
end

--- Returns the amount of a resource of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
-- @param res Resource name as string. 
function ents_methods:LSResAmount(res)
    local ent = eunwrap(self)
	if not IsValid(ent) then return -1 end
	
	local env = GetEnv(ent)
	if env then
		return env:GetResourceAmount(res)
	end
	
	return -1
end

--- Returns the maximum amount of a resource of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
-- @param res Resource name as string. 
function ents_methods:LSResMaxAmount(res)
    local ent = eunwrap(self)
	if not IsValid(ent) then return -1 end
	
	local env = GetEnv(ent)
	if env then
		return env:GetResource(res):GetMaxAmount()
	end
	
	return -1
end

---Returns the sum total of all resources of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
-- fuhrballs magic number (/ama/a m a/that guy who made gooniverse/what ever the fuck his name is now)
function ents_methods:LSResTotal()
    local ent = eunwrap(self)
    if not IsValid(ent) then return -1 end
	
	local env = GetEnv(ent)
	if env then
		return env:GetResTotal()
	end
	
	return -1
end

--- Returns the percentage of a resource compared to the total of the LS Enviroment the entity is in. 
-- if used on planet returns info about that planet.
function ents_methods:LSResPercent(res) --Aquire fuhrballs magic number
    local ent = eunwrap(self)
	if not IsValid(ent) then return -1 end
	
	local env = GetEnv(ent)
	if env then
		local res1 = env:GetResource(res)
		if res1 then
			return res1:GetAmount() / env:GetResTotal()
		end
	end
	return -1
end

--- Returns the celestial entity the entity is in.
function ents_methods:LSGetEnvironment()
    local ent = eunwrap(self)
	if not IsValid(ent) then return nil end
	
	local env = GetEnv(ent)
	if env then
		local celestial = env:GetCelestial()
		if celestial then
			return ewrap(celestial:getEntity())
		end
	end
	
	return nil
end

--- Returns a directional vector of where to point your solars for that sunnyD.
function ls_library.LSGetSunDirection()
	return GAMEMODE:GetSunDir()
end

--- Tells space combat to not apply its custom gravity to an entity
-- @param Bool Overrides SC Gravity if true. 
function ents_methods:SBSetGravityOverride(Bool)
    local ent = eunwrap(self)
	if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player then 
	    ent.HasGravityOverride = Bool
    end
end

--- Returns if the entity has had SBSetGravityOverride set on it. 
function ents_methods:SBGetGravityOverride()
    local ent = eunwrap(self)
	if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player then 
        return ent.HasGravityOverride
    end
end

--- Tells space combat to not apply its custom drag to an entity
-- @param Bool Overrides SCDrag if true. 
function ents_methods:SBSetDragOverride(Bool)
    local ent = eunwrap(self)
	if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player then 
	    ent.HasDragOverride = Bool
    end
end

--- Returns if the entity has had SBSetDragOverride set on it.
function ents_methods:SBGetDragOverride()
    local ent = eunwrap(self)
	if IsValid(ent) and SF.Entities.GetOwner(ent) == SF.instance.player then 
	    return ent.HasDragOverride
    end
end

--- Returns the amount of life support in the players suit. (as a 0 - 100 percentage) 
function ents_methods:LSSuitLifesupport()
    local ent = eunwrap(self)
	if IsValid(ent) and ent.SB4Suit then
		return (ent.SB4Suit:GetIntake() / ent.SB4Suit:GetMaxIntake())*100
	end
end

--============================================
--STEEB WHAT THE SHIT ARE THOES PUMP FUNCTIONS
--LIKE WHAT? I ANT DOIN THAT AWW HELL NAW
--STEEB PLS.

--I'M HANDLING THIS SHIT FOR YOU WHINY BABIES GODDAMN
local function isPump(ent)
	if not IsValid(ent) then return false end
	
	return (ent:GetClass() == "sc_resource_pump") or (ent:GetClass() == "sc_resource_pump_global")
end

--- Gets the plug associated with this resource pump.
function ents_methods:LSPumpGetPlug()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return nil end
	
	return ewrap(ent:GetPlug())
end

--- Undocks the plug associated with this resource pump from its socket.
function ents_methods:LSPumpUndockPlug()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return false end
	if not IsValid(ent:GetPlug()) then
		ent:SpawnPlug()
	else
		ent:GetPlug():DisconnectFromSocket()
	end
	
	ent:GetPlug():DisconnectFromSocket()
	return true
end

--- Disconnects this resource pump from any pump it is currently connected to.
function ents_methods:LSPumpDisconnect(both)
	local ent = eunwrap(self)
	
	if not isPump(ent) then return false end
	
	ent:Disconnect(both)
	return true
end

--- Disconnects this resource pump from the pump it is connected to
function ents_methods:LSPumpDisconnectOutbound()
	local ent = eunwrap(self)

	if not isPump(ent) then return false end
	if not IsValid(ent:GetOutboundPump()) then return false end

	ent:Disconnect()

	return true
end

--- Disconnects this pump from the pump connected to it
function ents_methods:LSPumpDisconnectInbound()
	local ent = eunwrap(self)

	if not isPump(ent) then return false end
	if not IsValid(ent:GetInboundPump()) then return false end

	ent:GetInboundPump():Disconnect()

	return true
end

--- Gets the Pump that this Pump is connected to.
function ents_methods:LSPumpGetOutboundConnection()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return nil end
	
	return ewrap(ent:GetOutboundPump())
end

--- Gets the Pump that's connected inbound to this Pump.
function ents_methods:LSPumpGetInboundConnection()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return nil end
	
	return ewrap(ent:GetInboundPump())
end

--- Gets the transfer speed of this resource pump, in resource units per second.
function ents_methods:LSPumpGetTransferSpeed()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return -1 end
	
	return ent:GetTransferSpeed()
end

--- Sets the transfer speed of this resource pump, in resource units per second.
function ents_methods:LSPumpSetTransferSpeed(speed)
	local ent = eunwrap(self)
	
	if not isPump(ent) then return false end
	
	ent:SetDesiredTransferSpeed(speed)
	
	return true
end

--- Adds a task to the Queue of this resource pump.
-- @param amount If set to -1, task will send resources to the outbound pump continuously until removed.
function ents_methods:LSPumpAddTask(resourceName, amount)
	local ent = eunwrap(self)
	
	if amount ~= amount then return false end --NAN check
	if not isPump(ent) then return false end
	if not IsValid(ent:GetOutboundPump()) then return false end
	
	return ent:AddTask(resourceName, amount)
end

--- Removes the task with the provided index from the Queue of this resource pump.
function ents_methods:LSPumpRemoveTask(taskIndex)
	local ent = eunwrap(self)
	
	if not isPump(ent) then return false end
	
	ent:RemoveTask(taskIndex)
	return true
end

--- Returns the task with the provided index as a table.
--- Table variables are Resource (String), Amount (Number), Output(Boolean)
--- If Output is true, task is sending resources to the connected pump, if false, task is receiving resources from the connected pump.
function ents_methods:LSPumpGetTask(taskIndex)
	local ent = eunwrap(self)
	
	if not isPump(ent) then return {} end
	
	--Get Current Task
	local task = {}
	if taskIndex == 0 then
		task = ent:GetCurrentTask()
	else
		task = ent:GetQueue()[taskIndex]
	end
	
	return task
end

--- Gets how many tasks are left in this resource pump's queue.
function ents_methods:LSPumpGetQueueLength()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return -1 end
	
	return ent:GetQueueLength()
end

--- Gets the progress of the current task as a percentage.
function ents_methods:LSPumpTaskProgress()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return -1 end
	
	local task = ent:GetCurrentTask()
	if task then
		return task.Progress
	end
	
	return 0
end

--- Gets the complete table of tasks for this pump
function ents_methods:LSPumpGetTasks()
	local ent = eunwrap(self)
	
	if not isPump(ent) then return {} end
	
	return SF.Sanitize(ent:GetQueue())
end

-- Reactor functions
--- Returns true if the entity is a reactor
function ents_methods:LSReactorIsReactor()
	local ent = eunwrap(self)

	return IsValid(ent) and ent.IsSCReactor
end

--- Returns true if the reactor entity is an auxiliary reactor
function ents_methods:LSReactorIsAuxiliary()
	local ent = eunwrap(self)

	return ent.IsSCReactor and ent:GetIsAuxiliary()
end

--- Returns the size of the reactor
function ents_methods:LSReactorGetSize()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return ent:GetReactorSize()
	end

	return 0
end

--- Returns the efficient temperature for the current reaction
function ents_methods:LSReactorGetMaxTemperature()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		
		return ent.MaxTemperature
	end

	return 0
end

--- Returns the current temperature of the reactor
function ents_methods:LSReactorGetTemperature()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return ent:GetTemperature()
	end

	return 0
end

--- Returns the current coolant ID used in the reactor
function ents_methods:LSReactorGetCoolantID()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return ent:GetReactorCoolant()
	end

	return 0
end

--- Returns the current reaction ID used in the reactor
function ents_methods:LSReactorGetReactionID()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return ent:GetReactorReaction()
	end

	return 0
end

--- Returns the current reactor ID of the reactor
function ents_methods:LSReactorGetReactorID()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return ent:GetReactorType()
	end

	return 0
end

--- Returns the information for the reactor's current reaction
function ents_methods:LSReactorGetReaction()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return SF.Sanitize(SC.Reactors.GetReactionTypes()[SC.Reactors.GetReactorTypes()[ent:GetReactorType()]][ent:GetReactorReaction()])
	end

	return {}
end

--- Returns the information for the reactor's current coolant
function ents_methods:LSReactorGetCoolant()
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		return SF.Sanitize(SC.Reactors.GetCoolantTypes()[ent:GetReactorCoolant()])
	end

	return {}
end

--- Reconfigures the reactor to run off of a different coolant. Returns true if the coolant was changed successfully.
function ents_methods:LSReactorSetCoolant(CoolantID)
	local ent = eunwrap(self)

	if IsValid(ent) and ent.IsSCReactor then
		ent:SetReactorCoolant(CoolantID or 1)

		return ent:GetReactorCoolant() == CoolantID
	end

	return false
end

--#region Refinery Control

---Get the type of an sc_refinery
---@return string|nil RefineryType #What type of refinery this is
function ents_methods:LSRefineryGetType()
	local ent = eunwrap(self)
	if IsValid(ent) and (ent:GetClass() == "sc_refinery") then
		return ent:GetType()
	end
end

---Change the recipe of a refinery to a new one
---@param RecipeName string #The name of the recipe
---@return boolean ChangeSuccessful #True if the recipe was changed, false if the recipe was not changed or the entity is not a refinery
function ents_methods:LSRefineryChangeRecipe(RecipeName)
	local ent = eunwrap(self)
	if IsValid(ent) and (ent:GetClass() == "sc_refinery") then
		return ent:ChangeRecipe(RecipeName)
	end

	return false
end

---Get the current recipe of the refinery
---@return string|nil RecipeName #The current recipe. Nil if the entity is not a valid refinery
function ents_methods:LSRefineryGetRecipe()
	local ent = eunwrap(self)
	if IsValid(ent) and (ent:GetClass() == "sc_refinery") then
		return ent:GetRecipe()
	end
end

--#endregion