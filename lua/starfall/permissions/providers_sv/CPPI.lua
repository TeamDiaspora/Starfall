--- Provides permissions for entities based on CPPI if present

local P = setmetatable( {}, { __index = SF.Permissions.Provider } )

local ALLOW = SF.Permissions.Result.ALLOW
local DENY = SF.Permissions.Result.DENY
local NEUTRAL = SF.Permissions.Result.NEUTRAL

local canTool = {
    ["entities.canTool"] = true,
	[ "entities.parent" ] = true,
	[ "entities.unparent" ] = true,
	[ "entities.setSolid" ] = true,
	[ "entities.enableGravity" ] = true,
	[ "entities.setRenderProperty" ] = true,
	[ "wire.createWire" ] = true,
	[ "wire.deleteWire" ] = true
}

local canPhysgun = {
	[ "entities.applyForce" ] = true,
	[ "entities.setPos" ] = true,
	[ "entities.setAngles" ] = true,
	[ "entities.setVelocity" ] = true,
	[ "entities.setFrozen" ] = true
}

function P:check ( principal, target, key )
	if not CPPI then return NEUTRAL end
    if not target or not IsValid(target) then return NEUTRAL end

	if canTool[ key ] and target.CPPICanTool then
		if target:CPPICanTool( principal, "starfall_ent_lib" ) then return ALLOW end
		return DENY
	elseif canPhysgun[ key ] and target.CPPICanPhysgun then
		if target:CPPICanPhysgun( principal ) then return ALLOW end
		return DENY
	end

    if target.CPPIGetOwner and target:CPPIGetOwner() == principal then return ALLOW end

	return NEUTRAL
end

SF.Permissions.registerProvider( P )
