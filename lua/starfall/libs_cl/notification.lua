-------------------------------------------------------------------------------
-- Clientside Notification Library functions
-------------------------------------------------------------------------------

--- Notification Library - Create notifications and progress bars on your client!
-- @client

local noti_library, _ = SF.Libraries.Register("notify")


noti_library.NOTIFY_GENERIC = NOTIFY_GENERIC
noti_library.NOTIFY_ERROR = NOTIFY_ERROR
noti_library.NOTIFY_UNDO = NOTIFY_UNDO
noti_library.NOTIFY_HINT = NOTIFY_HINT
noti_library.NOTIFY_CLEANUP = NOTIFY_CLEANUP

--- Adds a notification to the client's screen.
--- Enums require "notify." in front of them. Example: "notify.NOTIFY_GENERIC"
-- @param Text The string to display on the notification.
-- @param Type Determines the method for displaying the notification. See the <a href="https://wiki.garrysmod.com/page/Enums/NOTIFY">NOTIFY Enums</a> for details.
-- @param Length The number of seconds to display the notification for.
function noti_library.AddLegacy(Text, Type, Length)
	if Type ~= nil then Type = SF.CheckType( Type, "number" ) end
	
	return notification.AddLegacy(Text, Type, Length)
end

--- Adds a notification to the client's screen, with an included sound.
--- Enums require "notify." in front of them. Example: "notify.NOTIFY_GENERIC"
-- @param Text The string to display on the notification.
-- @param SoundPath The filepath of the sound you want the hint to play. 
-- @param Type Determines the method for displaying the notification. See the <a href="https://wiki.garrysmod.com/page/Enums/NOTIFY">NOTIFY Enums</a> for details.
-- @param Length The number of seconds to display the notification for.
function noti_library.AddHint(Text, SoundPath, Type, Length)
	if Type ~= nil then Type = SF.CheckType( Type, "number" ) end
	
	notification.AddLegacy(Text, Type, Length)
	surface.PlaySound(SoundPath)
end

--- Adds a notification with an animated progress bar to the client's screen.
-- @param id The ID for the notification. Can be any type.
-- @param strText The string to display on the notification.
function noti_library.AddProgress(...)
	return notification.AddProgress(...)
end

--- Removes the AddProgress notification after 0.8 seconds.
-- @param uid The unique ID for the notification. Can be any type.
function noti_library.Kill(...)
	return notification.Kill(...)
end