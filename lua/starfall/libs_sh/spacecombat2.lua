-------------------------------------------------------------------------------
-- Shared Space Combat 2 functions
-------------------------------------------------------------------------------

assert( SF.Entities )

--- Space Combat 2 Library, Does Space Stuff
-- @shared
local sc_library, _ = SF.Libraries.Register( "SC" )

--- Space Combat 2 Manufacturing Library, Does Space Stuff
-- @shared
-- @name SC.Manufacturing
-- @class library
-- @libtbl sc_manufacturing_library
local sc_manufacturing_library, sc_manufacturing_meta = SF.Typedef( "Library: SC.Manufacturing" )
SF.Libraries.libraries.SC.Manufacturing = sc_manufacturing_meta
SF.DefaultEnvironment.SC.Manufacturing = setmetatable( {}, sc_manufacturing_meta )

-- Hooks
if SERVER then
    SF.hookAdd("OnEntityConstrained")
end

-- Main Space Combat 2 Utility Functions
--- Get the version of Space Combat 2 the server is running.
function sc_library.GetVersion()
    return SC.Version
end

--- Get all of the Space Combat 2 plugins installed on the server or client.
function sc_library.GetPlugins()
    return SC.Plugins
end

--- Determines if debugging is currently enabled
function sc_library.GetDebug()
    return SC.Debug
end

--- Get the current debug level.
function sc_library.GetDebugLevel()
    return SC.DebugLevel
end

--- Adds commas to a number and returns it as a string.
function sc_library.sc_ds(num)
    return sc_ds(num)
end

--- Converts a number like 1750 to 1.75K
function sc_library.DigiSpace(num)
    return SC_DigiSpace(num)
end

--- Tests if a flag has been set
-- @param set The number to test for flags
-- @param flag The flag to test for
function sc_library.TestFlag(set, flag)
    return SC.TestFlag(set, flag)
end

--- Sets a flag on a number
-- @param set The number to set the flag on
-- @param flag The flag to set
function sc_library.SetFlag(set, flag)
    return SC.SetFlag(set, flag)
end

--- Removes a flag from a number
-- @param set The number to clear the flag from
-- @param flag The flag to clear
function sc_library.ClearFlag(set, flag)
    return SC.ClearFlag(set, flag)
end

--- Gets a console variable as a boolean
-- @param cvar The name of the console variable to get
function sc_library.GetCvarBool(cvar) --Mainly just to save space
	return SC.GetCvarBool(cvar)
end

if CLIENT then
    -- I'd document these but I have no idea what they do --Kanz

    function sc_library.Print(mess, level)
        SC.Print(mess, level)
    end

    function sc_library.Msg(mess, level)
        SC.Msg(mess, level)
    end

    function sc_library.Error(mess, level)
        SC.Error(mess, level)
    end
end

-- Reactor Functions (should probably be moved to their own library)
--- Returns a table of reactor coolant information
function sc_library.ReactorGetCoolantTypes()
    return SF.Sanitize(SC.Reactors.GetCoolantTypes())
end

--- Returns a table of reactor reaction information
function sc_library.ReactorGetReactionTypes()
    return SF.Sanitize(SC.Reactors.GetReactionTypes())
end

--- Returns a table of reactor type information
function sc_library.ReactorGetReactorTypes()
    return SF.Sanitize(SC.Reactors.GetReactorTypes())
end

-- Manufacturing Functions
--- Checks if a type is valid
function sc_manufacturing_library.IsValidType(type)
    return SC.Manufacturing.IsValidType(type)
end

--- Checks if a recipe is valid
function sc_manufacturing_library.IsValidRecipe(name, type)
    return SC.Manufacturing.IsValidRecipe(name, type)
end

--- Gets a recipe's table
function sc_manufacturing_library.GetRecipe(name, type)
    return table.Copy(SC.Manufacturing.GetRecipe(name, type))
end

--- Fetches the tables of all recipes of the specified type
function sc_manufacturing_library.GetRecipes(type)
    return table.Copy(SC.Manufacturing.GetRecipes(type))
end

--- Returns a list of all the recipe names of a type
function sc_manufacturing_library.GetRecipeList(type)
    return SC.Manufacturing.GetRecipeList(type)
end

--- Gets the default recipe for a type
function sc_manufacturing_library.GetDefaultRecipe(type)
    return table.Copy(SC.Manufacturing.GetDefaultRecipe(type))
end

local ents_lib = SF.Entities.Library
local ents_metatable = SF.Entities.Metatable

--- Entity type
--@class class
--@name Entity
local ents_methods = SF.Entities.Methods
local ewrap, eunwrap = SF.Entities.Wrap, SF.Entities.Unwrap
local vwrap = SF.WrapObject
local vunwrap = SF.UnwrapObject

local function convert(results)
	local wrap = SF.WrapObject

	local t = {}
	for i,k in pairs(results) do
        t[i] = wrap(k)
    end
	return t
end

-- Ship Core Accessors
local function GetCoreEnt(self)
    local ent = eunwrap(self)
    return ent and ent:GetProtector() or nil
end

--- Get's the Core Entity
-- @return Entity
function ents_methods:GetProtector()
    return ewrap(GetCoreEnt(self))
end

--- Gets the ship's gyropod entity
-- @return Entity
function ents_methods:GetGyropod()
    return ewrap(GetCoreEnt(self):GetGyropod())
end

--- Gets the the current Cap amount.
-- @return Number
function ents_methods:GetCapAmount()
    return GetCoreEnt(self):GetCapAmount()
end

--- Gets the the current Cap amount in a percentage from the maximum.
-- @return Number
function ents_methods:GetCapPercent()
    return GetCoreEnt(self):GetCapPercent()
end

--- Gets the the maximum amount of Cap the current core can have.
-- @return Number
function ents_methods:GetCapMax()
    return GetCoreEnt(self):GetCapMax()
end

--- Gets the Current Shield HP
-- @return Number
function ents_methods:GetShieldAmount()
    return GetCoreEnt(self):GetShieldAmount()
end

--- Gets the Current Armor HP
-- @return Number
function ents_methods:GetArmorAmount()
    return GetCoreEnt(self):GetArmorAmount()
end

--- Gets the Current Hull HP
-- @return Number
function ents_methods:GetHullAmount()
    return GetCoreEnt(self):GetHullAmount()
end

--- Gets the Maximum Shield HP
-- @return Number
function ents_methods:GetShieldMax()
    return GetCoreEnt(self):GetShieldMax()
end

--- Gets the Maximum Armor HP
-- @return Number
function ents_methods:GetArmorMax()
    return GetCoreEnt(self):GetArmorMax()
end

--- Gets the Maximum Hull HP
-- @return Number
function ents_methods:GetHullMax()
    return GetCoreEnt(self):GetHullMax()
end

--- Gets the Percentage of the Core's Shield Remaining
-- @return Number From 0 to 1
function ents_methods:GetShieldPercent()
    return GetCoreEnt(self):GetShieldPercent()
end

--- Gets the Percentage of the Core's Armor Remaining
-- @return Number From 0 to 1
function ents_methods:GetArmorPercent()
    return GetCoreEnt(self):GetArmorPercent()
end

--- Gets the Percentage of the Core's Hull Remaining
-- @return Number From 0 to 1
function ents_methods:GetHullPercent()
    return GetCoreEnt(self):GetHullPercent()
end

--- Gets the Ship Name set in the Core
-- @return String
function ents_methods:GetShipName()
    return GetCoreEnt(self):GetShipName()
end

--- Get's the Signature Radius of the Core
-- @return SigRad in units, multiply by 0.02540 to obtain Sigrad in meters as is displayed on the Core Tooltip
function ents_methods:GetSigRad()
    return GetCoreEnt(self):GetNodeRadius()
end

--- Gets the center point of a ship from the ship's core. The vector returned is within the core's local coordinate space.
-- @return Vector
function ents_methods:GetBoxCenter()
    local ent = eunwrap(self)
    if ent:GetClass() ~= "ship_core" then
        return nil
    else
        return vwrap(ent:GetBoxCenter())
    end
end

--- Get's the time required to fully recharge the shield
-- @return Number: Time in Seconds
function ents_methods:GetShieldRechargeTime()
    return GetCoreEnt(self):GetShieldRechargeTime()
end

--- Get's the current Recharge Rate in Shield HP/s
-- @return Number
function ents_methods:GetShieldRecharge()
    local Core = GetCoreEnt(self)
    return Core:CalculateRecharge(Core:GetShieldMax(), Core:GetShieldAmount(), Core:GetShieldRechargeTime())
end

--- Get's the Shield's Peak Recharge in Shield HP/s
function ents_methods:GetShieldRechargePeak()
    local Core = GetCoreEnt(self)
    return Core:CalculateRecharge(Core:GetShieldMax(), Core:GetShieldAmount() * 0.25, Core:GetShieldRechargeTime())
end

--- Get's the total mass of the ship
-- @return Always 0 for the time being
function ents_methods:GetShipMass()
    return GetCoreEnt(self).total_mass or 0
end

--- Get's the Class of the Core
-- @return String class name
function ents_methods:GetShipClass()
    return GetCoreEnt(self):GetShipClass()
end

-- Weapon Accessor Functions

--- Gets the Entites of the weapons in the turret
-- @return Table of Entites
function ents_methods:GetTurretWeapons()
    local ent = eunwrap(self)
    return ent.Weapons and convert(ent.Weapons) or {}
end

--- Gets the Class of a Weapon
-- @return String: Class of the Weapon or "Not A Weapon"
function ents_methods:GetWeaponClass()
    local ent = eunwrap(self)
    return (ent and ent.Data and ent.Data.GUN) and ent.Data.GUN.CLASS or "Not A Weapon"
end

--- Gets the Number of shots left in the current magazine
-- @return Number: Shots Remaining, or -1 if N/A
function ents_methods:GetShotsRemaining()
    local ent = eunwrap(self)
    return ent.ShotsRemaining or -1
end

--- Returns True if the Weapon is Currently Reloading
-- @return Boolean
function ents_methods:GetIsReloading()
    local ent = eunwrap(self)
    return ent.IsReloading or false
end

--- Returns True if the Weapon is Currently Shooting
-- @return Boolean
function ents_methods:GetShooting()
    local ent = eunwrap(self)
    return ent:GetShooting() or false
end

--- Space Combat 2 Weapon Information


--- Returns the name of a ship module, or the launcher type of a fixed weapon
-- @return String
function ents_methods:GetModuleName()
    local ent = eunwrap(self)
    return ent.GetModuleName and ent:GetModuleName()
end

--- Returns the current projectile recipe of a weapon
-- @return String
function ents_methods:GetProjectileRecipe()
    local ent = eunwrap(self)
    return ent.GetProjectileRecipe and ent:GetProjectileRecipe()
end

--- Returns the launcher type of a turret's weapons
-- @return String
function ents_methods:GetLauncherType()
    local ent = eunwrap(self)
    return ent.GetLauncherType and ent:GetLauncherType()
end

--TODO: Add more weapon interface functions for GetAmmo(), GetReloading(), etc... and remove the old legacy weapon interface functions
