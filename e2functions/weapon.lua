/******************************************************************
****************          Fehas weapon Extension          ******************
******************************************************************/

/***********************************************************************************************************************
Credits


Myself - I am coding the stuff

gmod lua wiki - they got a list of functions you can use.
***********************************************************************************************************************/

//Give weapon and ammo
e2function entity entity:giveWep(string weapon)
	if (!IsValid(this)) then return end
	
	local owner = self.player
	
	if (owner:IsAdmin() and this:IsPlayer()) then
		this:StripWeapon(weapon)
		return this:Give(weapon)
	end
	
	return
end

e2function entity entity:giveWep(string weapon, number amount)
	if (!IsValid(this)) then return end
	
	local owner = self.player
	
	if (owner:IsAdmin() and this:IsPlayer()) then
		this:StripWeapon(weapon)
		weaponEnt = this:Give(weapon)
		if (weaponEnt:IsValid()) then
			ammo = weaponEnt:GetPrimaryAmmoType()
			this:SetAmmo(amount, ammo)
			
			return weaponEnt
		end
	end
	
	return
end

e2function string entity:giveAmmo(number amount, string ammo)
	if (!IsValid(this)) then return "" end
	
	local owner = self.player
	
	if (owner:IsAdmin() and this:IsPlayer()) then
		this:GiveAmmo(amount, ammo)
		
		return amount .. " " .. ammo
	end
	
	return ""
end

//Set ammo, health and armor
e2function string entity:setAmmo(number amount, string ammo)
	if (!IsValid(this)) then return "" end
	
	local owner = self.player
	
	if (owner:IsAdmin() and this:IsPlayer()) then
		this:SetAmmo(amount, ammo)
		
		return amount .. " " .. ammo
	end
	
	return ""
end

//Adding in Rank based permissions for setHealth/Armor. -Steeveeo
//	- Players: Can set own health to cap of 100, armor to 50. Cannot touch others.
//	- Respected: Can set own health/armor unlimitedly. Can set others to cap of 100 each, but cannot kill others.
//	- Administrator: Unlimited control.

//Ranks are based on Teams set by server code.
//	- Team 4 = Player
//	- Team 5 = Admin
//	- Team 6 = Respected

function getE2PermissionLevel(ply, self)
	if self.PermissionLevel != nil then return self.PermissionLevel end
	
	if ply == nil or !IsValid(ply) then return 0 end
	if self == nil then return 0 end
	
	if ply:Team() == 4 then
		self.PermissionLevel = 1
		return 1
	elseif ply:Team() == 6 then
		self.PermissionLevel = 2
		return 2
	elseif ply:Team() == 5 or ply:IsAdmin() then
		self.PermissionLevel = 3
		return 3
	//Banned? Spectator? I don't know.
	else
		self.PermissionLevel = 0
		return 0
	end
end

local function setPlyHealth(ply, amt, adminOverride)
	
	//Godmode Check
	if ply.ASSHasGod and (!adminOverride or ply:IsAdmin()) then
		amt = math.max(ply:Health(), amt)
	end

	if amt <= 0 and (ply:Alive() or adminOverride) then
		ply:Kill()
	else
		ply:SetHealth(amt)
	end
end

e2function number entity:setHealth(number amount)
	if !IsValid(this) then return -1 end
	if !this:IsPlayer() then return -1 end
	
	local owner = self.player
	local permLevel = getE2PermissionLevel(owner, self)
	
	if permLevel == 0 then return -1 end
	
	//Players
	if permLevel == 1 then
		if this == owner then
			amount = math.Clamp(amount, 0, 100)
			setPlyHealth(this, amount, 0)
			return amount
		end
	//Respected
	elseif permLevel == 2 then
		if this == owner then
			setPlyHealth(this, amount, 0)
			return amount
		else
			amount = math.Clamp(amount, 1, 100)
			setPlyHealth(this, amount, 0)
			return amount
		end
	//Admins
	elseif permLevel == 3 then
		setPlyHealth(this, amount, 1)
	end
	
	return -1 //Error, player isn't set to an expected team! D:
end

e2function number entity:setArmor(number amount)
	if !IsValid(this) then return -1 end
	if !this:IsPlayer() then return -1 end
	
	local owner = self.player
	local permLevel = getE2PermissionLevel(owner, self)
	
	if permLevel == 0 then return -1 end
	
	//Negative armor is not a thing!
	amount = math.max(0, amount)
	
	//Players
	if permLevel == 1 then
		if this == owner then
			amount = math.Clamp(amount, 0, 50)
			this:SetArmor(amount)
			return amount
		end
	//Respected
	elseif permLevel == 2 then
		if this == owner then
			this:SetArmor(amount)
			return amount
		else
			amount = math.Clamp(amount, 0, 100)
			this:SetArmor(amount)
			return amount
		end
	//Admins
	elseif permLevel == 3 then
		this:SetArmor(amount)
	end
	
	return -1 //Error, player isn't set to an expected team! D:
end

//Strip weapon and ammo
e2function number entity:stripWep()
	if (!IsValid(this)) then return 0 end
	
	local owner = self.player
	
	if ((owner:IsAdmin() or owner == this) and this:IsPlayer()) then
		this:StripAmmo()
		this:StripWeapons()
		
		return 1
	end
	
	return 0
end

e2function number entity:stripWep(string weapon)
	if (!IsValid(this)) then return 0 end
	
	local owner = self.player
	
	if ((owner:IsAdmin() or owner == this) and this:IsPlayer()) then
		this:StripWeapon(weapon)
		
		return 1
	end
	
	return 0
end

e2function number entity:stripAmmo()
	if (!IsValid(this)) then return 0 end
	
	local owner = self.player
	
	if ((owner:IsAdmin() or owner == this) and this:IsPlayer()) then
		this:StripAmmo()
		
		return 1
	end
	
	return 0
end

//has weapon?
e2function number entity:hasWeapon(string weapon)
	if (!IsValid(this)) then return 0 end
	
	local owner = self.player
	
	if (this:IsPlayer()) then
		return this:HasWeapon(weapon)
	end
	
	return 0
end