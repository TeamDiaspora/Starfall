local string_match = string.match
local string_gsub = string.gsub
local string_find = string.find
local table_concat = table.concat
local string_sub = string.sub

local function checkArgs( str_args, func_args )
	if func_args == "..." then  -- Argument types don't matter
		return true, true
	elseif string_find( func_args, "...", 1, true ) then		
		func_args = string_match(func_args,"(.+)%.%.%.") -- Get the argument types that do matter
		local ok = (func_args == string_sub( str_args, 1, #func_args )) -- Compare them
		
		return ok, true
	else
		return (func_args == str_args), false
	end
end

local function generateArgs( self, has_varargs, typeids, func_args, args, startAt )
	---- Create fake args table
	local new_typeids 
	if has_varargs then
		-- Copy the typeids (Doesn't seem to be necessary, so I commented it out, and it still works)
	
		-- Calculate how many typeids we need to skip (hacky code D:)
		local n = 0
		for i=1,#func_args do
			n = n + 1
			if func_args[i] == "x" then n = n - 2 end
		end
		
		new_typeids = {}
		for i=startAt+n,#typeids do
			new_typeids[i-(startAt-1+n)] = typeids[i]
		end
	end
	
	-- Copy the arguments
	local new_args = {}
	for i=startAt+1,#args-1 do
		-- Didn't work
		--new_args[i-4] = args[i]

		-- Didn't work either
		--new_args[i-4] = {}
		--for k,v in pairs( args[i] ) do
		--	new_args[i-4][k] = v
		--end
		
		-- Hacky code D:
		local val = args[i][1]( self, args[i] )
		new_args[i-(startAt-1)] = {function() return val end}
	end
	
	-- Set the first argument to the function itself (Not 100% sure this is correct, but it works)
	new_args[1] = func
	
	-- Set the new typeids
	if has_varargs then new_args[#new_args+1] = new_typeids end
	
	return new_args
end

registerFunction("timer","nnss...","", function( self, args )
	local op1, op2, op3, op4 = args[2], args[3], args[4], args[5]
	local delay, reps, uniqueid, funcname = op1[1](self,op1), op2[1](self,op2), op3[1](self,op3), op4[1](self,op4)
	
	if funcname == "timer(nnss...)" then error( "You may not call the timer function from inside a timer", 4 ) end
	
	local func = self.funcs[funcname] or (wire_expression2_funcs[funcname] ~= nil and wire_expression2_funcs[funcname][3] or nil)
	
	if func then
		local typeids = args[#args]		
		
		local func_args = string_gsub(string_match( funcname, "%((.+)%)" ) or "",":","")	
		local str_args = table_concat( typeids, "", 5 )
		
		local args_are_ok, has_varargs = checkArgs( str_args, func_args )

		if args_are_ok then
			
			local id = "Wire_Expression2_Timer_" .. self.entity:EntIndex() .. "_" .. uniqueid
			
			local new_args = generateArgs( self, has_varargs, typeids, func_args, args, 5 )
			
			timer.Create( id, delay/1000, reps, function()
				if self and self.entity and self.entity:IsValid() then
					func( self, new_args )
				else
					timer.Remove( id )
				end
			end)
		else
			error( "Mismatching arguments to timer: expected " .. func_args .. ", got " .. str_args, 4 )
		end
	else
		error( "The specified function does not exist.", 4 )
	end
end)

e2function void removetimer( string uniqueid )
	local id = "Wire_Expression2_Timer_" .. self.entity:EntIndex() .. "_" .. uniqueid
	if timer.Exists( id ) then
		timer.Remove( id )
	end
end

local function upperfirst( word )
	return word:Left(1):upper() .. word:Right(-2):lower()
end

local funcnamecheck = {}
funcnamecheck["callFunction(s...)"] = true

registerCallback( "postinit", function()
	for k,v in pairs( wire_expression_types ) do
		local name = upperfirst( k )
		if (name == "Normal") then name = "Number" end
		local id = v[1]
		
		funcnamecheck["callFunction" .. name .. "(s...)"] = true
		
		registerFunction("callFunction" .. name,"s...",id,function(self,args)
			local op1 = args[2]
			local funcname = op1[1](self,op1)
			
			if funcnamecheck[funcname] then error( "You may not call the callFunction function from itself", 4 ) end
			
			-- Get the function and return type
			local func, func_return_type
			if self.funcs[funcname] ~= nil then
				func = self.funcs[funcname]
				func_return_type = self.funcs_ret[funcname]
			elseif wire_expression2_funcs[funcname] ~= nil then
				func = wire_expression2_funcs[funcname][3]
				func_return_type = wire_expression2_funcs[funcname][2]
			end
			
			if func then
				-- Check return type
				if func_return_type ~= v[1] then
					error( "Mismatching return type. Expected " .. upperfirst(wire_expression_types2[k][1]) .. ", function had " .. upperfirst(wire_expression_types2[func_return_type][1] ), 4 )
				end
			
				local typeids = args[#args]
				
				local func_args = string_gsub(string_match( funcname, "%((.+)%)" ) or "",":","")	
				local str_args = table_concat( typeids, "", 2 )
				
				local args_are_ok, has_varargs = checkArgs( str_args, func_args )

				if args_are_ok then					
					local new_args = generateArgs( self, has_varargs, typeids, func_args, args, 2 )
					
					-- Call the function
					return func( self, new_args )
				else
					error( "Mismatching arguments to callFunction: expected " .. func_args .. ", got " .. str_args, 4 )
				end
			else
				error( "The specified function does not exist.", 4 )
			end
		end)
	end
end)

registerFunction("callFunction","s...","",function(self,args)
	local op1 = args[2]
	local funcname = op1[1](self,op1)
	
	if funcnamecheck[funcname] then error( "You may not call the callFunction function from itself", 4 ) end

	local func = self.funcs[funcname] or (wire_expression2_funcs[funcname] ~= nil and wire_expression2_funcs[funcname][3] or nil)
	
	if func then	
		local typeids = args[#args]
		
		local func_args = string_gsub(string_match( funcname, "%((.+)%)" ) or "",":","")	
		local str_args = table_concat( typeids, "", 2 )
		
		local args_are_ok, has_varargs = checkArgs( str_args, func_args )

		if args_are_ok then					
			local new_args = generateArgs( self, has_varargs, typeids, func_args, args, 2 )
			
			-- Call the function
			func( self, new_args )
		else
			error( "Mismatching arguments to callFunction: expected " .. func_args .. ", got " .. str_args, 4 )
		end
	else
		error( "The specified function does not exist.", 4 )
	end
end)