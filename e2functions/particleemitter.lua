//-------------------------------------
// PARTICLE EMITTER: E2 Extension
//		Author: Steeveeo
//
//	This E2 Extension is to allow E2
//	users to create CLuaEmitter effects
//	wherever needed (Note: this does
//	not work for PCF Particles!).
//-------------------------------------

//Setup Emitter Table (mainly used to send to new clients)
Emitters = {}

//Setup Control Signals
util.AddNetworkString("Emitter - Client Requesting Sync")
util.AddNetworkString("Emitter - Client Sync Complete")
util.AddNetworkString("Emitter - Create Emitter")
util.AddNetworkString("Emitter - Destroy Emitter")
util.AddNetworkString("Emitter - Sync Emitter")
util.AddNetworkString("Emitter - Set Parent")
util.AddNetworkString("Emitter - Set Pos")
util.AddNetworkString("Emitter - Set Emitter Rate")
util.AddNetworkString("Emitter - Set Material")
util.AddNetworkString("Emitter - Set Drag")
util.AddNetworkString("Emitter - Set Bounce")
util.AddNetworkString("Emitter - Set Can Collide")
util.AddNetworkString("Emitter - Set Color")
util.AddNetworkString("Emitter - Set Die Time")
util.AddNetworkString("Emitter - Set Start Alpha")
util.AddNetworkString("Emitter - Set End Alpha")
util.AddNetworkString("Emitter - Set Start Size")
util.AddNetworkString("Emitter - Set End Size")
util.AddNetworkString("Emitter - Set Start Length")
util.AddNetworkString("Emitter - Set End Length")
util.AddNetworkString("Emitter - Set Gravity")
util.AddNetworkString("Emitter - Set Roll")
util.AddNetworkString("Emitter - Set Roll Noise")
util.AddNetworkString("Emitter - Set Roll Speed")
util.AddNetworkString("Emitter - Set Roll Speed Noise")
util.AddNetworkString("Emitter - Set Roll Drag")
util.AddNetworkString("Emitter - Set Roll Speed Min")
util.AddNetworkString("Emitter - Set Velocity")
util.AddNetworkString("Emitter - Set Velocity Noise")
util.AddNetworkString("Emitter - Set Velocity Inherit")
--util.AddNetworkString("Emitter - Set Angles")
--util.AddNetworkString("Emitter - Set Angle Velocity")

/* Figure out either what these are or how to implement them
NewerClient.png CLuaParticle:SetCollideCallback		-- Maybe for setting up bounce sounds for particles?
NewerClient.png CLuaParticle:SetLifeTime			-- Probably not needed (sets the current life of a particle to a specific timestamp)
NewerClient.png CLuaParticle:SetLighting			-- Not a clue
NewerClient.png CLuaParticle:SetNextThink			-- Probably not needed (Time to next Think tick per-particle)
NewerClient.png CLuaParticle:SetPos					-- Probably not needed (setting the positions of each particle is beyond the scope of this e2)
NewerClient.png CLuaParticle:SetThinkFunction		-- Will probably use this later for more advanced particles
NewerClient.png CLuaParticle:SetVelocityScale		-- Not a clue
NewerClient.png CLuaParticle:VelocityDecay			-- Not entirely sure what this does, but I think it's a boolean or a speed limit in which to remove particles
*/


//Set up Server Convars
CreateConVar("wire_particles_maxemitters", 10, { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_ARCHIVE, FCVAR_NOTIFY }, "Maximum number of Particle Emitters per E2.")
CreateConVar("wire_particles_maxparticlerate", 100, { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_ARCHIVE, FCVAR_NOTIFY }, "Maximum number of Particles that can be emitted in one second.")
CreateConVar("wire_particles_maxparticlelife", 30, { FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED, FCVAR_ARCHIVE, FCVAR_NOTIFY }, "Maximum number of seconds that each Particle can last.")



//-----------------------------------
// PARTICLE CREATION FUNCTIONS
//-----------------------------------

function syncEmitters(ply)
	for ent, k in pairs(Emitters) do
		for index, v in pairs(Emitters[ent]) do
			net.Start("Emitter - Sync Emitter")
				
				//Prepare Data
				local pos = Emitters[ent][index]["pos"]
				local parentpos = Emitters[ent][index]["parentpos"]
				local color = Emitters[ent][index]["color"]
				local gravity = Emitters[ent][index]["gravity"]
				local velocity = Emitters[ent][index]["velocity"]
				local velNoise = Emitters[ent][index]["velNoise"]
				local velInherit = Emitters[ent][index]["velInherit"]
				--local ang = Emitters[ent][index]["angles"]
				--local angVel = Emitters[ent][index]["angVel"]	//These don't have a use yet.
				
				if not pos then ErrorNoHalt("Why the hell is POS nil?!") return end
				if not parentpos then Vector(0, 0, 0) end
				if not color then color = Vector(255, 255, 255) end
				if not gravity then gravity = Vector(0, 0, 0) end
				if not velocity then velocity = Vector(0, 0, 0.01) end
				if not velNoise then velNoise = Vector(0, 0, 0) end
				if not velInherit then velInherit = Vector(0, 0, 0) end
				--if not ang then ang = Angle(0, 0, 0) end
				--if not angVel then angVel = Angle(0, 0, 0) end
				
				//Send Data
				net.WriteFloat(index)
				net.WriteEntity(ent)
				net.WriteVector(Vector(pos[1], pos[2], pos[3]))
				net.WriteEntity(Emitters[ent][index]["parent"])
				net.WriteVector(Vector(parentpos[1], parentpos[2], parentpos[3]))
				net.WriteFloat(Emitters[ent][index]["rate"] or 0)
				net.WriteString(Emitters[ent][index]["mat"] or "")
				net.WriteFloat(Emitters[ent][index]["drag"] or 0)
				net.WriteFloat(Emitters[ent][index]["bounce"] or 0)
				net.WriteFloat(Emitters[ent][index]["canCollide"] or 0)
				net.WriteVector(Vector(color[1], color[2], color[3]))
				net.WriteFloat(Emitters[ent][index]["lifespan"] or 0)
				net.WriteFloat(Emitters[ent][index]["startAlpha"] or 0)
				net.WriteFloat(Emitters[ent][index]["endAlpha"] or 0)
				net.WriteFloat(Emitters[ent][index]["startSize"] or 0)
				net.WriteFloat(Emitters[ent][index]["endSize"] or 0)
				net.WriteFloat(Emitters[ent][index]["startLength"] or 0)
				net.WriteFloat(Emitters[ent][index]["endLength"] or 0)
				net.WriteVector(Vector(gravity[1], gravity[2], gravity[3]))
				net.WriteFloat(Emitters[ent][index]["roll"] or 0)
				net.WriteFloat(Emitters[ent][index]["rollNoise"] or 0)
				net.WriteFloat(Emitters[ent][index]["rollSpeed"] or 0)
				net.WriteFloat(Emitters[ent][index]["rollSpeedNoise"] or 0)
				net.WriteFloat(Emitters[ent][index]["rollDrag"] or 0)
				net.WriteFloat(Emitters[ent][index]["rollSpeedMin"] or 0)
				net.WriteVector(Vector(velocity[1], velocity[2], velocity[3]))
				net.WriteVector(Vector(velNoise[1], velNoise[2], velNoise[3]))
				net.WriteVector(Vector(velInherit[1], velInherit[2], velInherit[3]))
				--net.WriteAngle(Angle(ang[1], ang[2], ang[3]))
				--net.WriteAngle(Angle(angVel[1], angVel[2], angVel[3]))
			net.Send(ply)
		end
	end
	
	//Notify client that it can start rendering
	net.Start("Emitter - Client Sync Complete")
		//Need something here?
	net.Send(ply)
end

//Client asking for Emitters
net.Receive("Emitter - Client Requesting Sync", function(len, ply)
	syncEmitters(ply)
end)


local function canCreateParticles(self)
	local ent = self.entity
	
	if Emitters[ent] then
		return (table.Count(Emitters[ent]) < GetConVar("wire_particles_maxemitters"):GetInt())
	else
		return true
	end
end


local function createEmitter(self, index, pos)
	local ent = self.entity
	
	//Create table if it doesn't exist
	if not Emitters[ent] then
		Emitters[ent] = {}
	end
	
	//Check if we can create new particles
	if canCreateParticles(self) then
		if not Emitters[ent][index] then
			Emitters[ent][index] = {}
		end
		
		Emitters[ent][index]["pos"] = pos
		
		//Default Settings
		Emitters[ent][index]["parent"] = nil
		Emitters[ent][index]["parentpos"] = Vector(0, 0, 0)
		Emitters[ent][index]["rate"] = 1
		Emitters[ent][index]["mat"] = ""
		Emitters[ent][index]["drag"] = 0
		Emitters[ent][index]["bounce"] = 0
		Emitters[ent][index]["canCollide"] = 0
		Emitters[ent][index]["color"] = Vector(255, 255, 255)
		Emitters[ent][index]["lifespan"] = 1
		Emitters[ent][index]["startAlpha"] = 255
		Emitters[ent][index]["endAlpha"] = 255
		Emitters[ent][index]["startSize"] = 10
		Emitters[ent][index]["endSize"] = 10
		Emitters[ent][index]["startLength"] = 10
		Emitters[ent][index]["endLength"] = 10
		Emitters[ent][index]["gravity"] = Vector(0, 0, 0)
		Emitters[ent][index]["roll"] = 0
		Emitters[ent][index]["rollNoise"] = 0
		Emitters[ent][index]["rollSpeed"] = 0
		Emitters[ent][index]["rollSpeedNoise"] = 0
		Emitters[ent][index]["rollDrag"] = 0
		Emitters[ent][index]["rollSpeedMin"] = 0
		Emitters[ent][index]["velocity"] = Vector(0, 0, 0.01)
		Emitters[ent][index]["velNoise"] = Vector(0, 0, 0)
		Emitters[ent][index]["velInherit"] = Vector(0, 0, 0)
		--Emitters[ent][index]["angles"] = Angle(0, 0, 0)
		--Emitters[ent][index]["angVel"] = Angle(0, 0, 0)	//These don't have a use yet.
		
		//Send Info (Don't send defaults, that's already predicted clientside)
		net.Start("Emitter - Create Emitter")
			net.WriteFloat(index)
			net.WriteEntity(ent)
			net.WriteVector(Vector(pos[1], pos[2], pos[3]))
		net.Broadcast()
	end
end


local function destroyEmitter(self, index)
	local ent = self.entity
	
	//Make sure this emitter table exists
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	//Remove and notify
	Emitters[ent][index] = nil
	net.Start("Emitter - Destroy Emitter")
		net.WriteFloat(index)
		net.WriteEntity(ent)
	net.Broadcast()
end


local function flushEmitters(self)
	local ent = self.entity
	
	//Check if there's anything to clean up
	if not Emitters[ent] then return end
	
	//Loop and destroy all active particles
	for k,v in pairs(Emitters[ent]) do
		destroyEmitter(self, k)
	end
	
	Emitters[ent] = nil
end


//Cleanup with chip
registerCallback("destruct", function(self)
	flushEmitters(self)
end)


//-------------------------------
// E2 Function Definitions
//-------------------------------

e2function void emitterCreate(number index, vector pos)
	return createEmitter(self, index, pos)
end


e2function void emitterDestroy(number index)
	return destroyEmitter(self, index)
end


e2function void emitterDestroyAll()
	return flushEmitters(self)
end


e2function number emitterCanCreate()
	return canCreateParticles(self)
end


e2function number emitterRemainingSpawns()
	if Emitters[ent] then
		return (GetConVar("wire_particles_maxemitters"):GetInt() - table.Count(Emitters[ent]))
	else
		return GetConVar("wire_particles_maxemitters"):GetInt()
	end
end

//--End E2 Function Definitions--


//--END PARTICLE CREATION FUNCTIONS--


//----------------------------------
// PARTICLE CONTROL FUNCTIONS
//----------------------------------

e2function void emitterSetParent(number index, entity parent)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["parent"] = parent
	
	net.Start("Emitter - Set Parent")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteEntity(parent)
	net.Broadcast()
end


e2function void emitterSetPos(number index, vector pos)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["pos"] = pos
	
	net.Start("Emitter - Set Pos")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteVector(Vector(pos[1], pos[2], pos[3]))
	net.Broadcast()
end


e2function void emitterSetRate(number index, number particlesPerSec)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	local rate = math.Clamp(particlesPerSec, 0, GetConVar("wire_particles_maxparticlerate"):GetInt())
	
	Emitters[ent][index]["rate"] = rate
	
	net.Start("Emitter - Set Emitter Rate")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(rate)
	net.Broadcast()
end


e2function void emitterSetMaterial(number index, string mat)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["mat"] = mat
	
	net.Start("Emitter - Set Material")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteString(mat)
	net.Broadcast()
end


e2function void emitterSetDrag(number index, number drag)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["drag"] = drag
	
	net.Start("Emitter - Set Drag")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(drag)
	net.Broadcast()
end


e2function void emitterSetBounce(number index, number bounceMult)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["bounce"] = bounceMult
	
	net.Start("Emitter - Set Bounce")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(bounceMult)
	net.Broadcast()
end


e2function void emitterSetCanBounce(number index, number canBounce)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["canCollide"] = tobool(canBounce) //Quick way to make a bool out of a number
	
	net.Start("Emitter - Set Can Collide")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(canBounce)
	net.Broadcast()
end


e2function void emitterSetColor(number index, vector color)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["color"] = color
	
	net.Start("Emitter - Set Color")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteVector(Vector(color[1], color[2], color[3]))
	net.Broadcast()
end


e2function void emitterSetLifespan(number index, number time)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["lifespan"] = time
	
	net.Start("Emitter - Set Die Time")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(time)
	net.Broadcast()
end


e2function void emitterSetStartAlpha(number index, number alpha)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["startAlpha"] = alpha
	
	net.Start("Emitter - Set Start Alpha")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(alpha)
	net.Broadcast()
end


e2function void emitterSetEndAlpha(number index, number alpha)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["endAlpha"] = alpha
	
	net.Start("Emitter - Set End Alpha")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(alpha)
	net.Broadcast()
end


e2function void emitterSetStartSize(number index, number size)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["startSize"] = size
	
	net.Start("Emitter - Set Start Size")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(size)
	net.Broadcast()
end


e2function void emitterSetEndSize(number index, number size)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["endSize"] = size
	
	net.Start("Emitter - Set End Size")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(size)
	net.Broadcast()
end


e2function void emitterSetStartLength(number index, number length)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["startLength"] = length
	
	net.Start("Emitter - Set Start Length")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(length)
	net.Broadcast()
end


e2function void emitterSetEndLength(number index, number length)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["endLength"] = length
	
	net.Start("Emitter - Set End Length")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(length)
	net.Broadcast()
end


e2function void emitterSetGravity(number index, vector direction)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["gravity"] = direction
	
	net.Start("Emitter - Set Gravity")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteVector(Vector(direction[1], direction[2], direction[3]))
	net.Broadcast()
end


e2function void emitterSetRoll(number index, number roll)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["roll"] = roll
	
	net.Start("Emitter - Set Roll")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(roll)
	net.Broadcast()
end


e2function void emitterSetRollNoise(number index, number roll)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["roll"] = roll
	
	net.Start("Emitter - Set Roll Noise")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(roll)
	net.Broadcast()
end


e2function void emitterSetRollSpeed(number index, number speed)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["rollSpeed"] = speed
	
	net.Start("Emitter - Set Roll Speed")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(speed)
	net.Broadcast()
end


e2function void emitterSetRollSpeedNoise(number index, number speed)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["rollSpeedNoise"] = speed
	
	net.Start("Emitter - Set Roll Speed Noise")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(speed)
	net.Broadcast()
end


e2function void emitterSetRollDrag(number index, number drag)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["rollDrag"] = drag
	
	net.Start("Emitter - Set Roll Drag")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(drag)
	net.Broadcast()
end


e2function void emitterSetRollSpeedMin(number index, number speed)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["rollSpeedMin"] = speed
	
	net.Start("Emitter - Set Roll Speed Min")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteFloat(speed)
	net.Broadcast()
end


e2function void emitterSetVelocity(number index, vector vel)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["velocity"] = vel
	
	net.Start("Emitter - Set Velocity")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteVector(Vector(vel[1], vel[2], vel[3]))
	net.Broadcast()
end


e2function void emitterSetVelocityNoise(number index, vector vel)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["velNoise"] = vel
	
	net.Start("Emitter - Set Velocity Noise")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteVector(Vector(vel[1], vel[2], vel[3]))
	net.Broadcast()
end


local function setVelInherit(self, index, mult)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["velInherit"] = vel
	
	net.Start("Emitter - Set Velocity Inherit")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteVector(mult)
	net.Broadcast()
end

e2function void emitterSetVelocityInherit(number index, vector mult)
	setVelInherit(self, index, Vector(mult[1], mult[2], mult[3]))
end

e2function void emitterSetVelocityInherit(number index, number mult)
	setVelInherit(self, index, Vector(mult, mult, mult))
end


/* Commented out until I can find a use for these, perhaps an angle override of some sort.
e2function void emitterSetAngles(number index, angle ang)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["angles"] = ang
	
	net.Start("Emitter - Set Angles")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteAngle(Angle(ang[1], ang[2], ang[3]))
	net.Broadcast()
end


e2function void emitterSetAngleVelocity(number index, angle angVel)
	local ent = self.entity
	
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end

	Emitters[ent][index]["angVel"] = angVel
	
	net.Start("Emitter - Set Angle Velocity")
		net.WriteFloat(index)
		net.WriteEntity(ent)
		net.WriteAngle(Angle(angVel[1], angVel[2], angVel[3]))
	net.Broadcast()
end
*/

//--END PARTICLE CONTROL FUNCTIONS--