include("shared.lua")
ENT.RenderGroup = RENDERGROUP_OPAQUE
include("starfall/SFLib.lua")
assert(SF, "Starfall didn't load correctly!")
local context = SF.CreateContext()

hook.Add("sf_libs_loaded", "sf_create_context", function()
    context = SF.CreateContext()
end)

local dlScreen
local dlOwner
local dlMain
local dlFiles
local hashes = {}

net.Receive("starfall_download", function(len)
    if not dlScreen then
        dlScreen = net.ReadEntity()
        dlOwner = net.ReadEntity()
        dlMain = net.ReadString()
        dlFiles = {}
    else
        if net.ReadBit() ~= 0 then
            if dlScreen:IsValid() then
                dlScreen:CodeSent(dlFiles, dlMain, dlOwner)
                dlScreen.files = dlFiles
                dlScreen.mainfile = dlMain
                dlScreen.owner = dlOwner
            end

            dlScreen, dlFiles, dlMain, dlOwner = nil, nil, nil, nil

            return
        end

        local filename = net.ReadString()
        local filedata = net.ReadString()
        dlFiles[filename] = dlFiles[filename] and dlFiles[filename] .. filedata or filedata
    end
end)

net.Receive("starfall_update", function(len)
    local screen = net.ReadEntity()
    if not IsValid(screen) then return end
    local dirty = false
    local finish = net.ReadBit()

    while finish == 0 do
        local file = net.ReadString()
        local hash = net.ReadString()

        if hash ~= hashes[file] then
            dirty = true
            hashes[file] = hash
        end

        finish = net.ReadBit()
    end

    if dirty then
        net.Start("starfall_download")
        net.WriteEntity(screen)
        net.SendToServer()
    else
        screen:CodeSent(screen.files, screen.mainfile, screen.owner)
    end
end)

net.Receive("starfall_used", function(len)
    local screen = net.ReadEntity()
    local activator = net.ReadEntity()
    if not IsValid(screen) then return end
    screen:runScriptHook("starfallUsed", SF.Entities.Wrap(activator))

    -- Error message copying
    if screen.error then
        SetClipboardText(string.format("%q", screen.error.orig))
    end
end)

local function getRenderBounds(ent)
    if not ent:IsValid() then return end

    return ent:OBBMins(), ent:OBBMaxs()
end

function ENT:Initialize()
    self:SetRenderBounds(getRenderBounds(self))
    self.instance = nil
    net.Start("starfall_download")
    net.WriteEntity(self)
    net.SendToServer()
end

function ENT:Think()
    self:NextThink(CurTime())

    if self.instance and not self.instance.error then
        local bufferAvg = self.instance:movingCPUAverage()
        self.CPUus = math.Round(bufferAvg * 1000000)
        self.CPUpercent = math.floor(bufferAvg / self.instance.context.cpuTime.getMax() * 100)
        self.instance.cpu_total = 0
        self.instance.cpu_average = bufferAvg
        self:runScriptHook("think")
    end

    return true
end

function ENT:CodeSent(files, main, owner)
    if not files or not main or not owner then return end
    local ppdata = {}
    SF.Preprocessor.ParseDirectives(main, files[main], {}, ppdata)

    if ppdata.shared then
        if self.instance then
            self.instance:deinitialize()
        end

        self.owner = owner

        local ok, instance = SF.Compiler.Compile(files, context, main, owner, {
            entity = self,
            render = {}
        })

        if not ok then
            self:Error(instance)

            return
        end

        instance.runOnError = function(inst, ...)
            self:Error(...)
        end

        self.instance = instance
        local ok, msg, traceback = instance:initialize()

        if not ok then
            self:Error(msg, traceback)
        end
    end
end

function ENT:OnRemove()
    if not self.instance then return end
    hook.Run("sf_deinitialize", self:EntIndex())
    self:runScriptHook("Removed")

    if self.instance then
        self.instance:deinitialize()
    end

    self.instance = nil
end