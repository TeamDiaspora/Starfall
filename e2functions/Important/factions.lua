e2function string entity:getFaction()
	if this then 
		if this:GetClass() == "player" then
			return this:GetFaction()
		else
			return ""
		end
	end
end

e2function number entity:setFaction(string faction)
	local owner = self.player
	if(owner:IsAdmin() or owner:IsSuperAdmin()) then
		if(this:IsPlayer() and factionExists(faction)) then
			if(this:GetFaction(faction)) then return 1
			else return 0 end
		else return 0 end
	else return 0 end
end