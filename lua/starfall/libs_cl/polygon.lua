-------------------------------------------------------------------------------
--- Polygon Library
-------------------------------------------------------------------------------

--- Polygon library. This library is used for generating, handling,
-- and processing of polygons for use in rendering on Starfall Screens,
-- HUDs, etc.

SF.Polygon = {}
local poly_library, _ = SF.Libraries.Register( "polygon" )

--- Polygon type
--@client
local poly_methods, poly_metamethods = SF.Typedef( "Polygon" )
local wrappoly, unwrappoly = SF.CreateWrapper( poly_metamethods )
SF.Polygon.Wrap = wrappoly
SF.Polygon.Unwrap = unwrappoly
SF.Polygon.Methods = poly_methods
SF.Polygon.Metamethods = poly_metamethods

--The silly things you do for those extra nanoseconds...
local clamp = math.Clamp
local floor = math.floor
local cosine = math.cos
local sine = math.sin
local toRad = math.rad
local tableInsert = table.insert
local tableCopy = table.Copy

function SF.Polygon.Checkvertex ( vert )
	return {
		x = SF.CheckType( vert.x or vert[ 1 ], "number", 1 ),
		y = SF.CheckType( vert.y or vert[ 2 ], "number", 1 ),
		u = tonumber( vert.u or vert[ 3 ] ) or 0,
		v = tonumber( vert.v or vert[ 4 ] ) or 0,
	}
end

--[[ These don't work currently two-fold. They didn't work as originally intended in the
		render library (you can't access them like arrays), AND they fuck with internal
		accessing. Commented out until I can rewrite these to be not-dumb. -Steeveeo
function poly_metamethods:__index ( k )
	SF.CheckType( self, poly_metamethods )
	SF.CheckType( k, "number" )
	local poly = unwrappoly( self )
	if not poly then return nil end
	if k <= 0 or k > #poly then return nil end
	return table.Copy( poly[ k ] )
end

function poly_metamethods:__len ()
	SF.CheckType( self, poly_metamethods )
	local poly = unwrappoly( self )
	return poly and #poly or nil
end

function poly_metamethods:__newindex ( k, v )
	SF.CheckType( self,poly_metamethods )
	SF.CheckType( k,"number" )
	SF.CheckType( v,"table" )
	local poly = unwrappoly( self )
	if not poly then return end
	if k <= 0 or k > ( #poly ) + 1 then return SF.throw( "poly index out of bounds: " .. k .. " out of " .. #poly, 2 ) end
	poly[ k ] = SF.Polygon.Checkvertex( v )
end
]]--

--- Compiles a 2D poly from an array of vertices. This is needed so that poly doesn't
-- have to be type-checked each frame or between modifications. Polys can be indexed
-- by a number, in which a copy of the vertex at that spot is returned. They can also
-- be assigned a new vertex at 1 <= i <= #poly+1. And the length of the poly can be taken.
-- @param verts Array of verticies to convert.
-- @return compiled polygon
function poly_library.CreatePoly ( verts )
	SF.CheckType( verts, "table" )
	local poly = { }
	local wrappedpoly = wrappoly( poly )
	for i = 1, #verts do
		local v = verts[ i ]
		SF.CheckType( v, "table" )
		poly[ i ] = SF.Polygon.Checkvertex( v )
	end
	return wrappedpoly
end


----------------------------
-- POLYGON TRANSFORMS
----------------------------

--- Moves the polygon around each axis.
-- @param xAmount Number of screen units to move on the X Axis.
-- @param yAmount Number of screen units to move on the Y Axis.
-- @return Self, for use in chaining transforms.
function poly_methods:Translate( xAmount, yAmount )
	SF.CheckType( self, poly_metamethods )
	SF.CheckType( xAmount, "number" )
	SF.CheckType( yAmount, "number" )
	
	local poly = unwrappoly( self )
	
	for i = 1, #poly do
		poly[ i ].x = poly[ i ].x + xAmount
		poly[ i ].y = poly[ i ].y + yAmount
	end
	
	return self
end

--- Moves a copy of the polygon around each axis.
-- @param xAmount Number of screen units to move on the X Axis.
-- @param yAmount Number of screen units to move on the Y Axis.
-- @return Translated Polygon Copy
function poly_methods:GetTranslated( xAmount, yAmount )
	SF.CheckType( self, poly_metamethods )
	SF.CheckType( xAmount, "number" )
	SF.CheckType( yAmount, "number" )
	
	local verts = {}
	local poly = unwrappoly( self )
	
	for i = 1, #poly do
		verts[ i ] = tableCopy(poly[ i ])
		
		verts[ i ].x = poly[ i ].x + xAmount
		verts[ i ].y = poly[ i ].y + yAmount
	end
	
	return wrappoly( verts )
end

--- Scales a polygon in each axis.
-- @param xScale Amount to scale by on the X axis.
-- @param yScale Amount to scale by on the Y axis.
-- @return Self, for use in chaining transforms.
function poly_methods:Scale( xScale, yScale )
	SF.CheckType( self, poly_metamethods )
	SF.CheckType( xScale, "number" )
	SF.CheckType( yScale, "number" )
	
	local poly = unwrappoly( self )
	
	for i = 1, #poly do
		poly[ i ].x = poly[ i ].x * xScale
		poly[ i ].y = poly[ i ].y * yScale
	end
	
	return self
end

--- Scales a copy of the polygon in each axis.
-- @param xScale Amount to scale by on the X axis.
-- @param yScale Amount to scale by on the Y axis.
-- @return Scaled Polygon Copy
function poly_methods:GetScaled( xScale, yScale )
	SF.CheckType( self, poly_metamethods )
	SF.CheckType( xScale, "number" )
	SF.CheckType( yScale, "number" )
	
	local verts = {}
	local poly = unwrappoly( self )
	
	for i = 1, #poly do
		verts[ i ] = tableCopy(poly[ i ])
		
		verts[ i ].x = poly[ i ].x * xScale
		verts[ i ].y = poly[ i ].y * yScale
	end
	
	return wrappoly( verts )
end

--- Rotates a polygon around a point. If no point given, rotates around origin.
-- @param angle Amount to rotate in degrees.
-- @param xOrigin (Optional) Point on the X axis at which to rotate around.
-- @param yOrigin (Optional) Point on the Y axis at which to rotate around.
-- @return Self, for use in chaining transforms.
function poly_methods:Rotate( angle, xOrigin, yOrigin )
	SF.CheckType( angle, "number" )
	local xPoint = SF.CheckType( xOrigin, "number", 0, 0 )
	local yPoint = SF.CheckType( yOrigin, "number", 0, 0 )
	
	local poly = unwrappoly( self )
	
	local sin = sine( toRad( angle ) )
	local cos = cosine( toRad( angle ) )
	
	for i = 1, #poly do
		local newX = poly[ i ].x
		local newY = poly[ i ].y
		
		--Translate to origin
		newX = newX - xPoint
		newY = newY - yPoint
		
		--Apply Rotation
		local oldX = newX
		local oldY = newY
		newX = (oldX * cos) - (oldY * sin)
		newY = (oldX * sin) + (oldY * cos)
		
		--Detranslate
		newX = newX + xPoint
		newY = newY + yPoint
	
		poly[ i ].x = newX
		poly[ i ].y = newY
	end
	
	return self
end

--- Rotates a copy of the polygon around a point. If no point given, rotates around origin.
-- @param angle Amount to rotate in degrees.
-- @param xOrigin (Optional) Point on the X axis at which to rotate around.
-- @param yOrigin (Optional) Point on the Y axis at which to rotate around.
-- @return Scaled Polygon Copy
function poly_methods:GetRotated( angle, xOrigin, yOrigin )
	SF.CheckType( angle, "number" )
	local xPoint = SF.CheckType( xOrigin, "number", 0, 0 )
	local yPoint = SF.CheckType( yOrigin, "number", 0, 0 )
	
	local verts = {}
	local poly = unwrappoly( self )
	
	local sin = sine( toRad( angle ) )
	local cos = cosine( toRad( angle ) )
	
	for i = 1, #poly do
		verts[ i ] = tableCopy(poly[ i ])
		
		local newX = poly[ i ].x
		local newY = poly[ i ].y
		
		--Translate to origin
		newX = newX - xPoint
		newY = newY - yPoint
		
		--Apply Rotation
		local oldX = newX
		local oldY = newY
		newX = (oldX * cos) - (oldY * sin)
		newY = (oldX * sin) + (oldY * cos)
		
		--Detranslate
		newX = newX + xPoint
		newY = newY + yPoint
	
		verts[ i ].x = newX
		verts[ i ].y = newY
	end
	
	return wrappoly( verts )
end

----------------------------
-- PRIMITIVE GENERATION
----------------------------

--- Generates a circle or ovoid primitive with UV Mapping.
-- Origin is in the center of the circle.
-- @param xRadius (Optional) Maximum radius on the X axis.
-- @param yRadius (Optional) Maximum radius on the Y axis.
-- @param sideCount (Optional) Number of sides the circle has (clamped from 3 to 360). More sides = smoother circles, but also slower rendering.
-- @return Polygon primitive
function poly_library.CreateCircle ( xRadius, yRadius, sideCount )
	local xRad = SF.CheckType( xRadius, "number", 0, 1 )
	local yRad = SF.CheckType( yRadius, "number", 0, 1 )
	local sides = SF.CheckType( sideCount, "number", 0, 90 )
	
	local step = floor( 360 / clamp( sides, 3, 360 ) )
	
	local verts = {}
	local poly = wrappoly( verts )
	local index = 1
	for i = 0, 360, step do
		local x = cosine( toRad( i ) )
		local y = sine( toRad( i ) )
		local uMap = ( x * 0.5 ) + 0.5
		local vMap = ( y * 0.5 ) + 0.5
		local xPoint = x * xRad
		local yPoint = y * yRad

		verts[ index ] = { x = xPoint, y = yPoint, u = uMap, v = vMap }
		index = index + 1
	end

	return poly
end