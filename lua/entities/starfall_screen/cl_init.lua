include( "shared.lua" )

ENT.RenderGroup = RENDERGROUP_OPAQUE

include( "starfall/SFLib.lua" )
assert( SF, "Starfall didn't load correctly!" )

local context

hook.Add( "sf_libs_loaded", "sf_create_screen_context", function ()
	if SF.Libraries.Local[ "render" ] then
		context = SF.CreateContext( nil, nil, nil, SF.Libraries.CreateLocalTbl{ "render" } )
	else
		context = SF.CreateContext()
	end
end )

surface.CreateFont( "Starfall_ErrorFont", {
	font = "arial",
	size = 26,
	weight = 200
} )

function ENT:Initialize ()
	self.BaseClass.Initialize( self )
	self.GPU = GPULib.WireGPU( self )
end

function ENT:OnRemove ()
	self.GPU:Finalize()
	self.BaseClass.OnRemove(self)
end

function ENT:Error ( msg, traceback )
	msg = self.BaseClass.Error( self, msg, traceback )
	
	-- Process error message
	self.error = {}
	self.error.orig = msg
	self.error.source, self.error.line, self.error.msg = string.match( msg, "%[@?SF:(%a+):(%d+)](.+)$" )

	if not self.error.source or not self.error.line or not self.error.msg then
		self.error.source, self.error.line, self.error.msg = nil, nil, msg
	else
		self.error.msg = string.TrimLeft( self.error.msg )
	end
	
	if self.instance then
		self.instance:deinitialize()
		self.instance = nil
	end
	
	--self:SetOverlayText( "Starfall Screen\nInactive ( Error )" )
end

function ENT:CodeSent ( files, main, owner )
	if not files or not main or not owner then return end
	if self.instance then self.instance:deinitialize() end
	self.owner = owner
	local ok, instance = SF.Compiler.Compile( files, context, main, owner, { entity = self, render = {} } )
	if not ok then self:Error( instance ) return end
	
	instance.runOnError = function ( inst, ... ) self:Error( ... ) end
	
	self.instance = instance
	instance.data.render.gpu = self.GPU
	instance.data.render.matricies = 0
	local ok, msg, traceback = instance:initialize()
	if not ok then self:Error( msg, traceback ) end
	
	if not self.instance then return end
	
	local data = instance.data
	
	function self.renderfunc ()
		if self.instance then
			data.render.isRendering = true
			draw.NoTexture()
			surface.SetDrawColor( 255, 255, 255, 255 )

			self:runScriptHook( "render" )

			if data.render.usingRT then
				render.PopRenderTarget()
				data.render.usingRT = false
			end
			data.render.isRendering = nil
			
		elseif self.error then
			surface.SetTexture( 0 )
			surface.SetDrawColor( 0, 0, 0, 120 )
			surface.DrawRect( 0, 0, 512, 512 )
			
			draw.DrawText( "Error occurred in Starfall Screen:", "Starfall_ErrorFont", 32, 16, Color( 0, 255, 255, 255 ) ) -- Cyan
			draw.DrawText( tostring( self.error.msg ), "Starfall_ErrorFont", 16, 80, Color( 255, 0, 0, 255 ) )
			if self.error.source and self.error.line then
				draw.DrawText( "Line: " .. tostring( self.error.line), "Starfall_ErrorFont", 16, 512 - 16 * 7, Color( 255, 255, 255, 255 ) )
				draw.DrawText( "Source: " .. self.error.source, "Starfall_ErrorFont", 16, 512 - 16 * 5, Color( 255, 255, 255, 255 ) )
			end
			draw.DrawText( "Press USE to copy to your clipboard", "Starfall_ErrorFont", 512 - 16 * 25, 512 - 16 * 2, Color( 255, 255, 255, 255 ) )
			self.renderfunc = nil
		end
	end
end

function ENT:Draw ()
	baseclass.Get( self.Base ).Draw( self )
	self:DrawModel()
	Wire_Render( self )
	
	if self.renderfunc then
		self.GPU:RenderToGPU( self.renderfunc )
	end
	
	self.GPU:Render()
end
