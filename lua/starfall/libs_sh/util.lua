-------------------------------------------------------------------------------
-- Shared Util Library functions
-------------------------------------------------------------------------------

SF.Util = {}
--- Util Library - It has all of the "special" garry lua functions. 
-- @shared
local util_library, _ = SF.Libraries.Register( "util" )

SF.Util.Library = util_library

local ewrap
local eunwrap

local function postload ()
    ewrap = SF.Entities.Wrap
    eunwrap = SF.Entities.Unwrap
end
SF.Libraries.AddHook( "postload", postload )

--#region Local Functions

---Unwrap a sequential table of entities
---@param Filter table|Entity
---@return table
local function ConvertFilter(Filter)
    if eunwrap(Filter) then
        return { Filter }
    else
        local UnwrappedFilter = {}
        for _, Ent in ipairs(Filter) do
            local UnwrappedEnt = eunwrap(Ent)
            if UnwrappedEnt then
                table.insert(UnwrappedFilter, UnwrappedEnt)
            end
        end

        return UnwrappedFilter
    end
end


--#endregion
--#region Shared Util Functions
---Get an aim vector from a 2D screen position.
---@param ViewAngles Angle #The world angle to calculate the vector from
---@param ViewFOV number #The FOV of the 2D screen
---@param x number #The X axis of the position on the screen
---@param y number #The y axis of the position on the screen
---@param scrWidth number #The width of the screen
---@param scrHeight number #The height of the screen
---@return Vector AimVector #The calculated aim vector
function util_library.AimVector(ViewAngles, ViewFOV, x, y, scrWidth, scrHeight)
    return SF.WrapObject(util.AimVector(SF.UnwrapObject(ViewAngles), ViewFOV, x, y, scrWidth, scrHeight))
end

---Quickly generate a trace table that starts at the player's view position and ends 32768 untils along a specified direction vector
---@param ply Player
---@param dir Vector
---@return table TraceData
function util_library.GetPlayerTrace(ply, dir)
    return SF.Sanitize(util.GetPlayerTrace(eunwrap(ply), SF.UnwrapObject(dir)))
end

---Perform a trace from `TraceData.start` to `TraceData.endpos`
---@param TraceData table
---@return table TraceResult
function util_library.TraceLine(TraceData)
    -- Check if the player has permission to perform a trace, yell at them if they don't
    if not SF.Permissions.check( SF.instance.player, nil, "trace" ) then SF.throw( "Insufficient permissions", 2 ) end

    -- Make sure the trace data's fields are the right types
    SF.CheckType(TraceData.start, SF.Types[ "Vector" ])
    SF.CheckType(TraceData.endpos, SF.Types[ "Vector" ])

    -- Unwrap the fields
    TraceData.start = SF.UnwrapObject(TraceData.start)
    TraceData.endpos = SF.UnwrapObject(TraceData.endpos)
    TraceData.filter = ConvertFilter(SF.CheckType(TraceData.filter, "table", 0, {}))
    if TraceData.mask then TraceData.mask = SF.CheckType( TraceData.mask, "number" ) end

    return SF.Sanitize(util.TraceLine(TraceData))
end

-- TODO: Move Trace functions into here too
--#endregion
