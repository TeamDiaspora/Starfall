
/******************************************************************
****************          Fehas Constraint Extension          ******************
******************************************************************/

/***********************************************************************************************************************
Credits


Myself - I am coding the stuff

KJIAD - is the reason I added strings returning if it was succesfull or not. Also helped me alot with unConstrain.

DuneD - Helped alot with minor (and some major) stuff, aswell as helped me to grasp the basics. He also uploaded this to the McBuilds-server, so I could test it in mp (and notice I needed pp).

JeremyDeath - He is your saint, as he made the pp finally work for me.

The guys who created the entities, color and constraint part for egate2 functions, as I used them to learn how to code egate2 functions.

gmod lua wiki - they got a list of functions you can use.

F�hrball - Added in Freezing/Unfreezing functions
***********************************************************************************************************************/

//An egate function for welding, put the file in the custom folder.

//The function that welds
local function Weld(self, ent1, ent2, Force, Nocollide)
	//Makes non-valid entities into world
	if (!IsValid(ent1)) then
		ent1 = game.GetWorld()
	end
	if (!IsValid(ent2)) then
		ent2 = game.GetWorld()
	end
	//Prop-Protection
	if ent1 != game.GetWorld() and (!isOwner(self, ent1) and !self.player:IsAdmin()) then return "Not owner" end			//check ownership or admin
	if ent2 != game.GetWorld() and (!isOwner(self, ent2) and !self.player:IsAdmin()) then return "Not owner" end			//check ownership or admin
	
	if (ent1 and ent2) then
		constraint.Weld( ent1, ent2, 0, 0, Force, Nocollide )			//Welds
		return ""
	end
	return "Failed to weld"
end


//Makes a function for egate2 named weld, and you set what entities it weld, force until break (0 dont break) and welded nocollide until break (boolean)
registerFunction("weld", "e:enn", "s", function(self, args)
	local op1, op2, op3, op4 = args[2], args[3], args[4], args[5]
	local rv1, rv2, rv3, rv4 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4)
	return Weld(self,rv1,rv2,rv3,rv4)
end)

//Makes a function for egate2 named weld, and you set what entities it weld and force until break (0 dont break) . It is made welded nocollide until break (boolean).
registerFunction("weld", "e:en", "s", function(self, args)
	local op1, op2, op3 = args[2], args[3], args[4]
	local rv1, rv2, rv3= op1[1](self, op1), op2[1](self, op2), op3[1](self, op3)
	return Weld(self,rv1,rv2,rv3,1)
end)

//Makes a function for egate2 named weld, and you set what entities it weld. It wont break (0 dont break) . It is made welded nocollide until break (boolean).
registerFunction("weld", "e:e", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return Weld(self,rv1,rv2,0,1)
end)

/********
The same three functions as above, except it is weld(e,n,n) in egate, not e:weld(e,n,n). It welds the entity to the egate.
********/

//Makes a function for egate2 named weld, and you set what entity it weld, force until break (0 dont break) and welded nocollide until break (boolean). It welds the entity to the egate.
registerFunction("weld", "enn", "s", function(self, args)
	local op1, op2, op3 = args[2], args[3], args[4]
	local rv1, rv2, rv3, rv4 = self.entity, op1[1](self, op1), op2[1](self, op2), op3[1](self, op3)
	return Weld(self,rv1,rv2,rv3,rv4)
end)

//Makes a function for egate2 named weld, and you set what entity it weld and force until break (0 dont break) . It is made welded nocollide until break (boolean). It welds the entity to the egate.
registerFunction("weld", "en", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2, rv3, rv4 = self.entity, op1[1](self, op1), op2[1](self, op2)
	return Weld(self,rv1,rv2,rv3,1)
end)

//Makes a function for egate2 named weld, and you set what entity it weld. It wont break (0 dont break) . It is made welded nocollide until break (boolean). It welds the entity to the egate.
registerFunction("weld", "e", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return Weld(self,self,rv1,0,1)
end)

/********
The same six functions as above, except it is e:weld(n,n) in egate, not e:weld(e,n,n) or weld(e,n,n). It welds the entity to the egate.
********/

//Makes a function for egate2 named weld, and you set what entity it weld, force until break (0 dont break) and welded nocollide until break (boolean). It welds the entity to the egate.
registerFunction("weld", "e:nn", "s", function(self, args)
	local op1, op2, op3 = args[2], args[3], args[4]
	local rv1, rv2, rv3, rv4 = self.entity, op1[1](self, op1), op2[1](self, op2), op3[1](self, op3)
	return Weld(self,rv1,rv2,rv3,rv4)
end)

//Makes a function for egate2 named weld, and you set what entity it weld and force until break (0 dont break) . It is made welded nocollide until break (boolean). It welds the entity to the egate.
registerFunction("weld", "e:n", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2, rv3 = self.entity, op1[1](self, op1), op2[1](self, op2)
	return Weld(self,rv1,rv2,rv3,1)
end)

//Makes a function for egate2 named weld, and you set what entity it weld. It wont break (0 dont break) . It is made welded nocollide until break (boolean). It welds the entity to the egate.
registerFunction("weld", "e:", "s", function(self, args)
	local op1 = args[2]
	local rv1, rv2 = self.entity, op1[1](self, op1)
	return Weld(self,rv1,rv2,0,1)
end)

/************************************************************************/

//An egate function for unwelding, put the file in the custom folder.

//The function that unwelds
local function UnWeld(self, ent1, ent2)
	//Makes non-valid entities into world
	if (!IsValid(ent1)) then
		ent1 = game.GetWorld()
	end
	//Prop-Protection
	if (ent1 != game.GetWorld() and !isOwner(self, ent1)) then return "Not owner" end			//check ownership
	//It has cosntraints?
	if (!constraint.HasConstraints(ent1)) then return "The first entity has no constraints" end

	if (ent2 != "null") then
		if (!IsValid(ent2)) then
			ent2 = game.GetWorld()
		end
		if (ent2 != game.GetWorld()) then
			if (!isOwner(self, ent2)) then return "Not owner" end			//check ownership
			if (!constraint.HasConstraints(ent2)) then return "The second entity has no constraints" end
		end
	else
		ent2 = nil
	end
		
	if (ent1 and ent2) then
		local Table = constraint.FindConstraints(ent1,"Weld")
		for _, ent in pairs(Table) do
			if (ent.Ent2 == ent2 or ent.Ent1 == ent2) then
				ent.Constraint:Remove()
				return ""
			end
		end
	else	
		constraint.RemoveConstraints( ent1, "Weld" )			//UnWelds
		return ""
	end
	return "Failed to unweld"
end

//Makes a function for egate2 named unWeld and you set what entities to unweld with eachothers.
registerFunction("unWeld", "e:e", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return UnWeld(self,rv1,rv2)
end)

//Makes a function for egate2 named unWeld and you set what entities to unweld with eachothers.
registerFunction("unWeld", "ee", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return UnWeld(self,rv1,rv2)
end)

//Makes a function for egate2 named unWeld and you set what entitie to unweld to everything its welded to.
registerFunction("unWeld", "e:", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return UnWeld(self,rv1,"null")
end)

//Makes a function for egate2 named unWeld and you set what entitie to unweld to everything its welded to.
registerFunction("unWeld", "e", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return UnWeld(self,rv1,"null")
end)

//Makes a function for egate2 named unWeld and unwelds the egate to everything its welded to.
registerFunction("unWeld", "", "s", function(self, args)
	return UnWeld(self,self,"null")
end)

/************************************************************************/

//An egate function for unConstraining, put the file in the custom folder.

//The function that unConstrain
local function unConstrain(self, ent1, ent2, Type)
	//Makes non-valid entities into world
	if (!IsValid(ent1)) then
		ent1 = game.GetWorld()
	end
	//Prop-Protection
	if (ent1 != game.GetWorld() and !isOwner(self, ent1)) then return "Not owner" end			//check ownership
	//It has cosntraints?
	if (!constraint.HasConstraints(ent1)) then return "The first entity has no constraints" end

	if (ent2 != "null") then
		if (!IsValid(ent2)) then
			ent2 = game.GetWorld()
		end
		if (ent2 != game.GetWorld()) then
			if (!isOwner(self, ent2)) then return "Not owner" end			//check ownership
			if (!constraint.HasConstraints(ent2)) then return "The second entity has no constraints" end
		end
	else
		ent2 = nil
	end
	
	if (Type) then // Fix Type naming
		local Type = Type:sub(1,1):upper() .. Type:sub(2):lower()
		if (Type == "Nocollide") then Type = "NoCollide" end
		if (Type == "Advballsocket") then Type = "AdvBallsocket" end
		if (Type == "All") then Type = "" end
	end
	
	if (ent1 and ent2) then
		for _, const in pairs(ent1.Constraints) do
			if (const.Ent2 == ent2 or const.Ent1 == ent2) then
				if (Type) then
					if (Type == const.Type) then const:Remove() end
				else
					const:Remove()
				end
			end
		end 
		return ""
	else
		if (Type) then
			constraint.RemoveConstraints(ent1,Type)
		else
			constraint.RemoveAll(ent1)
		end
		return ""
	end
	return "Failed to unconstrain"
end

//Makes a function for egate2 named unConstrain and you set what entities to unConstrain with eachothers aswell as what constraint to remove.
registerFunction("unConstrain", "e:es", "s", function(self, args)
	local op1, op2, op3 = args[2], args[3], args[4]
	local rv1, rv2, rv3 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3)
	return unConstrain(self,rv1,rv2,rv3)
end)

//Makes a function for egate2 named unConstrain and you set what entities to unConstrain with eachothers aswell as what constraint to remove.
registerFunction("unConstrain", "ees", "s", function(self, args)
	local op1, op2, op3 = args[2], args[3], args[4]
	local rv1, rv2, rv3 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3)
	return unConstrain(self,rv1,rv2,rv3)
end)

//Makes a function for egate2 named unConstrain and you set what entities to unConstrain with eachothers
registerFunction("unConstrain", "e:e", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return unConstrain(self,rv1,rv2)
end)

//Makes a function for egate2 named unConstrain and you set what entities to unConstrain with eachothers.
registerFunction("unConstrain", "ee", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return unConstrain(self,rv1,rv2)
end)

//Makes a function for egate2 named unConstrain and you set what entities to unConstrain with eachothers aswell as what constraint to remove.
registerFunction("unConstrain", "e:s", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return unConstrain(self,rv1,0,rv2)
end)

//Makes a function for egate2 named unConstrain and you set what entities to unConstrain with eachothers aswell as what constraint to remove.
registerFunction("unConstrain", "es", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return unConstrain(self,rv1,0,rv2)
end)

//Makes a function for egate2 named unConstrain and you set what entitie to unConstrain to everything its unConstrained to.
registerFunction("unConstrain", "e:", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return unConstrain(self,rv1,"null")
end)

//Makes a function for egate2 named unConstrain and you set what entitie to unConstrain to everything its unConstrained to.
registerFunction("unConstrain", "e", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return unConstrain(self,rv1,"null")
end)

//Makes a function for egate2 named unConstrain and unwelds the egate to everything its unConstrained to aswell as what constraint to remove.
registerFunction("unConstrain", "s", "s", function(self, args)
	local op1 = args[2], args[3]
	local rv1 = op1[1](self, op1)
	return unConstrain(self,self,"null",rv1)
end)

//Makes a function for egate2 named unConstrain and unwelds the egate to everything its unConstrained to.
registerFunction("unConstrain", "", "s", function(self, args)
	local rv1 = self.entity
	return unConstrain(self,self,"null")
end)

/************************************************************************/

//An egate function for roping, put the file in the custom folder.

//The function that welds
local function Rope(self, ent1, ent2, LPos1, LPos2, addlength, Force, width, material, rigid)
	//Makes non-valid entities into world
	if (!IsValid(ent1)) then
		ent1 = game.GetWorld()
	end
	if (!IsValid(ent2)) then
		ent2 = game.GetWorld()
	end
	//Prop-Protection
	if (ent1 != game.GetWorld() and !isOwner(self, ent1)) then return "Not owner" end			//check ownership
	if (ent2 != game.GetWorld() and !isOwner(self, ent2)) then return "Not owner" end			//check ownership
	
	if (!LPos1 and !LPos2) then
		LPos1 = Vector(0,0,0)
		LPos2 = Vector(0,0,0)
	else
		LPos1 = Vector(LPos1[1],LPos1[2],LPos1[3])
		LPos2 = Vector(LPos2[1],LPos2[2],LPos2[3])
	end
	if (ent1 and ent2) then			//Check if it really is entities.
		local length = ent1:LocalToWorld(LPos1):Distance(ent2:LocalToWorld(LPos2))
		if (rigid == 0) then
			rigid = false
		end
		constraint.Rope(ent1, ent2, 0, 0, LPos1, LPos2, length, addlength, Force, width, material, rigid)			//Ropes
		return ""
	end
	return "Failed to rope"
end


//Makes a function for egate2 named rope, and you set what entities it rope, force until break (0 dont break) and welded nocollide until break (boolean)
registerFunction("rope", "e:evvnnnsn", "s", function(self, args)
	local op1, op2, op3, op4, op5, op6, op7, op8, op9 = args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10]
	local rv1, rv2, rv3, rv4, rv5, rv6, rv7, rv8, rv9 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4), op5[1](self, op5), op6[1](self, op6), op7[1](self, op7), op8[1](self, op8), op9[1](self, op9)
	return Rope(self,rv1,rv2,rv3,rv4,rv5,rv6,rv7,rv8,rv9)
end)

//Makes a function for egate2 named rope, and you set what entities it rope, force until break (0 dont break) and welded nocollide until break (boolean)
registerFunction("rope", "e:ennn", "s", function(self, args)
	local op1, op2, op3, op4, op5 = args[2], args[3], args[4], args[5], args[6]
	local rv1, rv2, rv3, rv4, rv5 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4), op5[1](self, op)
	return Rope(self,rv1,rv2,false,false,rv3,rv4,rv5,"cable/rope",false)
end)

//Makes a function for egate2 named rope, and you set what entities it rope, force until break (0 dont break) and welded nocollide until break (boolean)
registerFunction("rope", "e:evv", "s", function(self, args)
	local op1, op2, op3, op4 = args[2], args[3], args[4], args[5]
	local rv1, rv2, rv3, rv4 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4)
	return Rope(self,rv1,rv2,rv3,rv4,0,0,1,"cable/rope",false)
end)


//Makes a function for egate2 named rope, and you set what entities it rope, force until break (0 dont break) and welded nocollide until break (boolean)
registerFunction("rope", "e:e", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return Rope(self,rv1,rv2,false,false,0,0,1,"cable/rope",false)
end)

//Makes a function for egate2 named rope, and you set what entities it rope, force until break (0 dont break) and welded nocollide until break (boolean)
registerFunction("rope", "e", "s", function(self, args)
	local op1, op2 = args[2]
	local rv1, rv2 = op1[1](self, op1), self.entity
	return Rope(self,rv1,rv2,false,false,0,0,1,"cable/rope",false)
end)

/************************************************************************/

//An egate function for nocolliding, put the file in the custom folder.

//The function that nocollides.
local function NoCollide(self, ent1, ent2)
	//Makes non-valid entities into world
	if (!IsValid(ent1)) then return "First entity is not valid" end
	//Prop-Protection
	if (!isOwner(self, ent1)) then return "Not owner of first entity" end			//check ownership
	
	if (ent2 != "null") then
		if (!IsValid(ent2)) then return "Second entity is not valid" end
		if (ent2) then
			if (!isOwner(self, ent2)) then return "Not owner of second entity" end			//check ownership
		end
	else
		ent2 = nil
	end
	
	if (ent1 and !ent2) then
		ent1:SetCollisionGroup( COLLISION_GROUP_WORLD )			//Nocollides to all
		return ""
	elseif (ent1 and ent2) then
		local nocollide = constraint.NoCollide( ent1, ent2, 0, 0 )			//Nocollides
		return ""
	end
	return "Failed to nocollide"
end


//Makes a function for egate2 named noCollide and you set what entitie to nocollide with another entitie.
registerFunction("noCollide", "e:e", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return NoCollide(self,rv1,rv2)
end)

//Makes a function for egate2 named noCollide and you set what entitie to nocollide with another entitie.
registerFunction("noCollide", "ee", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return NoCollide(self,rv1,rv2)
end)

//Makes a function for egate2 named noCollide and you set what entitie to nocollide to everything but world.
registerFunction("noCollide", "e:", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return NoCollide(self,rv1,"null")
end)

//Makes a function for egate2 named noCollide and you set what entitie to nocollide to everything but world.
registerFunction("noCollide", "e", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return NoCollide(self,rv1,"null")
end)

//Makes a function for egate2 named noCollide and nocollides the egate to everything but world.
registerFunction("noCollide", "", "s", function(self, args)
	return NoCollide(self,self,"null")
end)

/************************************************************************/

//An egate function for removing nocollide, put the file in the custom folder.

//The function that remove nocollide.
local function Collide(self, ent1, ent2)
	//Makes non-valid entities into world
	if (!IsValid(ent1)) then return "First entity is not valid" end
	//Prop-Protection
	if (!isOwner(self, ent1)) then return "Not owner of first entity" end			//check ownership
	
	if (ent2 != "null") then
		if (!IsValid(ent2)) then return "Second entity is not valid" end
		if (!isOwner(self, ent2)) then return "Not owner of second entity" end			//check ownership
	else
		ent2 = nil
	end
	
	if (ent1 and ent2) then
		//Does ent1 have constraints?
		if (!constraint.HasConstraints(ent1)) then return "The first entity has no constraints" end
		local Table = constraint.FindConstraints(ent1,"NoCollide")
		for _, ent in pairs(Table) do
			if (ent.Ent2 == ent2 or ent.Ent1 == ent2) then
				ent.Constraint:Remove()
				local weld = constraint.Weld( ent1, ent2, 0, 0, 0, 1 )			//Welds
				weld:Remove()
				return ""
			end
		end
	elseif (ent1) then
		ent1:SetCollisionGroup( COLLISION_GROUP_NONE )			//Nocollides to all
		return ""
	end
	return "Failed to nocollide"	
end


//Makes a function for egate2 named collide and you set what entitie to remove nocollide from.
registerFunction("collide", "e:e", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return Collide(self,rv1,rv2)
end)

//Makes a function for egate2 named collide and you set what entitie to remove nocollide from.
registerFunction("collide", "ee", "s", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	return Collide(self,rv1,rv2)
end)

//Makes a function for egate2 named collide and you set what entitie to remove nocollide from.
registerFunction("collide", "e:", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return Collide(self,rv1,"null")
end)

//Makes a function for egate2 named collide and you set what entitie to remove nocollide from.
registerFunction("collide", "e", "s", function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	return Collide(self,rv1,"null")
end)

//Makes a function for egate2 named collide and removes nocollide from the egate.
registerFunction("collide", "", "s", function(self, args)
	local rv1 = self.entity
	return Collide(self,rv1,"null")
end)

-------------------------------------------------------------------------------------------------

//Extra Prop Stuff by F�hrball

e2function void entity:freeze()
	if (!validPhysics(this)) then return end
	if(!isOwner(self, this)) then return end
	if(!this:IsWorld()) then
		local phys = this:GetPhysicsObject()
		phys:EnableMotion(false)
		phys:Wake()
	end
end

e2function void entity:unFreeze()
	if (!validPhysics(this)) then return end
	if(!isOwner(self, this)) then return end
	if(!this:IsWorld()) then
		local phys = this:GetPhysicsObject()
		phys:EnableMotion(true)
		phys:Wake()
	end
end

e2function void entity:shadowEnable(number enabled)
	if (!IsValid(this)) then return end
	if(!isOwner(self, this)) then return end
	if(!this:IsWorld()) then
		if enabled > 0 then
			this:DrawShadow(true)
		else
			this:DrawShadow(false)
		end
	end
end