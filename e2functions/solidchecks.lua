/*
e2function number aimAt(vector Origin, vector OriginDirVec, vector Target, number Accuracy)
	return Aim(Origin,OriginDirVec,Target,Accuracy)
end

*/

e2function number entity:isSolid()
	if this == nil then return 0 end
	
	return this:GetSolid() and !this:GetCollisionGroup()
end