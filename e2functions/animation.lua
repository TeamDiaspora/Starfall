--[[***********************************
Animation Control Functions
By: Jeremydeath
8-21-08
*************************************]]
e2function void entity:sequenceSet(number Sequence)
	if not IsValid(this) then return nil end
	if not isOwner(self,this) then return nil end
	if Sequence < 0 then return nil end
	if not this.AutomaticFrameAdvance then
		this.AutomaticFrameAdvance = true
	end
	this:SetSequence(math.floor(Sequence))
end

e2function void entity:sequenceReset(number Sequence)
	if not IsValid(this) then return nil end
	if not isOwner(self,this) then return nil end
	if Sequence < 0 then return nil end
	if not this.AutomaticFrameAdvance then
		this.AutomaticFrameAdvance = true
	end
	this:ResetSequence(math.floor(Sequence))
end

e2function void entity:sequenceSet(number Sequence, number Speed)
	if not IsValid(this) then return nil end
	if not isOwner(self,this) then return nil end
	if Sequence < 0 then return nil end
	if not this.AutomaticFrameAdvance then
		this.AutomaticFrameAdvance = true
	end
	this:SetSequence(math.floor(Sequence))
	this:SetPlaybackRate( math.max(Speed,0) )
end

e2function void entity:sequenceReset(number Sequence, number Speed)
	if not IsValid(this) then return nil end
	if not isOwner(self,this) then return nil end
	if Sequence < 0 then return nil end
	if not this.AutomaticFrameAdvance then
		this.AutomaticFrameAdvance = true
	end
	this:ResetSequence(math.floor(Sequence))
	this:SetPlaybackRate( math.max(Speed,0) )
end

e2function number entity:sequenceGet()
	if not IsValid(this) then return 0 end
	if not isOwner(self,this) then return 0 end
	return this:GetSequence() or 0
end

e2function number entity:sequenceLookup(string SequenceName)
	if not IsValid(this) then return 0 end
	if not isOwner(self,this) then return 0 end
	if(string.Trim(SequenceName) == "") then
		return 0
	else
		return this:LookupSequence(string.Trim(SequenceName)) or 0
	end
end


//--E2Helper stuff (Added by Steeveeo)
if E2Helper then

E2Helper.Descriptions["sequenceSet"] = "Plays an animation on the entity (with optional speed).	By: Jeremydeath"
E2Helper.Descriptions["sequenceReset"] = "Plays an animation on the entity (with optional speed).	By: Jeremydeath"
E2Helper.Descriptions["sequenceGet"] = "Gets the currently playing animation of the entity.	By: Jeremydeath"
E2Helper.Descriptions["sequenceLookup"] = "Returns the animation number for the specified name (0 if not found).	By: Jeremydeath"

end
