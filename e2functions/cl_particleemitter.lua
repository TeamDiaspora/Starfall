//-------------------------------------
// PARTICLE EMITTER: Clientside
//		Author: Steeveeo
//
//	This E2 Extension is to allow E2
//	users to create CLuaEmitter effects
//	wherever needed (Note: this does
//	not work for PCF Particles!).
//-------------------------------------

Emitters = {}
Particles = {}
local requestedEmitters = false
local receivedEmitters = true

//Blocked Materials List
// - Note: Don't include extensions, they're optional anyway.
local MatBlacklist = {
	"effects/prisonmap_disp",
    "effects/ar2_altfire1",
    "effects/combineshield/comshieldwall"
}

//Clientside Settings
CreateClientConVar("wire_particles_maxparticles", 300, true, false)
CreateClientConVar("wire_particles_enable", 1, true, false)
CreateClientConVar("wire_particles_maxdistance", 10000, true, false)

//Send a request to the server for Emitters
net.Start("Emitter - Client Requesting Sync")
	requestedEmitters = true
net.SendToServer()

//Server finished sending Emitters
net.Receive("Emitter - Client Sync Complete", function(len)
	receivedEmitters = true
end)


//------------------------
// EMITTER CREATION
//------------------------

local function createEmitter(ent, index, pos)
	if !IsValid(ent) then return end
	
	//Create necessary tables
	if not Emitters[ent] then Emitters[ent] = {} end
	if not Emitters[ent][index] then Emitters[ent][index] = {} end
	
	//Create Emitter Object
	Emitters[ent][index]["emitter"] = ParticleEmitter(pos, true)
	Emitters[ent][index]["pos"] = pos
	
	//Default Settings
	Emitters[ent][index]["parent"] = nil
	Emitters[ent][index]["rate"] = 1
	Emitters[ent][index]["mat"] = ""
	Emitters[ent][index]["drag"] = 0
	Emitters[ent][index]["bounce"] = 0
	Emitters[ent][index]["canCollide"] = 0
	Emitters[ent][index]["color"] = Vector(255, 255, 255)
	Emitters[ent][index]["lifespan"] = 1
	Emitters[ent][index]["startAlpha"] = 255
	Emitters[ent][index]["endAlpha"] = 255
	Emitters[ent][index]["startSize"] = 10
	Emitters[ent][index]["endSize"] = 10
	Emitters[ent][index]["startLength"] = 10
	Emitters[ent][index]["endLength"] = 10
	Emitters[ent][index]["gravity"] = Vector(0, 0, 0)
	Emitters[ent][index]["roll"] = 0
	Emitters[ent][index]["rollNoise"] = 0
	Emitters[ent][index]["rollSpeed"] = 0
	Emitters[ent][index]["rollSpeedNoise"] = 0
	Emitters[ent][index]["rollDrag"] = 0
	Emitters[ent][index]["rollSpeedMin"] = 0
	Emitters[ent][index]["velocity"] = Vector(0, 0, 0.01)
	Emitters[ent][index]["velNoise"] = Vector(0, 0, 0)
	Emitters[ent][index]["velInherit"] = Vector(0, 0, 0)
	--Emitters[ent][index]["angles"] = Angle(0, 0, 0)
	--Emitters[ent][index]["angVel"] = Angle(0, 0, 0)	//These don't have a use yet.
end

local function destroyEmitter(ent, index)
	if !IsValid(ent) then return end
	
	//Delete tables if necessary
	if not Emitters[ent] then return end
	if Emitters[ent][index] then
		Emitters[ent][index]["emitter"]:Finish()
		Emitters[ent][index] = nil
	end
end


net.Receive("Emitter - Create Emitter", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local pos = net.ReadVector()
	
	createEmitter(ent, index, pos)
end)

net.Receive("Emitter - Destroy Emitter", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	
	destroyEmitter(ent, index)
end)

//--END EMITTER CREATION--


//-----------------------
// EMITTER CONTROL
//-----------------------

local function setEmitterParent(ent, index, parent)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	//Parent existing Emitter
	if parent != nil and parent:IsValid() then
		Emitters[ent][index]["parent"] = parent
		Emitters[ent][index]["parentpos"] = parent:WorldToLocal(Emitters[ent][index]["pos"] or Vector(0, 0, 0))
	//Unparent a parented Emitter
	else
		local oldParent = Emitters[ent][index]["parent"]
		if oldParent != nil and oldParent:IsValid() then
			Emitters[ent][index]["pos"] = oldParent:LocalToWorld(Emitters[ent][index]["parentpos"])
		end
	end
end

local function setEmitterPos(ent, index, pos)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	//Move a parented Emitter
	if Emitters[ent][index]["parent"] != nil and Emitters[ent][index]["parent"]:IsValid() then
		Emitters[ent][index]["parentpos"] = pos
	else
		Emitters[ent][index]["pos"] = pos
	end
end


local function setEmitterRate(ent, index, rate)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["rate"] = rate
end


local function setEmitterMat(ent, index, mat)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	//Check Blacklist
	for k,v in pairs(MatBlacklist) do
		if string.find(mat, v, 1, true) then
			mat = "THIS MATERIAL IS NOW BROKEN"
			break
		end
	end
	
	Emitters[ent][index]["mat"] = mat
end


local function setEmitterDrag(ent, index, drag)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["drag"] = drag
end


local function setEmitterBounce(ent, index, bounceMult)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["bounce"] = bounceMult
end


local function setEmitterCanBounce(ent, index, enable)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["canCollide"] = tobool(enable)
end


local function setEmitterColor(ent, index, color)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["color"] = color
end


local function setEmitterLifespan(ent, index, time)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["lifespan"] = time
end


local function setEmitterStartAlpha(ent, index, alpha)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["startAlpha"] = alpha
end


local function setEmitterEndAlpha(ent, index, alpha)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["endAlpha"] = alpha
end


local function setEmitterStartSize(ent, index, size)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["startSize"] = size
end


local function setEmitterEndSize(ent, index, size)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["endSize"] = size
end


local function setEmitterStartLength(ent, index, length)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["startLength"] = length
end


local function setEmitterEndLength(ent, index, length)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["endLength"] = length
end


local function setEmitterGravity(ent, index, gravity)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["gravity"] = gravity
end


local function setEmitterRoll(ent, index, roll)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["roll"] = roll
end


local function setEmitterRollNoise(ent, index, roll)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["rollNoise"] = roll
end


local function setEmitterRollSpeed(ent, index, rollSpeed)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["rollSpeed"] = rollSpeed
end


local function setEmitterRollSpeedNoise(ent, index, rollSpeed)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["rollSpeedNoise"] = rollSpeed
end


local function setEmitterRollDrag(ent, index, rollDrag)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["rollDrag"] = rollDrag
end


local function setEmitterRollSpeedMin(ent, index, rollSpeed)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["rollSpeedMin"] = rollSpeed
end


local function setEmitterVelocity(ent, index, vel)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["velocity"] = vel
end


local function setEmitterVelocityNoise(ent, index, vel)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["velNoise"] = vel
end


local function setEmitterVelocityInherit(ent, index, mult)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["velInherit"] = mult
end

/* Commented out until I can find a use for these, perhaps an angle override of some sort.
local function setEmitterAngles(ent, index, ang)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["angles"] = ang
end


local function setEmitterAngleVelocity(ent, index, angVel)
	if !IsValid(ent) then return end
	if not Emitters[ent] then return end
	if not Emitters[ent][index] then return end
	
	Emitters[ent][index]["angVel"] = angVel
end
*/



//--HERE THERE BE RECEIVE FUNCTIONS--

net.Receive("Emitter - Set Parent", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local parent = net.ReadEntity()
	
	setEmitterParent(ent, index, parent)
end)


net.Receive("Emitter - Set Pos", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local pos = net.ReadVector()
	
	setEmitterPos(ent, index, pos)
end)


net.Receive("Emitter - Set Emitter Rate", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local rate = net.ReadFloat()
	
	setEmitterRate(ent, index, rate)
end)


net.Receive("Emitter - Set Material", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local mat = net.ReadString()
	
	setEmitterMat(ent, index, mat)
end)


net.Receive("Emitter - Set Drag", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local drag = net.ReadFloat()
	
	setEmitterDrag(ent, index, drag)
end)


net.Receive("Emitter - Set Bounce", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local bounce = net.ReadFloat()
	
	setEmitterBounce(ent, index, bounce)
end)


net.Receive("Emitter - Set Can Collide", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local bounce = net.ReadFloat()
	
	setEmitterCanBounce(ent, index, bounce)
end)


net.Receive("Emitter - Set Color", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local color = net.ReadVector()
	
	setEmitterColor(ent, index, color)
end)


net.Receive("Emitter - Set Die Time", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local time = net.ReadFloat()
	
	setEmitterLifespan(ent, index, time)
end)


net.Receive("Emitter - Set Start Alpha", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local alpha = net.ReadFloat()
	
	setEmitterStartAlpha(ent, index, alpha)
end)


net.Receive("Emitter - Set End Alpha", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local alpha = net.ReadFloat()
	
	setEmitterEndAlpha(ent, index, alpha)
end)


net.Receive("Emitter - Set Start Size", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local size = net.ReadFloat()
	
	setEmitterStartSize(ent, index, size)
end)


net.Receive("Emitter - Set End Size", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local size = net.ReadFloat()
	
	setEmitterEndSize(ent, index, size)
end)


net.Receive("Emitter - Set Start Length", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local length = net.ReadFloat()
	
	setEmitterStartLength(ent, index, length)
end)


net.Receive("Emitter - Set End Length", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local length = net.ReadFloat()
	
	setEmitterEndLength(ent, index, length)
end)


net.Receive("Emitter - Set Gravity", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local gravity = net.ReadVector()
	
	setEmitterGravity(ent, index, gravity)
end)


net.Receive("Emitter - Set Roll", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local roll = net.ReadFloat()
	
	setEmitterRoll(ent, index, roll)
end)


net.Receive("Emitter - Set Roll Noise", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local roll = net.ReadFloat()
	
	setEmitterRollNoise(ent, index, roll)
end)


net.Receive("Emitter - Set Roll Speed", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local roll = net.ReadFloat()
	
	setEmitterRollSpeed(ent, index, roll)
end)


net.Receive("Emitter - Set Roll Speed Noise", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local roll = net.ReadFloat()
	
	setEmitterRollSpeedNoise(ent, index, roll)
end)


net.Receive("Emitter - Set Roll Drag", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local drag = net.ReadFloat()
	
	setEmitterRollDrag(ent, index, drag)
end)


net.Receive("Emitter - Set Roll Speed Min", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local roll = net.ReadFloat()
	
	setEmitterRollSpeedMin(ent, index, roll)
end)


net.Receive("Emitter - Set Velocity", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local vel = net.ReadVector()
	
	setEmitterVelocity(ent, index, vel)
end)


net.Receive("Emitter - Set Velocity Noise", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local vel = net.ReadVector()
	
	setEmitterVelocityNoise(ent, index, vel)
end)


net.Receive("Emitter - Set Velocity Inherit", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local mult = net.ReadVector()
	
	setEmitterVelocityInherit(ent, index, mult)
end)

/* Commented out until I can find a use for these, perhaps an angle override of some sort.
net.Receive("Emitter - Set Angles", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local ang = net.ReadAngle()
	
	setEmitterAngles(ent, index, ang)
end)


net.Receive("Emitter - Set Angle Velocity", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	local ang = net.ReadAngle()
	
	setEmitterAngleVelocity(ent, index, ang)
end)
*/


//Sync Existing Emitter
net.Receive("Emitter - Sync Emitter", function(len)

	local index = net.ReadFloat()
	local ent = net.ReadEntity()
	
	if not Emitters[ent] then Emitters[ent] = {} end
	if not Emitters[ent][index] then Emitters[ent][index] = {} end
	
	local pos = net.ReadVector()
	local parent = net.ReadEntity()
	local parentpos = net.ReadVector()
	local rate = net.ReadFloat()
	local mat = net.ReadString()
	local drag = net.ReadFloat()
	local bounce = net.ReadFloat()
	local canCollide = net.ReadFloat()
	local color = net.ReadVector()
	local life = net.ReadFloat()
	local startAlpha = net.ReadFloat()
	local endAlpha = net.ReadFloat()
	local startSize = net.ReadFloat()
	local endSize = net.ReadFloat()
	local startLength = net.ReadFloat()
	local endLength = net.ReadFloat()
	local gravity = net.ReadVector()
	local roll = net.ReadFloat()
	local rollNoise = net.ReadFloat()
	local rollSpeed = net.ReadFloat()
	local rollSpeedNoise = net.ReadFloat()
	local rollDrag = net.ReadFloat()
	local rollSpeedMin = net.ReadFloat()
	local velocity = net.ReadVector()
	local velNoise = net.ReadVector()
	local velInherit = net.ReadVector()
	--local ang = net.ReadAngle()
	--local angVel = net.ReadAngle()	//These don't have a use yet.
	
	createEmitter(ent, index, pos)
	if IsValid(parent) then
		setEmitterParent(ent, index, parent)
		setEmitterPos(ent, index, parentpos)
	end
	setEmitterRate(ent, index, rate)
	setEmitterMat(ent, index, mat)
	setEmitterDrag(ent, index, drag)
	setEmitterBounce(ent, index, bounce)
	setEmitterCanBounce(ent, index, canCollide)
	setEmitterColor(ent, index, color)
	setEmitterLifespan(ent, index, life)
	setEmitterStartAlpha(ent, index, startAlpha)
	setEmitterEndAlpha(ent, index, endAlpha)
	setEmitterStartSize(ent, index, startSize)
	setEmitterEndSize(ent, index, endSize)
	setEmitterStartLength(ent, index, startLength)
	setEmitterEndLength(ent, index, endLength)
	setEmitterGravity(ent, index, gravity)
	setEmitterRoll(ent, index, roll)
	setEmitterRollNoise(ent, index, rollNoise)
	setEmitterRollSpeed(ent, index, rollSpeed)
	setEmitterRollSpeedNoise(ent, index, rollSpeedNoise)
	setEmitterRollDrag(ent, index, rollDrag)
	setEmitterRollSpeedMin(ent, index, rollSpeedMin)
	setEmitterVelocity(ent, index, velocity)
	setEmitterVelocityNoise(ent, index, velNoise)
	setEmitterVelocityInherit(ent, index, velInherit)
	--setEmitterAngles(ent, index, ang)
	--setEmitterAngleVelocity(ent, index, angVel)
end)

//--END EMITTER CONTROL--


//-----------------------
// EMITTER DRAWING
//-----------------------

//Update Particles
function ParticleThink()
	
	//Get camera position
	local viewPos = Vector(0, 0, 0)
	local viewDir = Vector(0, 0, 1)
	if IsValid(LocalPlayer()) then
		local viewModel = LocalPlayer():GetViewModel()
		if IsValid(viewModel) then
			viewPos = viewModel:GetPos()
			viewDir = viewModel:GetForward()
		end
	end
	
	//Update sprite angles to face camera (because gmod doesn't do this automatically...figures)
	for k, particle in pairs(Particles) do
	
		//Remove invalid (cleaned up) particles
		if not particle or particle == nil or particle:GetLifeTime() > particle:GetDieTime() then
			table.remove(Particles, k)
			continue
		end
		
		local spriteAng = viewDir:Angle()
		
		//Spin the particle
		particle.data["roll"] = (particle.data["roll"] + (particle.data["rollSpeed"]) * FrameTime()) % 360
		spriteAng = Angle(-spriteAng.pitch, spriteAng.yaw + 180, particle.data["roll"])
		
		//Apply drag to spin rate
		if particle.data["rollSpeed"] != 0 then
			if math.abs(particle.data["rollSpeed"]) - particle.data["rollDrag"] >= particle.data["rollSpeedMin"] then
				local isPositive = particle.data["rollSpeed"] / math.abs(particle.data["rollSpeed"])
				particle.data["rollSpeed"] = particle.data["rollSpeed"] - (particle.data["rollDrag"] * isPositive)
			end
		end
		
		//Apply
		particle:SetAngles(spriteAng)
	end
end
hook.Add("PreDrawOpaqueRenderables", "expression2_update_particles", ParticleThink)


function DrawEmitters()
	if not receivedEmitters then return end //Anti-Error Spam
	if not GetConVar("wire_particles_enable"):GetBool() then return end //Toggle particle drawing
	
	for ent, k in pairs(Emitters) do
		for index, v in pairs(Emitters[ent]) do
			if ent and IsValid(ent) then
			
				//Get camera position
				local viewPos = Vector(0, 0, 0)
				if IsValid(LocalPlayer()) then
					local viewModel = LocalPlayer():GetViewModel()
					if IsValid(viewModel) then
						viewPos = viewModel:GetPos()
					end
				end
				
				//Get data to check if we can/need to draw
				local pos = Emitters[ent][index]["pos"]
				local parent = Emitters[ent][index]["parent"]
				local emitter = Emitters[ent][index]["emitter"]
				local rate = Emitters[ent][index]["rate"]
				
				//Calculate Parenting Stuff
				if(IsValid(parent)) then
					local parentPos = Emitters[ent][index]["parentpos"]
					pos = parent:LocalToWorld(parentPos)
				end
				
				//Check if this emitter already has the maximum number of allowed particles
				if emitter:GetNumActiveParticles() >= GetConVar("wire_particles_maxparticles"):GetInt() then continue end
				
				//Check if this emitter can actually fire
				if Emitters[ent][index]["lastEmit"] and CurTime() < (Emitters[ent][index]["lastEmit"] + (1/rate)) then continue end
				
				//Check if this emitter is in range to emit
				local distSqr = GetConVar("wire_particles_maxdistance"):GetFloat()
				distSqr = distSqr * distSqr
				if pos:DistToSqr(viewPos) > distSqr then continue end
				
				//Get the rest of the render data
				local mat = Emitters[ent][index]["mat"]
				local drag = Emitters[ent][index]["drag"]
				local bounce = Emitters[ent][index]["bounce"]
				local canCollide = Emitters[ent][index]["canCollide"]
				local color = Emitters[ent][index]["color"]
				local life = Emitters[ent][index]["lifespan"]
				local startAlpha = Emitters[ent][index]["startAlpha"]
				local endAlpha = Emitters[ent][index]["endAlpha"]
				local startSize = Emitters[ent][index]["startSize"]
				local endSize = Emitters[ent][index]["endSize"]
				local startLength = Emitters[ent][index]["startLength"]
				local endLength = Emitters[ent][index]["endLength"]
				local gravity = Emitters[ent][index]["gravity"]
				local roll = Emitters[ent][index]["roll"]
				local rollNoise = Emitters[ent][index]["rollNoise"]
				local rollSpeed = Emitters[ent][index]["rollSpeed"]
				local rollSpeedNoise = Emitters[ent][index]["rollSpeedNoise"]
				local rollDrag = Emitters[ent][index]["rollDrag"]
				local rollSpeedMin = Emitters[ent][index]["rollSpeedMin"]
				local velocity = Emitters[ent][index]["velocity"]
				local velNoise = Emitters[ent][index]["velNoise"]
				local velInherit = Emitters[ent][index]["velInherit"]
				--local ang = Emitters[ent][index]["angles"]
				--local angVel = Emitters[ent][index]["angVel"]		//Commented out until I find a use for these.
				
				//Apply Velocity Calcs
				velocity = velocity + Vector(math.random(-velNoise.x, velNoise.x), math.random(-velNoise.y, velNoise.y), math.random(-velNoise.z, velNoise.z))
				if velocity:LengthSqr() == 0 then velocity = Vector(0, 0, 0.01) end
				
				//If parented, adjust particles relative to parent
				if IsValid(parent) then
					
					//Transform relative velocity to world
					velocity:Rotate(parent:GetAngles())
					
					//Add in inherited velocity
					local parentVel = parent:GetVelocity()
					parentVel.x = parentVel.x * velInherit.x
					parentVel.y = parentVel.y * velInherit.y
					parentVel.z = parentVel.z * velInherit.z
					
					velocity = velocity + parentVel
				end
				
				//Apply Roll Noise Calcs
				roll = roll + math.random(-rollNoise, rollNoise)
				rollSpeed = rollSpeed + math.random(-rollSpeedNoise, rollSpeedNoise)
				
				//Check if we have the required data to start drawing
				if mat == nil then continue end
				if life == nil then continue end
				
				//Emit the particle
				local particle = emitter:Add(mat, pos)
				particle:SetDieTime(life)
				particle:SetVelocity(velocity)
				particle:SetAirResistance(drag)
				particle:SetBounce(bounce)
				particle:SetCollide(canCollide)
				particle:SetColor(color.x, color.y, color.z)
				particle:SetStartAlpha(startAlpha)
				particle:SetEndAlpha(endAlpha)
				particle:SetStartSize(startSize)
				particle:SetEndSize(endSize)
				particle:SetStartLength(startLength)
				particle:SetEndLength(endLength)
				particle:SetGravity(gravity)
				
				//Send processing data to the particle
				particle.data = {}
				particle.data["roll"] = roll
				particle.data["rollSpeed"] = rollSpeed
				particle.data["rollDrag"] = rollDrag
				particle.data["rollSpeedMin"] = rollSpeedMin
				
				table.insert(Particles, particle)
				
				
				Emitters[ent][index]["lastEmit"] = CurTime()
			else
				Emitters[ent] = nil
			end
		end 
	end
end
hook.Add("PostDrawOpaqueRenderables","expression2_draw_emitters", DrawEmitters)

//--END EMITTER DRAWING--


//-----------------------------
// E2HELPER DESCRIPTIONS
//-----------------------------

if E2Helper then

E2Helper.Descriptions["emitterCreate"] = "Creates a new Particle Emitter at the given position.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterDestroy"] = "Cleans up a Particle Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterDestroyAll"] = "Cleans up all Particle Emitters on an E2.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterCanCreate"] = "Returns 1 if the E2 can create another Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterRemainingSpawns"] = "Returns the number of Emitters that can still be created by this E2.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetParent"] = "Parents a Particle Emitter to the given Entity.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetPos"] = "Sets the position of an Emitter. If the Emitter is parented, this is a relative vector.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetRate"] = "Sets the number of particles emitted per-second by an Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetMaterial"] = "Sets the texture to use for particles emitted.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetDrag"] = "Sets the amount of drag on the particles emitted by this Emitter (i.e. how much a particle slows down per second).\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetBounce"] = "Sets the amount of velocity particles retain when bouncing off of things. Requires emitterSetCanBounce to be true.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetCanBounce"] = "Toggles the ability for particles emitted by this Emitter to collide with things.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetColor"] = "Sets the color of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetLifespan"] = "Sets the time to life (in seconds) for particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetStartAlpha"] = "Sets the initial opacity of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetEndAlpha"] = "Sets the ending opacity of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetStartSize"] = "Sets the initial lateral size of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetEndSize"] = "Sets the ending lateral size of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetStartLength"] = "Sets the initial vertical size of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetEndLength"] = "Sets the ending vertical size of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetGravity"] = "Sets the direction and magnitude of gravity on particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetRoll"] = "Sets the initial 2D angle of the particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetRollSpeed"] = "Sets the amount of spin each particle has.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetRollSpeedNoise"] = "Randomizes the amount of spin each particle has. Random range is plus or minus the value given.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetRollDrag"] = "Sets the amount that particle spin slows down over time.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetRollSpeedMin"] = "Sets the minimum spin speed of a particle, after which emitterSetRollDrag has no effect.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetVelocity"] = "Sets the direction and speed of particles emitted by this Emitter.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetVelocityNoise"] = "Randomizes the direction and speed of particles emitted by this Emitter. Random range for each component is plus or minus the value given.\n\nBy: Steeveeo"
E2Helper.Descriptions["emitterSetVelocityInherit"] = "Sets the amount of velocity imparted onto particles emitted by this Emitter. Requires the Emitter to be parented to something.\nGiving this a Vector value allows you to control the speed multiplier for each direction, where a Number will just multiply the speed itself.\n\nBy: Steeveeo"

end

//--END E2HELPER DESCRIPTIONS--