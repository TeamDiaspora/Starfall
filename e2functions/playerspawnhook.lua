//Created by Nielk1

local PlayerSpawnAlert = {}
local runByPlayerSpawn = 0
local PlayerSpawnPly = nil


--[[************************************************************************]]--

hook.Add( "PlayerSpawn", "E2PlayerSpawn", function( ply )
	runByPlayerSpawn = 1
	PlayerSpawnPly = ply
	for e,_ in pairs(PlayerSpawnAlert) do
		if IsValid(e) then
			e:Execute()
		else
			PlayerSpawnAlert[e] = nil
		end
	end
	PlayerSpawnPly = nil
	runByPlayerSpawn = 0
end )

e2function void runOnPlayerSpawn(activate)
	if activate ~= 0 then
		PlayerSpawnAlert[self.entity] = true
	else
		PlayerSpawnAlert[self.entity] = nil
	end
end
 
e2function entity playerSpawned()
	return PlayerSpawnPly
end

e2function number playerSpawnClk()
	if not IsValid(PlayerSpawnPly) then return 0 end
	return runByPlayerSpawn
end

e2function number playerSpawnClk(entity target)
	if not IsValid(target) then return 0 end
	if target != PlayerSpawnPly then return 0 end
	return runByPlayerSpawn
end