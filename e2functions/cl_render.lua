//-------------------------------------
// DRAW SPRITE CLIENTSIDE FUNCTIONS
//		Author: Steeveeo
//-------------------------------------

SpriteTables = {}
local requestedSprites = false
local receivedSprites = true

//Send a request to the server for sprites
net.Start("DrawSprite - Client Requesting Sync")
	requestedSprites = true
net.SendToServer()

//---------------------------
// SPRITE FUNCTIONS
//---------------------------

local function setSpriteMaterial(index, ent, mat)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	SpriteTables[ent][index]["mat"] = Material(mat)
	SpriteTables[ent][index]["mat"]:SetInt("$spriterendermode",9)
	SpriteTables[ent][index]["mat"]:SetInt("$ignorez",1)
	SpriteTables[ent][index]["mat"]:SetInt("$illumfactor",8)
	SpriteTables[ent][index]["mat"]:SetFloat("$alpha",0.6)
	SpriteTables[ent][index]["mat"]:SetInt("$nocull",1)
end

local function setSpriteParent(index, ent, parent)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	//Parent existing sprite
	if parent != nil and parent:IsValid() then
		SpriteTables[ent][index]["parent"] = parent
		SpriteTables[ent][index]["parentpos"] = parent:WorldToLocal(SpriteTables[ent][index]["pos"] or Vector(0, 0, 0))
	//Unparent a parented sprite
	else
		local oldParent = SpriteTables[ent][index]["parent"]
		if oldParent != nil and oldParent:IsValid() then
			SpriteTables[ent][index]["pos"] = oldParent:LocalToWorld(SpriteTables[ent][index]["parentpos"])
		end
	end
end

local function setSpriteColor(index, ent, color)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	SpriteTables[ent][index]["color"] = Color(color[1], color[2], color[3], 255) --SpriteTables[ent][index]["color"].a or 255)
end

local function setSpriteAlpha(index, ent, alpha)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	if SpriteTables[ent][index]["color"] == nil then
		SpriteTables[ent][index]["color"] = Color(255,255,255,255)
	end
	SpriteTables[ent][index]["color"].a = alpha
end

local function setSpriteSizeX(index, ent, sizeX)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	SpriteTables[ent][index]["sizex"] = sizeX
end

local function setSpriteSizeY(index, ent, sizeY)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	SpriteTables[ent][index]["sizey"] = sizeY
end

local function setSpritePos(index, ent, pos)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	//Move a parented sprite
	if SpriteTables[ent][index]["parent"] != nil and SpriteTables[ent][index]["parent"]:IsValid() then
		SpriteTables[ent][index]["parentpos"] = pos
	else
		SpriteTables[ent][index]["pos"] = pos
	end
end

local function setSpriteIgnoreZ(index, ent, ignoreZ)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	if ignoreZ == 0 then
		SpriteTables[ent][index]["ignoreZ"] = false
	else
		SpriteTables[ent][index]["ignoreZ"] = true
	end
end

local function setSpriteEntFilter(index, ent, ignoreEnts)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil or SpriteTables[ent][index] == nil then return end
	
	SpriteTables[ent][index]["entfilter"] = ignoreEnts
end

local function removeSprite(index, ent)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil then return end
	
	SpriteTables[ent][index] = nil
end

local function removeAllSprites(ent)
	if ent == nil or !IsValid(Entity(ent)) then return end
	if SpriteTables[ent] == nil then return end
	
	SpriteTables[ent] = nil
end

//--END OF SPRITE FUNCTIONS--


//--------------------------------
// NET RECEIVE FUNCTIONS
//--------------------------------

//Create a Sprite
net.Receive("DrawSprite - Create Sprite", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local mat = net.ReadString()
	local parent = net.ReadEntity()
	local color = net.ReadVector()
	local alpha = net.ReadFloat()
	local sizeX = net.ReadFloat()
	local sizeY = net.ReadFloat()
	local pos = net.ReadVector()
	
	if !SpriteTables[ent] then
		SpriteTables[ent] = {}
	end
	
	if !SpriteTables[ent][index] then
		SpriteTables[ent][index] = {}
	end
	
	//Set Attributes
	setSpriteMaterial(index, ent, mat)
	setSpritePos(index, ent, pos)
	setSpriteParent(index, ent, parent)
	setSpriteColor(index, ent, color)
	setSpriteAlpha(index, ent, alpha)
	setSpriteSizeX(index, ent, sizeX)
	setSpriteSizeY(index, ent, sizeY)
	setSpriteIgnoreZ(index, ent, 0)
end)

//Sync Existing Sprite
net.Receive("DrawSprite - Sync Sprite", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	
	if !SpriteTables[ent] then
		SpriteTables[ent] = {}
	end
	
	if !SpriteTables[ent][index] then
		SpriteTables[ent][index] = {}
	end
	
	--MsgN("Syncing Received Sprite for " .. tostring(Entity(ent)) .. ": " .. tostring(index))
	
	setSpriteMaterial(index, ent, net.ReadString())
	setSpritePos(index, ent, net.ReadVector())
	setSpriteParent(index, ent, net.ReadEntity())
	setSpriteColor(index, ent, net.ReadVector())
	setSpriteAlpha(index, ent, net.ReadFloat())
	setSpriteSizeX(index, ent, net.ReadFloat())
	setSpriteSizeY(index, ent, net.ReadFloat())
	setSpriteIgnoreZ(index, ent, net.ReadFloat())
	
	local count = net.ReadFloat()
	if count > 0 then
		local ignoreEnts = {}
		for index = 1, count do
			table.insert(ignoreEnts, Entity(net.ReadFloat()))
		end
		
		setSpriteEntFilter(index, ent, ignoreEnts)
	end
end)

//Server finished sending Sprites
net.Receive("DrawSprite - Client Sync Complete", function(len)
	receivedSprites = true
end)

//Set Material
net.Receive("DrawSprite - setSpriteMat", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local mat = net.ReadString()
	
	setSpriteMaterial(index, ent, mat)
end)

//Set Parent
net.Receive("DrawSprite - setSpriteParent", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local parent = net.ReadEntity()
	
	setSpriteParent(index, ent, parent)
end)

//Set Color
net.Receive("DrawSprite - setSpriteColor", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local color = net.ReadVector()
	
	setSpriteColor(index, ent, color)
end)

//Set Alpha
net.Receive("DrawSprite - setSpriteAlpha", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local alpha = net.ReadFloat()
	
	setSpriteAlpha(index, ent, alpha)
end)

//Set Size X
net.Receive("DrawSprite - setSpriteSizeX", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local sizeX = net.ReadFloat()
	
	setSpriteSizeX(index, ent, sizeX)
end)

//Set Size Y
net.Receive("DrawSprite - setSpriteSizeY", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local sizeY = net.ReadFloat()
	
	setSpriteSizeY(index, ent, sizeY)
end)

//Set Position
net.Receive("DrawSprite - setSpritePos", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local pos = net.ReadVector()
	
	setSpritePos(index, ent, pos)
end)

//Set Ignore Z
net.Receive("DrawSprite - setSpriteIgnoreZ", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	local ignoreZ = net.ReadFloat()
	
	setSpriteIgnoreZ(index, ent, ignoreZ)
end)

//Set Ignored Entities (Draw through)
net.Receive("DrawSprite - setSpriteEntFilter", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	
	local ignoreEnts = {}
	local count = net.ReadFloat()
	for index = 1, count do
		table.insert(ignoreEnts, Entity(net.ReadFloat()))
	end
	
	setSpriteEntFilter(index, ent, ignoreEnts)
end)

//Remove a Sprite
net.Receive("DrawSprite - Remove Sprite", function(len)
	local index = net.ReadFloat()
	local ent = net.ReadFloat()
	
	removeSprite(index, ent)
end)

//Remove all Sprites on Chip
net.Receive("DrawSprite - Remove All Sprites", function(len)
	local ent = net.ReadFloat()
	
	removeAllSprites(ent)
end)

//--END OF NET RECEIVE FUNCTIONS--


//----------------
// Sprite Drawing
//----------------

local function checkCanSee(vec1, vec2, ignoreEnts)

	local entFilter = {}
	if ignoreEnts != nil then
		entFilter = table.Copy(ignoreEnts)
	end
	
	local tracedata = {}
	tracedata.start = vec1
	tracedata.endpos = vec2
	if IsValid(LocalPlayer()) then //Why the FUCK does LocalPlayer() return NIL sometimes?
		table.insert(entFilter, LocalPlayer())
	end
	tracedata.filter = entFilter
	tracedata.mask = CONTENTS_SOLID + CONTENTS_MOVEABLE + CONTENTS_OPAQUE + CONTENTS_DEBRIS + CONTENTS_HITBOX + CONTENTS_MONSTER
	
	local trace = util.TraceLine(tracedata)
	if !trace.Hit then
		return true
	end
	
	return false
end

function DrawSprites()
	
	if !receivedSprites then return end //Anti-Error Spam
	
	local viewPos;
	if IsValid(LocalPlayer()) then //Seriously, why? I really would like to know.
		local viewModel = LocalPlayer():GetViewModel() //GARRY WHY
		if IsValid(viewModel) then
			viewPos = viewModel:GetPos()
		end
	end
		
	for ent, k in pairs(SpriteTables) do
		for index, v in pairs(SpriteTables[ent]) do
			if IsValid(Entity(ent)) then
				
				local mat = SpriteTables[ent][index]["mat"]
				local pos = SpriteTables[ent][index]["pos"]
				local sizeX = SpriteTables[ent][index]["sizex"]
				local sizeY = SpriteTables[ent][index]["sizey"]
				local color = SpriteTables[ent][index]["color"]
				local parent = SpriteTables[ent][index]["parent"]
				local ignoreZ = SpriteTables[ent][index]["ignoreZ"]
				local ignoreEnts = SpriteTables[ent][index]["entfilter"]
				local parentPos = Vector(0,0,0)
				if(IsValid(parent)) then
					parentPos = SpriteTables[ent][index]["parentpos"]
				end
				
				if sizeX and sizeY and color then
					if !IsValid(parent) then
						if ignoreZ or checkCanSee(viewPos, pos, ignoreEnts) then
							render.SetMaterial(mat)
							render.DrawSprite(pos, sizeX, sizeY, color)
						end
					else
						if ignoreZ or checkCanSee(viewPos, parent:LocalToWorld(parentPos), ignoreEnts) then
							render.SetMaterial(mat)
							render.DrawSprite(parent:LocalToWorld(parentPos), sizeX, sizeY, color)
						end
					end
				end
			else
				SpriteTables[ent] = nil
			end
		end 
	end
end

hook.Add("PostDrawOpaqueRenderables","expression2_draw_sprites",DrawSprites)

