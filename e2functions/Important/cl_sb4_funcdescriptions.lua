if E2Helper then

-- Resource Distribution functions
E2Helper.Descriptions["rdIsNode"] = "Checks if this Entity is the central hub of a Resource Network."
E2Helper.Descriptions["rdIsDevice"] = "Checks if this Entity is an Resource Distribution Device."
E2Helper.Descriptions["rdCapacity"] = "Returns the maximum storage of the given Resource name for this network."
E2Helper.Descriptions["rdAmount"] = "Returns the amount of the given Resource name this network has stored."
E2Helper.Descriptions["rdName"] = "Returns the name of the network this Entity is on."
E2Helper.Descriptions["rdResourceList"] = "Returns an array of all the resources this network can hold."
E2Helper.Descriptions["rdNetEntities"] = "Returns an array of all the Resource Distribution Devices on a network."
E2Helper.Descriptions["rdNodeSize"] = "Gets the maximum radius of a given Node."
E2Helper.Descriptions["rdConsumeResource"] = "Drains the given amount of the named Resource from the network. Returns 1 upon success, 0 upon failure, -2 if the given node is invalid, and -3 if you do not have permissions to do this."

-- Spacebuild functions
E2Helper.Descriptions["sbPlanets"] = "Returns an array of all the Planets on the map."
E2Helper.Descriptions["lsName"] = "Gets the name of the Environment this entity is in. Returns the name of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsSize"] = "Gets the radius of the Environment this entity is in. Returns the radius of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsTemperature"] = "Gets the temperature of the Environment this entity is in. Returns the temperature of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsGravity"] = "Gets the gravity of the Environment this entity is in as a fraction of G (normal gravity is 1, half gravity is 0.5). Returns the gravity of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsPressure"] = "Gets the pressure of the Environment this entity is in as a fraction of P (normal pressure is 1, half pressure is 0.5). Returns the pressure of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsO2Percent"] = "Gets the percentage of Oxygen in the Environment this entity is in as a fraction of 1. Returns the O2 percentage of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsCO2Percent"] = "Gets the percentage of Carbon Dioxide in the Environment this entity is in as a fraction of 1. Returns the CO2 percentage of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsNPercent"] = "Gets the percentage of Nitrogen in the Environment this entity is in as a fraction of 1. Returns the Nitrogen percentage of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsHPercent"] = "Gets the percentage of Hydrogen in the Environment this entity is in as a fraction of 1. Returns the Hydrogen percentage of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsVolume"] = "Gets the unitary volume of the Environment this entity is in. Returns the volume of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsO2Amount"] = "Gets the amount of Oxygen in the Environment this entity is in. Returns amount of O2 of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsCO2Amount"] = "Gets the amount of Carbon Dioxide in the Environment this entity is in. Returns amount of CO2 of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsNAmount"] = "Gets the amount of Nitrogen in the Environment this entity is in. Returns amount of Nitrogen of the atmosphere if used on a Planet."
E2Helper.Descriptions["lsHAmount"] = "Gets the amount of Hydrogen in the Environment this entity is in. Returns amount of Hydrogen of the atmosphere if used on a Planet."

E2Helper.Descriptions["lsEnvResources"] = "Gets all resources currently present in the Environment this entity is in. Array entries are strings."
E2Helper.Descriptions["lsResPercent"] = "Gets the percentage of the provided resource name in the Environment this entity is in as a fraction of 1."
E2Helper.Descriptions["lsResAmount"] = "Gets the amount of the provided resource name in the Environment this entity is in."

E2Helper.Descriptions["lsGetEnvironment"] = "Returns the entity of the Environment that this entity is inside."
E2Helper.Descriptions["lsGetSunDirection"] = "Returns the direction of the sun as a directional vector."

E2Helper.Descriptions["sbSetGravityOverride"] = "Tells Spacebuild that you will be manually handling Gravity on this entity."
E2Helper.Descriptions["sbGetGravityOverride"] = "Checks if this entity's Gravity is being manually handled."
E2Helper.Descriptions["sbSetDragOverride"] = "Tells Spacebuild that you will be manually handling Drag on this entity."
E2Helper.Descriptions["sbGetDragOverride"] = "Checks if this entity's Drag is being manually handled."

-- Lifesupport functions
E2Helper.Descriptions["lsSuitLifesupport"] = "Gets the number of Lifesupport Canisters this Player has."

-- Resource Pump functions
E2Helper.Descriptions["rdPumpGetNearby"] = "Returns an array of the nearby Resource Pumps that this Pump can connect to."
E2Helper.Descriptions["rdPumpConnect"] = "Connect this Pump to the given Pump."
E2Helper.Descriptions["rdPumpDisconnect"] = "Disconnect this Pump and stop all tasks."
E2Helper.Descriptions["rdPumpGetConnection"] = "Returns the Pump that this Pump is connected to."
E2Helper.Descriptions["rdPumpQueueTask"] = "Tells this Pump to start or queue a Task. EX: Pump:rdPumpQueueTask(\"Oxygen\", 5000, 1) sends 5000 Oxygen. Setting the third argument to 0 sets the Task to Receive. Resource names are Case Sensitive."
E2Helper.Descriptions["rdPumpRemoveTask"] = "Removes the numbered Task from the queue. Currently active Task is 0."
E2Helper.Descriptions["rdPumpGetTask"] = "Gets the Task data table at the given index. Currently active Task is 0."
E2Helper.Descriptions["rdPumpGetQueueLength"] = "Gets the number of Tasks queued on this Pump. Note: This is the total number including the current Task."
E2Helper.Descriptions["rdPumpGetTaskProgress"] = "Gets progress percentage of the current task as a fraction of 1."
E2Helper.Descriptions["runOnPumpTaskComplete"] = "Toggles a hook on a given pump and runs whenever a task is complete."
E2Helper.Descriptions["rdPumpCompleteClk"] = "Check if this script was run due to the completion of a Pump's Task."
E2Helper.Descriptions["rdPumpCompleted"] = "Returns which Pump completed a Task and ran this script."

-- Factions functions
E2Helper.Descriptions["getFaction"] = "Gets the current Faction of a player."
E2Helper.Descriptions["setFaction"] = "Sets the current Faction of a player if the Faction exists. Admin-only function."

end