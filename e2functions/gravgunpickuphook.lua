//Created by Nielk1

local PickupAlert = {}
local runByPickup = 0
local PickupBy = nil
local PickupEnt = nil

--[[************************************************************************]]--

hook.Add( "GravGunOnPickedUp", "E2GravGunOnPickedUp", function( userid, target )
	runByPickup = 1
	PickupBy = userid
	PickupEnt = target
	for e,_ in pairs(PickupAlert) do
		if IsValid(e) then
			e:Execute()
		else
			PickupAlert[e] = nil
		end
	end
	PickupBy = nil
	PickupEnt = nil
	runByPickup = 0
end )

e2function void runOnPickup(activate)
	if activate ~= 0 then
		PickupAlert[self.entity] = true
	else
		PickupAlert[self.entity] = nil
	end
end
 
e2function entity pickupBy()
	return PickupBy
end

e2function entity pickupEnt()
	return PickupEnt
end

e2function number pickupClk()
	if not IsValid(PickupEnt) then return 0 end
	return runByPickup
end

e2function number pickupClk(entity target)
	if not IsValid(target) then return 0 end
	if target != PickupEnt then return 0 end
	return runByPickup
end