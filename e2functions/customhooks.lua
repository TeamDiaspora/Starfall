// Author: Steeveeo
/*
	In this Lua, I have made a bunch of hooks for E2s to run on.
	Currently, this list consists of:
		- Use Hooks
		- Collision Hooks
		- Touch Hooks (Commented out for now)
		
	This list will grow as I find interesting hooks for E2s to run on.
*/

//-----------------Use Functions start Here----------------------------------------------------

--First the functionality
//local useactive = 0;
local user = nil;
local runByUse = 0;
//local nextUseExec = 0;

//Make sure it can only run once from a use.
registerCallback("construct", function(self)
	self.entity:SetUseType( SIMPLE_USE )
end)


//--This was a very DUMB way of doing it, trying something different!
-- function ENT:Use( activator, caller )
	-- if self.entity.CanUseHook == nil then
		-- self.entity.CanUseHook = 1;
	-- else
		-- self.Entity:SetUseType(SIMPLE_USE)
		-- if not self.entity.useactive then return end
		-- if not IsValid(activator) then return end
		--Prevent spamming of executions
		-- if CurTime() > self.entity.nextUseExec || self.entity.nextUseExec == nil then
			-- runByUse = 1;
			-- user = activator;
			-- self.entity:Execute()
			-- runByUse = 0;
			-- user = nil;
			-- self.entity.nextUseExec = CurTime()+0.05
		-- end
	-- end
-- end

--And now for the E2 registrations
e2function void runOnUse( activate )
	--self.entity.useactive = activate;
	if activate~=0 then
		self.entity.Use = function(self,activator)
			user = activator
			runByUse = 1
			self:Execute()
			runByUse = 0
			user = nil
		end
	else
		self.entity.Use = nil
	end

end

e2function number useClk()
	return runByUse
end

e2function number useClk(entity ply)
	if not IsValid(ply) then return 0 end
	if user == ply && runByUse == 1 then
		return 1
	else
		return 0
	end
end

e2function entity usedBy()
	return user
end
//--------End Use Functions----]]

//-----------------Collide Functions start Here----------------------------------------------------

--First the Functionality
--local collideActive = 0;
local collisionSensitivity = 50;
local collisionRecheckTime = 0.1;
local collisionData = {};
local runByCollision = 0;

//--Again, STUPID way to do this, doing it the same was as I did above.--
-- function ENT:PhysicsCollide( data, physobj )
	-- if not collideActive then return end
	-- if data.Speed >= collisionSensitivity && data.DeltaTime > collisionRecheckTime then
		-- collisionData = data;
		-- runByCollision = 1;
		-- self.Entity:Execute()
		-- collisionData = {};
		-- runByCollision = 0;
	-- end
-- end

//Set up collision defaults.
registerCallback("construct", function(self)
	self.entity.collisionSensitivity = 50;
	self.entity.collisionRecheckTime = 0.1;
end)

--And now for the E2 registrations
--//Initial setup functions
e2function void runOnCollide( activate )
	--collideActive = physactive;
	if activate~=0 then
		self.entity.PhysicsCollide = function(self, data, physobj)
			if data.Speed >= 50 && data.DeltaTime > 0.1 then
				collisionData = data;
				runByCollision = 1;
				self:Execute()
				collisionData = {};
				runByCollision = 0;
			end
		end
	else
		self.entity.PhysicsCollide = nil
	end
end

e2function number collideClk()
	return runByCollision
end

e2function void collisionSensitivity( sensitivity )
	self.entity.collisionSensitivity = math.Clamp(sensitivity, 15, math.huge);
end

e2function void collisionRecheckDelay( delay )
	self.entity.collisionRecheckTime = math.Clamp(delay, 0.1, math.huge);
end

--//Data returns
e2function vector collidePos()
	return collisionData.HitPos
end

e2function entity collideEnt()
	return collisionData.HitEntity
end

e2function vector collideSpeed()
	return collisionData.OurOldVelocity
end

e2function vector collideEntSpeed()
	return collisionData.TheirOldVelocity
end

e2function number collideLast()
	return collisionData.DeltaTime
end

e2function number collideForce()
	return collisionData.Speed
end

e2function vector collideNormal()
	return collisionData.HitNormal
end

//--------End Collide Functions----]]

//-----------------Touch Functions start Here----------------------------------------------------
/* 	Not using these for now, too tired to test them and they are quite buggy.
	However, they have quite a bit of potential.
--First the functionality
local touchactive = 0;
local untouchcheck = {};
local touched = nil;
local untouched = nil;
local runByTouch = 0;
local runByUntouch = 0;

function ENT:StartTouch( ent )
	if not touchactive then return end
	for _, v in pairs (constraint.GetAllConstrainedEntities(self.Entity)) do
		if v == ent then
			return
		end
	end
	touched = ent;
	runByTouch = 1;
	self.Entity:Execute()
end

function ENT:EndTouch( ent )
	for _, v in pairs ( untouchcheck ) do
		if ent == v then
			untouched = ent;
			untouchcheck[ent] = nil;
			runByUntouch = 1;
			self.Entity:Execute()
			return
		end
	end
end

--And now for the E2 registrations
e2function void runOnTouch( activate )
	touchactive = activate;
end

e2function void addUntouchCheck( ent )
	untouchcheck[ent] = ent;
end

e2function void removeUntouchCheck( ent )
	untouchcheck[ent] = nil;
end

e2function number touchClk()
	return runByTouch
end

e2function number touchClk(entity ent)
	if not IsValid(ent) then return 0 end
	if touched == ent && runByTouch == 1 then
		return 1
	else
		return 0
	end
end

e2function number untouchClk(entity ent)
	if not IsValid(ent) then return 0 end
	if touched == ent && runByUntouch == 1 then
		return 1
	else
		return 0
	end
end

e2function entity touched()
	return touched
end

e2function entity untouched()
	return untouched
end
*/
//--------End Touch Functions----]]

/************************************************************
***************   DLBs pointcontent extension     ******************
************************************************************/

e2function string pointContents(vector vec)
local contents = util.PointContents(vec)
	if( contents == 0 ) then return "EMPTY" end
	if( contents == 1 ) then return "SOLID" end
	if( contents == 2 ) then return "WINDOW" end
	if( contents == 4 ) then return "AUX" end
	if( contents == 8 ) then return "GRATE" end
	if( contents == 16 ) then return "SLIME" end
	if( contents == 32 ) then return "WATER" end
	if( contents == 64 ) then return "BLOCKLOS" end
	if( contents == 128 ) then return "OPAQUE" end
	if( contents == 256 ) then return "TESTFOGVOLUME" end
	if( contents == 512 ) then return "TEAM4" end
	if( contents == 1024 ) then return "TEAM3" end
	if( contents == 2048 ) then return "TEAM1" end
	if( contents == 4096 ) then return "TEAM2" end
	if( contents == 8192 ) then return "IGNORE_NODRAW_OPAQUE" end
	if( contents == 16384 ) then return "MOVEABLE" end
	if( contents == 32768 ) then return "AREAPORTAL" end
	if( contents == 65536 ) then return "PLAYERCLIP" end
	if( contents == 131072 ) then return "MONSTERCLIP" end
	if( contents == 262144 ) then return "CURRENT_0" end
	if( contents == 524288 ) then return "CURRENT_90" end
	if( contents == 1048576 ) then return "CURRENT_180" end
	if( contents == 2097152 ) then return "CURRENT_270" end
	if( contents == 4194304 ) then return "CURRENT_UP" end
	if( contents == 8388608 ) then return "CURRENT_DOWN" end
	if( contents == 16777216 ) then return "ORIGIN" end
	if( contents == 33554432 ) then return "MONSTER" end
	if( contents == 67108864 ) then return "DEBRIS" end
	if( contents == 134217728 ) then return "DETAIL" end
	if( contents == 268435488 ) then return "TRANSLUCENT" end
	if( contents == 536870912 ) then return "LADDER" end
	if( contents == 1073741824 ) then return "HITBOX" end
end