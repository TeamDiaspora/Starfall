-------------------------------------------------------------------------------
-- Surface Library functions
-------------------------------------------------------------------------------

--- Surface Library - Draw things and play sounds on your client screen! Virtually function works the same as it does in the Lua 'surface' library, <a href=""http://wiki.garrysmod.com/page/Category:surface"">Gmod Wiki</a> for details.
-- @client

local surf_library, _ = SF.Libraries.Register("surface")
local colUnwrap = SF.Color.Unwrap

--- Creates a new font.
function surf_library.CreateFont(...)
	return surface.CreateFont(...)
end

--- Enables or disables the clipping used by the VGUI that limits the drawing operations to a panels bounds.
function surf_library.DisableClipping(...)
	return surface.DisableClipping(...)
end

--- Draws a hollow circle, made of dots. For a filled circle, use surface.DrawPoly.
function surf_library.DrawCircle(originX, originY, radius, color)
	surface.DrawCircle(originX, originY, radius, colUnwrap(color))
end

--- Draws a line from one point to another.
function surf_library.DrawLine(...)
	return surface.DrawLine(...)
end

--- Draws a hollow box with a border width of 1 px.
function surf_library.DrawOutlinedRect(...)
	return surface.DrawOutlinedRect(...)
end

--- Draws a polygon with a maximum of 256 vertices. Only works properly with convex polygons. You may try to render concave polygons, but there is no guarantee that things won't get messed up.
function surf_library.DrawPoly(...)
	return surface.DrawPoly(...)
end

--- Draws a solid rectangle on the screen.
function surf_library.DrawRect(...)
	return surface.DrawRect(...)
end

--- Draw the specified text on the screen, using the previously set position, font and color.
function surf_library.DrawText(...)
	return surface.DrawText(...)
end

--- Draw a textured rectangle with the given position and dimensions on the screen, using the current active texture.
function surf_library.DrawTexturedRect(...)
	return surface.DrawTexturedRect(...)
end

--- Draw a textured rotated rectangle with the given position and dimensions and angle on the screen, using the current active texture.
function surf_library.DrawTexturedRectRotated(...)
	return surface.DrawTexturedRectRotated(...)
end

--- Draws a textured rectangle with a repeated or partial texture. U and V refer to texture coordinates. 0,0 is the bottom left corner of the texture, 1,0 is the bottom right, 1,1 is the top right and 0,1 is the top left.
--- If you are using a .png image, you must supply "noclamp" as second parameter for <a href="http://wiki.garrysmod.com/page/Global/Material">Material.</a>
--- Using a start point of (1,0) and an end point to (0,1), you can draw an image flipped horizontally, etc with other directions.
function surf_library.DrawTexturedRectUV(...)
	return surface.DrawTexturedRectUV(...)
end

--- Gets the HUD texture with the specified name.
function surf_library.GetHUDTexture(...)
	return surface.GetHUDTexture(...)
end

--- Returns the width and height (in pixels) of the given text, but only if the font has been set with surface.SetFont.
function surf_library.GetTextSize(...)
	return surface.GetTextSize(...)
end

--- Returns the texture id of the texture with the given name/path.
function surf_library.GetTextureID(...)
	return surface.GetTextureID(...)
end

--- Returns the size of the texture with the associated texture ID.
function surf_library.GetTextureSize(...)
	return surface.GetTextureSize(...)
end

--- Play a sound file directly on the client (such as UI sounds, etc).
function surf_library.PlaySound(...)
	return surface.PlaySound(...)
end

--- Returns the height of the current client's screen in pixels.
function surf_library.ScreenH()
	return ScrH()
end

--- Returns the width of the current client's screen in pixels.
function surf_library.ScreenW()
	return ScrW()
end

--- Sets a multiplier that will influence all upcoming drawing operations.
function surf_library.SetAlphaMultiplier(...)
	return surface.SetAlphaMultiplier(...)
end

--- Set the color of any future shapes to be drawn, set by a Color structure.
function surf_library.SetDrawColor(color)
	surface.SetDrawColor(colUnwrap(color))
end

--- Set the current font to be used for text operations later.
function surf_library.SetFont(...)
	return surface.SetFont(...)
end

--- Sets the material to be used in all upcoming surface draw operations.
-- @param IMaterial the material to use, made with Material()
function surf_library.SetMaterial(IMaterial)
	return surface.SetMaterial(SF.IMaterial.Unwrap(IMaterial))
end

--- Set the color of any future text to be drawn, set by a Color structure.
function surf_library.SetTextColor(color)
	surface.SetTextColor(colUnwrap(color))
end

--- Set the position to draw any future text, in pixels from the top left.
function surf_library.SetTextPos(...)
	return surface.SetTextPos(...)
end

--- Sets the texture to be used in all upcoming surface draw operations. Takes a Texture ID, use surface.GetTextureID() to acquire.
function surf_library.SetTexture(...)
	return surface.SetTexture(...)
end