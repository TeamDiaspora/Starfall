-------------------------------------------------------------------------------
-- PhysObj functions.
-------------------------------------------------------------------------------

SF.PhysObjs = {}

--- PhysObj Type
-- @shared
local physobj_methods, physobj_metatable = SF.Typedef("PhysObj")
local wrap, unwrap = SF.CreateWrapper(physobj_metatable, true, false, debug.getregistry().PhysObj)
local checktype = SF.CheckType
local checkpermission = SF.Permissions.check

SF.PhysObjs.Methods = physobj_methods
SF.PhysObjs.Metatable = physobj_metamethods
SF.PhysObjs.Wrap = wrap
SF.PhysObjs.Unwrap = unwrap

local ewrap, eunwrap
local owrap, ounwrap = SF.WrapObject, SF.UnwrapObject
local vec_meta

SF.Libraries.AddHook("postload", function()
	vec_meta = SF.Vectors.Metatable

	ewrap = SF.Entities.Wrap
	eunwrap = SF.Entities.Unwrap

    SF.DefaultEnvironment["FVPHYSICS_CONSTRAINT_STATIC"] = FVPHYSICS_CONSTRAINT_STATIC
    SF.DefaultEnvironment["FVPHYSICS_DMG_DISSOLVE"] = FVPHYSICS_DMG_DISSOLVE
    SF.DefaultEnvironment["FVPHYSICS_DMG_SLICE"] = FVPHYSICS_DMG_SLICE
    SF.DefaultEnvironment["FVPHYSICS_HEAVY_OBJECT"] = FVPHYSICS_HEAVY_OBJECT
    SF.DefaultEnvironment["FVPHYSICS_MULTIOBJECT_ENTITY"] = FVPHYSICS_MULTIOBJECT_ENTITY
    SF.DefaultEnvironment["FVPHYSICS_NO_IMPACT_DMG"] = FVPHYSICS_NO_IMPACT_DMG
    SF.DefaultEnvironment["FVPHYSICS_NO_NPC_IMPACT_DMG"] = FVPHYSICS_NO_NPC_IMPACT_DMG
    SF.DefaultEnvironment["FVPHYSICS_NO_PLAYER_PICKUP"] = FVPHYSICS_NO_PLAYER_PICKUP
    SF.DefaultEnvironment["FVPHYSICS_NO_SELF_COLLISIONS"] = FVPHYSICS_NO_SELF_COLLISIONS
    SF.DefaultEnvironment["FVPHYSICS_PART_OF_RAGDOLL"] = FVPHYSICS_PART_OF_RAGDOLL
    SF.DefaultEnvironment["FVPHYSICS_PENETRATING"] = FVPHYSICS_PENETRATING
    SF.DefaultEnvironment["FVPHYSICS_PLAYER_HELD"] = FVPHYSICS_PLAYER_HELD
    SF.DefaultEnvironment["FVPHYSICS_WAS_THROWN"] = FVPHYSICS_WAS_THROWN
end)

local function checkvector(v)
	if v[1]<-1e12 or v[1]>1e12 or v[1]~=v[1] or
	   v[2]<-1e12 or v[2]>1e12 or v[2]~=v[2] or
	   v[3]<-1e12 or v[3]>1e12 or v[3]~=v[3] then

		SF.Throw("Input vector too large or NAN", 3)

	end
end

--- Checks if the physics object is valid
-- @shared
-- @return boolean if the physics object is valid
function physobj_methods:IsValid()
	return unwrap(self):IsValid()
end

--- Gets the entity attached to the physics object
-- @shared
-- @return The entity attached to the physics object
function physobj_methods:GetEntity()
	return ewrap(unwrap(self):GetEntity())
end

--- Gets the position of the physics object
-- @shared
-- @return Vector position of the physics object
function physobj_methods:GetPos()
	return owrap(unwrap(self):GetPos())
end

--- Returns the world transform matrix of the physobj
-- @shared
-- @return The matrix
function physobj_methods:GetPositionMatrix()
	return owrap(unwrap(self):GetPositionMatrix())
end

--- Gets the angles of the physics object
-- @shared
-- @return Angle angles of the physics object
function physobj_methods:GetAngles()
	return owrap(unwrap(self):GetAngles())
end

--- Gets the velocity of the physics object
-- @shared
-- @return Vector velocity of the physics object
function physobj_methods:GetVelocity()
	return owrap(unwrap(self):GetVelocity())
end

--- Gets the velocity of the physics object at an arbitrary point in its local reference frame
--- This includes velocity at the point induced by rotational velocity
-- @shared
-- @param vec The point to get velocity of in local reference frame
-- @return Vector Local velocity of the physics object at the point
function physobj_methods:GetVelocityAtPoint(vec)
	checktype(vec, vec_meta)
	return owrap(unwrap(self):GetVelocityAtPoint(ounwrap(vec)))
end

--- Gets the angular velocity of the physics object
-- @shared
-- @return Vector angular velocity of the physics object
function physobj_methods:GetAngleVelocity()
	return owrap(unwrap(self):GetAngleVelocity())
end

--- Gets the mass of the physics object
-- @shared
-- @return mass of the physics object
function physobj_methods:GetMass()
	return unwrap(self):GetMass()
end

--- Gets the center of mass of the physics object in the local reference frame.
-- @shared
-- @return Center of mass vector in the physobject's local reference frame.
function physobj_methods:GetMassCenter()
	return owrap(unwrap(self):GetMassCenter())
end

--- Gets the inertia of the physics object
-- @shared
-- @return Vector Inertia of the physics object
function physobj_methods:GetInertia()
	return owrap(unwrap(self):GetInertia())
end

--- Gets the material of the physics object
-- @shared
-- @return The physics material of the physics object
function physobj_methods:GetMaterial()
	return unwrap(self):GetMaterial()
end

--- Returns a vector in the local reference frame of the physicsobject from the world frame
-- @param vec The vector to transform
-- @return The transformed vector
function physobj_methods:WorldToLocal(vec)
	checktype(vec, vec_meta)
	return owrap(unwrap(self):WorldToLocal(ounwrap(vec)))
end

--- Returns a vector in the reference frame of the world from the local frame of the physicsobject
-- @param vec The vector to transform
-- @return The transformed vector
function physobj_methods:LocalToWorld(vec)
	checktype(vec, vec_meta)
	return owrap(unwrap(self):LocalToWorld(ounwrap(vec)))
end

--- Returns a normal vector in the local reference frame of the physicsobject from the world frame
-- @param vec The normal vector to transform
-- @return The transformed vector
function physobj_methods:WorldToLocalVector(vec)
	checktype(vec, vec_meta)
	return owrap(unwrap(self):WorldToLocalVector(ounwrap(vec)))
end

--- Returns a normal vector in the reference frame of the world from the local frame of the physicsobject
-- @param vec The normal vector to transform
-- @return The transformed vector
function physobj_methods:LocalToWorldVector(vec)
	checktype(vec, vec_meta)
	return owrap(unwrap(self):LocalToWorldVector(ounwrap(vec)))
end

--- Returns a table of MeshVertex structures where each 3 vertices represent a triangle. See: http://wiki.garrysmod.com/page/Structures/MeshVertex
-- @return table of MeshVertex structures
function physobj_methods:GetMesh ()
	local mesh = unwrap(self):GetMesh()
	return SF.Sanitize(mesh)
end

--- Returns a structured table, the physics mesh of the physics object. See: http://wiki.garrysmod.com/page/Structures/MeshVertex
-- @return table of MeshVertex structures
function physobj_methods:GetMeshConvexes ()
	local mesh = unwrap(self):GetMeshConvexes()
	return SF.Sanitize(mesh)
end

--- Sets the physical material of a physics object
-- @param material The physical material to set it to
function physobj_methods:SetMaterial(material)
	checktype (material, "string")
	local phys = unwrap(self)
	checkpermission(SF.instance.player, phys:GetEntity(), "entities.setRenderProperty")
	phys:SetMaterial(material)
	if not phys:IsMoveable() then
		phys:EnableMotion(true)
		phys:EnableMotion(false)
	end
end

if SERVER then
	--- Sets the position of the physics object. Will cause interpolation of the entity in clientside, use entity.setPos to avoid this.
	-- @server
	-- @param pos The position vector to set it to
	function physobj_methods:SetPos(pos)
		checktype(pos, vec_meta)

		local vec = ounwrap(pos)
		checkvector(vec)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.setPos")
		phys:SetPos(vec)
	end

	--- Sets the velocity of the physics object
	-- @server
	-- @param vel The velocity vector to set it to
	function physobj_methods:SetVelocity(vel)
		checktype(vel, vec_meta)

		local vec = ounwrap(vel)
		checkvector(vec)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.setVelocity")
		phys:SetVelocity(vec)
	end

	--- Applys a force to the center of the physics object
	-- @server
	-- @param force The force vector to apply
	function physobj_methods:ApplyForceCenter(force)
		checktype(force, vec_meta)

		force = ounwrap(force)
		checkvector(force)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.applyForce")
		phys:ApplyForceCenter(force)
	end

	--- Applys an offset force to a physics object
	-- @server
	-- @param force The force vector to apply
	-- @param position The position in world coordinates
	function physobj_methods:ApplyForceOffset(force, position)
		checktype(force, vec_meta)
		checktype(position, vec_meta)

		force = ounwrap(force)
		checkvector(force)
		position = ounwrap(position)
		checkvector(position)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.applyForce")
		phys:ApplyForceOffset(force, position)
	end

	--- Sets the angular velocity of an object
	-- @server
	-- @param angvel The local angvel vector to set
	function physobj_methods:SetAngleVelocity(angvel)
		checktype(angvel, vec_meta)
		angvel = ounwrap(angvel)
		checkvector(angvel)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.applyForce")

		phys:AddAngleVelocity(angvel - phys:GetAngleVelocity())
	end

	--- Applys a angular velocity to an object
	-- @server
	-- @param angvel The local angvel vector to apply
	function physobj_methods:AddAngleVelocity(angvel)
		checktype(angvel, vec_meta)
		angvel = ounwrap(angvel)
		checkvector(angvel)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.applyForce")

		phys:AddAngleVelocity(angvel)
	end

	--- Applys a torque to a physics object
	-- @server
	-- @param torque The world torque vector to apply
	function physobj_methods:ApplyTorqueCenter(torque)
		checktype(torque, vec_meta)
		torque = ounwrap(torque)
		checkvector(torque)

		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.applyForce")

		phys:ApplyTorqueCenter(torque)
	end

	--- Sets the mass of a physics object
	-- @server
	-- @param mass The mass to set it to
	function physobj_methods:SetMass(mass)
		checktype(mass, "number")
		local phys = unwrap(self)
		local ent = phys:GetEntity()
		checkpermission(SF.instance.player, ent, "entities.setMass")
		local m = math.Clamp(mass, 1, 50000)
		phys:SetMass(m)
		duplicator.StoreEntityModifier(ent, "mass", { Mass = m })
	end

	--- Sets the inertia of a physics object
	-- @server
	-- @param inertia The inertia vector to set it to
	function physobj_methods:SetInertia(inertia)
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.setInertia")

		local vec = ounwrap(inertia)
		checkvector(vec)
		vec[1] = math.Clamp(vec[1], 1, 100000)
		vec[2] = math.Clamp(vec[2], 1, 100000)
		vec[3] = math.Clamp(vec[3], 1, 100000)

		phys:SetInertia(vec)
	end


	local validGameFlags = FVPHYSICS_DMG_DISSOLVE + FVPHYSICS_DMG_SLICE + FVPHYSICS_HEAVY_OBJECT + FVPHYSICS_NO_IMPACT_DMG +
		FVPHYSICS_NO_NPC_IMPACT_DMG + FVPHYSICS_NO_PLAYER_PICKUP
	--- Adds game flags to the physics object. Some flags cannot be modified
	-- @param flags The flags to add. FVPHYSICS enum. Can be:<br>
	-- FVPHYSICS.DMG_DISSOLVE<br>
	-- FVPHYSICS.DMG_SLICE<br>
	-- FVPHYSICS.HEAVY_OBJECT<br>
	-- FVPHYSICS.NO_IMPACT_DMG<br>
	-- FVPHYSICS.NO_NPC_IMPACT_DMG<br>
	-- FVPHYSICS.NO_PLAYER_PICKUP<br>
	function physobj_methods:AddGameFlag(flags)
		checktype(flags, "number")
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.canTool")
		local invalidFlags = bit.band(bit.bnot(validGameFlags), flags)
		if invalidFlags == 0 then
			phys:AddGameFlag(flags)
		else
			SF.Throw("Invalid flags " .. invalidFlags, 2)
		end
	end

	--- Clears game flags from the physics object. Some flags cannot be modified
	-- @param flags The flags to add. FVPHYSICS enum. Can be:<br>
	-- FVPHYSICS.DMG_DISSOLVE<br>
	-- FVPHYSICS.DMG_SLICE<br>
	-- FVPHYSICS.HEAVY_OBJECT<br>
	-- FVPHYSICS.NO_IMPACT_DMG<br>
	-- FVPHYSICS.NO_NPC_IMPACT_DMG<br>
	-- FVPHYSICS.NO_PLAYER_PICKUP<br>
	function physobj_methods:ClearGameFlag(flags)
		checktype(flags, "number")
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.canTool")
		local invalidFlags = bit.band(bit.bnot(validGameFlags), flags)
		if invalidFlags == 0 then
			phys:ClearGameFlag(flags)
		else
			SF.Throw("Invalid flags " .. invalidFlags, 2)
		end
	end

	--- Returns whether the game flags of the physics object are set.
	-- @param flags The flags to test. FVPHYSICS enum.
	-- @return boolean If the flags are set
	function physobj_methods:HasGameFlag(flags)
		checktype(flags, "number")
		local phys = unwrap(self)
		return phys:HasGameFlag(flags)
	end

	--- Sets bone gravity
	-- @param grav Bool should the bone respect gravity?
	function physobj_methods:EnableGravity (grav)
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.enableGravity")
		phys:EnableGravity(grav and true or false)
		phys:Wake()
	end

	--- Sets the bone drag state
	-- @param drag Bool should the bone have air resistence?
	function physobj_methods:EnableDrag (drag)
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.enableDrag")
		phys:EnableDrag(drag and true or false)
	end

	--- Sets the bone movement state
	-- @param move Bool should the bone move?
	function physobj_methods:EnableMotion (move)
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.enableMotion")
		phys:EnableMotion(move and true or false)
		phys:Wake()
	end

	--- Makes a sleeping physobj wakeup
	-- @server
	function physobj_methods:Wake()
		local phys = unwrap(self)
		checkpermission(SF.instance.player, phys:GetEntity(), "entities.applyForce")
		phys:Wake()
	end
end
