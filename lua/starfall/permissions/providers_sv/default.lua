--- Default starfall permission provider

-- start the provider table and set it to inherit from the default provider
local P = {}
P.__index = SF.Permissions.Provider
setmetatable( P, P )

-- localize the result set
local ALLOW = SF.Permissions.Result.ALLOW
local DENY = SF.Permissions.Result.DENY
local NEUTRAL = SF.Permissions.Result.NEUTRAL

function P:check ( principal, target, key )
    if principal:IsAdmin() or principal:IsSuperAdmin() then
        return ALLOW
	else
		return NEUTRAL
	end
end

-- register the provider
SF.Permissions.registerProvider( P )
