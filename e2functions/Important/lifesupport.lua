E2Lib.RegisterExtension("Lifesupport", true)

local isOwner = E2Lib.isOwner

registerCallback("e2lib_replace_function", function(funcname, func, oldfunc)
	if funcname == "isOwner" then 
		isOwner = func
	end
end)

local function IsRDDevice(ent)
	if ent.IsLSEnt then
		return true
	end
	
	return false
end

--=======================================
-- GENERAL RESOURCE FUNCTIONS
--=======================================

__e2setcost(1)
e2function number entity:isNode()
	if IsValid(this) then
		if this.IsNode then
			return 1
		end
	end
	
	return 0
end

__e2setcost(1)
e2function number entity:isLSDevice()
	if IsValid(this) then
		if this.IsLSEnt then
			return 1
		end
	end
	
	return 0
end

__e2setcost(2)
e2function number entity:lsCapacity(string res)
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		return this:GetMaxAmount(res)
	end
	
	return 0
end

__e2setcost(2)
e2function number entity:lsAmount(string res)
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		return this:GetAmount(res)
	end
	
	return 0
end

e2function string entity:lsDeviceName()
	if IsValid(this) then
		if this.GeneratorInfo then
			return this.GeneratorInfo:GetName()
		elseif this.PrintName then
			return this.PrintName
		else
			return ""
		end
	end
	
	return ""
end

__e2setcost(5)
e2function array entity:lsResourceList()
	local temp = {}
	if IsValid(this) and isOwner(self,this) and this.IsLSEnt then
		local storages = this:GetStorage()
		local num = 1
		for i,k in pairs(storages) do
            for name, res in pairs(k:GetStored()) do
			    temp[num] = name
			    num = num + 1
            end
		end
	end
	
	return temp
end

--Returns all of the entities which are part of the current res net.
--Only works on resource node entities!
__e2setcost(5)
e2function array entity:lsNetEntities()
	local temp = {}
	if IsValid(this) and isOwner(self,this) and this.IsNode then
		local res = this:GetLinks()
		local num = 1
		for i,k in pairs(res) do
			temp[num] = i
			num = num + 1
		end
	end
	
	return temp
end

__e2setcost(5)
e2function number entity:lsNodeSize()
	if IsValid(this) then
		if this.IsNode then
			return this:GetNodeRadius()
		end
	end
	
	return -1
end

-- Drains a resource from a network.
e2function number entity:lsConsumeResource(string name, amount)
	-- Check permissions
	if not self.player:IsAdmin() then return -3 end --Not allowed
	if not IsValid(this) then return -1 end -- Not valid.
	if amount < 0 then amount = math.abs(amount) end

	if this.IsLSEnt and (IsValid(this:GetNode()) or this.IsNode) then
		if this:ConsumeResource(name,amount) then return 1 end -- nom nom nom
		return 0
	end
	return -2 -- We can't do that, Jim.
end

--== End of General Resource Functions ==

--=======================================
-- ATMOSPHERE FUNCTIONS
--=======================================

--Environment Accessor Functions
e2function array getPlanets()
	local planets = {}
	for k,v in pairs(GAMEMODE.AtmosphereTable) do
		planets[k] = v:GetCelestial():getEntity()
	end
	
	return planets
end

--Get Environment from Ent
local function GetEnv(ent)
	if not IsValid(ent) then return nil end
	
	--Is an Environment
	if ent:GetClass() == "sc_planet" then
		return ent:GetCelestial():GetEnvironment()
		
	--Get Environment Reference
	else
		if ent.GetEnvironment then return ent:GetEnvironment() end
		
		return nil
	end
end

e2function string entity:lsName()
	if not IsValid(this) then return "Space" end
	
	local env = GetEnv(this)
	if env then
		return env:GetName()
	end
	
	return "Space"
end

e2function number entity:lsSize()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env and env:GetName() ~= "Space" then
		return env:GetRadius()
	else
		return math.huge
	end
	
	return -1
end

e2function number entity:lsTemperature()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetTemperature()
	end
	
	return -1
end

e2function number entity:lsGravity()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetGravity()
	end
	
	return -1
end

e2function number entity:lsPressure()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetPressure()
	end
	
	return -1
end

e2function number entity:lsO2Percent()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Oxygen")
		if res then
			return res:GetAmount() / res:GetMaxAmount()
		else
			return 0
		end
	end
	
	return -1
end

e2function number entity:lsCO2Percent()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Carbon Dioxide")
		if res then
			return res:GetAmount() / res:GetMaxAmount()
		else
			return 0
		end
	end
	
	return -1
end

e2function number entity:lsNPercent()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Nitrogen")
		if res then
			return res:GetAmount() / res:GetMaxAmount()
		else
			return 0
		end
	end
	
	return -1
end

e2function number entity:lsHPercent()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		local res = env:GetResource("Hydrogen")
		if res then
			return res:GetAmount() / res:GetMaxAmount()
		else
			return 0
		end
	end
	
	return -1
end

e2function number entity:lsVolume()
	if not IsValid(this)then return -1 end
	
	local env = GetEnv(this)
	if env and env:GetName() ~= "Space" then
		return env:GetVolume()
	else
		return math.huge
	end
	
	return -1
end

e2function number entity:lsO2Amount()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetResourceAmount("Oxygen")
	end
	
	return -1
end

e2function number entity:lsCO2Amount()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetResourceAmount("CO2")
	end
	
	return -1
end

e2function number entity:lsNAmount()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetResourceAmount("Nitrogen")
	end
	
	return -1
end

e2function number entity:lsHAmount()
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetResourceAmount("Hydrogen")
	end
	
	return -1
end

--Return all resources that currently exist within the atmosphere.
e2function array entity:lsEnvResources()
	if not IsValid(this) then return {} end
	
	local env = GetEnv(this)
	if env then
		local resources = env:GetResources()
		local resReturn = {}
		for k,v in pairs(resources) do
			table.insert(resReturn, v:GetName())
		end
		
		return resReturn
	end
	
	return {}
end

--Return the amount of the specified resource within the atmosphere.
e2function number entity:lsResAmount(string res)
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		return env:GetResourceAmount(res)
	end
	
	return -1
end

e2function number entity:lsResPercent(string res)
	if not IsValid(this) then return -1 end
	
	local env = GetEnv(this)
	if env then
		local res1 = env:GetResource(res)
		if res1 then
			return res1:GetAmount() / res1:GetMaxAmount()
		else
			return 0
		end
	end
	
	return -1
end

--Get the atmosphere an ent is being managed by
e2function entity entity:lsGetEnvironment()
	if not IsValid(this) then return nil end
	
	local env = GetEnv(this)
	if env then
		local celestial = env:GetCelestial()
		if celestial then
			return celestial:getEntity()
		end
	end
	
	return nil
end

e2function vector lsGetSunDirection()
	return GAMEMODE:GetSunDir()
end

--Tell spacebuild to ignore this entity's gravity (for propcore stuff)
e2function void entity:sbSetGravityOverride(number)
	if not IsValid(this) then return end
	
	this.HasGravityOverride = (number ~= 0)
end

e2function number entity:sbGetGravityOverride()
	if not IsValid(this) then return 0 end
	
	if this.HasGravityOverride then return 1 end
	return 0
end

--The above, for drag
e2function void entity:sbSetDragOverride(number)
	if not IsValid(this) then return end
	
	this.HasDragOverride = (number ~= 0)
end

e2function number entity:sbGetDragOverride()
	if not IsValid(this) then return 0 end
	
	if this.HasDragOverride then return 1 end
	return 0
end

--=== End of Atmosphere Functions =======


--=======================================
-- SUIT INFO FUNCTIONS
--=======================================

__e2setcost(1)
e2function number entity:lsSuitLifesupport()
	if CAF then
		if this.SB4Suit then
			return (this.SB4Suit:GetIntake() / this.SB4Suit:GetMaxIntake())*100
		end
	end
	
	return 0
end

--=== End of Suit Info Functions ========


--=======================================
-- RESOURCE PUMP FUNCTIONS
--=======================================

local function isPump(ent)
	if ent == nil or not IsValid(ent) then return false end
	
	return ent:GetClass() == "sc_resource_pump"
end

__e2setcost(5)
e2function array entity:lsPumpGetNearby()
    if not isPump(this) then return {} end
	local nearby = {}
	
	for k,v in pairs(ResourcePumpTable) do
		if not v:IsLinked() then continue end		--Don't do anything if we are not linked
		if v == this then continue end				--Don't include this pump
		if v:GetNode() == this:GetNode() then continue end	--Don't include pumps on same network
		
		local dist = v:GetPos():DistToSqr(this:GetPos())
		local maxDist = GetConVarNumber("sc_resourcepump_maxrange")
		maxDist = maxDist * maxDist
		
		if dist < maxDist then
			table.insert(nearby, v)
		end
	end
	
	return nearby
end

__e2setcost(1)
e2function number entity:lsPumpConnect(entity pump)
	if not isPump(this) or not isPump(pump) then return -1 end
	
	return this:ConnectToPump(pump) and 1 or 0
end

__e2setcost(1)
e2function number entity:lsPumpDisconnect()
	if not isPump(this) then return -1 end
	
	this:Disconnect()
	return 1
end

__e2setcost(1)
e2function entity entity:lsPumpGetConnection()
	if not isPump(this) then return NullEntity() end
	
	return this:GetConnectedPump() or NullEntity()
end

__e2setcost(1)
e2function number entity:lsPumpGetTransferSpeed()
	if not isPump(this) then return -1 end
	
	return this:GetTransferSpeed()
end

__e2setcost(1)
e2function number entity:lsPumpQueueTask(string resourceName, number amount, number output)
    if amount ~= amount then return -2 end -- if its nan, fuck off
	if not isPump(this) then return -1 end
	
	return this:AddTask(resourceName, amount, (output ~= 0)) and 1 or 0
end

__e2setcost(1)
e2function number entity:lsPumpRemoveTask(number task)
	if not isPump(this) then return -1 end
	
	this:RemoveTask(task)
	return 1
end

local function taskToE2Table(self, taskNum)
	local ret = {n={},ntypes={},s={},stypes={},size=0}
	
	if not IsValid(self) then return ret end
	if not self:GetClass() == "sc_resource_pump" then return ret end
	
	--Get Current Task
	local task = {}
	if taskNum == 0 then
		task = self:GetCurrentTask()
	else
		task = self:GetQueue()[taskNum]
	end
	
	if task == nil then return ret end
	
	--Convert to E2 table
	ret.s.Resource = task.Resource
	ret.stypes.Resource = "s"
	
	ret.s.Amount = task.Amount
	ret.stypes.Amount = "n"
	
	ret.s.Output = task.Output
	ret.stypes.Output = "n"

	ret.size = 3
	
	return ret
end

__e2setcost(1)
e2function table entity:lsPumpGetTask(number task)
	if not isPump(this) then return {n={},ntypes={},s={},stypes={},size=0} end
	
	return taskToE2Table(this, task)
end

__e2setcost(1)
e2function number entity:lsPumpGetQueueLength()
	if not isPump(this) then return -1 end
	
	local num = 0
	
	if this:GetCurrentTask() then num = 1 end
	num = num + #this:GetQueue()
	
	return num
end

__e2setcost(1)
e2function number entity:lsPumpGetTaskProgress()
	if not isPump(this) then return -1 end
	
	local task = this:GetCurrentTask()
	if task then
		return task.Progress
	end
	
	return 0
end

--Run On Task Completion Hook
__e2setcost(10)
e2function void runOnPumpTaskComplete(entity pump, number toggle)
	if not isPump(pump) then return end
	
	--Toggle On
	if toggle then
		pump.E2Hooks[self.entity] = true
		self.data.lifesupport.pumpHook[pump] = true
	
	--Toggle Off
	else
		pump.E2Hooks[self.entity] = nil
		self.data.lifesupport.pumpHook[pump] = nil
	end
end

__e2setcost(1)
e2function number lsPumpCompleteClk()
	if self.entity.RunByRDPump ~= 1 then return 0 end
	return 1
end

__e2setcost(1)
e2function entity lsPumpCompleted()
	if self.entity.RunByRDPump ~= 1 then return NullEntity() end
	
	return self.entity.CompletedPump
end

registerCallback("construct",function(self)
	self.data.lifesupport = {}
	self.data.lifesupport.pumpHook = {}
end)

registerCallback("destruct",function(self)
	for v,_ in pairs( self.data.lifesupport.pumpHook ) do
		if IsValid( v ) then
			v.E2Hooks[self.entity] = nil
		end
	end
	self.data.lifesupport.pumpHook = {}
end)

--=== End of Resource Pump Functions ====