-------------------------------------------------------------------------------
-- Builtins.
-- Functions built-in to the default environment
-------------------------------------------------------------------------------

local dgetmeta = debug.getmetatable

--- Built in values. These don't need to be loaded; they are in the default environment.
-- @name builtin
-- @shared
-- @class library
-- @libtbl SF.DefaultEnvironment

-- ------------------------- Lua Ports ------------------------- --
-- This part is messy because of LuaDoc stuff.

--- Returns the entity with index 'num'
-- @name SF.DefaultEnvironment.entity
-- @class function
-- @param num Entity index
-- @return entity
SF.DefaultEnvironment.Entity = function(id)
    SF.CheckType(id, "number")
    return SF.WrapObject(Entity(id))
end

--- Used to select single values from a vararg or get the count of values in it.
-- @name SF.DefaultEnvironment.select
-- @class function
-- @param parameter
-- @param vararg
-- @return Returns a number or vararg, depending on the select method.
SF.DefaultEnvironment.select = select

--- Attempts to convert the value to a string.
-- @name SF.DefaultEnvironment.tostring
-- @class function
-- @param obj
-- @return obj as string
SF.DefaultEnvironment.tostring = tostring

--- Attempts to convert the value to a number.
-- @name SF.DefaultEnvironment.tonumber
-- @class function
-- @param obj
-- @return obj as number
SF.DefaultEnvironment.tonumber = tonumber

--- Returns an iterator function for a for loop, to return ordered key-value pairs from a table.
-- @name SF.DefaultEnvironment.ipairs
-- @class function
-- @param tbl Table to iterate over
-- @return Iterator function
-- @return Table tbl
-- @return 0 as current index
SF.DefaultEnvironment.ipairs = ipairs

--- Returns an iterator function for a for loop that will return the values of the specified table in an arbitrary order.
-- @name SF.DefaultEnvironment.pairs
-- @class function
-- @param tbl Table to iterate over
-- @return Iterator function
-- @return Table tbl
-- @return nil as current index
SF.DefaultEnvironment.pairs = pairs

--- Returns a string representing the name of the type of the passed object.
-- @name SF.DefaultEnvironment.type
-- @class function
-- @param obj Object to get type of
-- @return The name of the object's type.
SF.DefaultEnvironment.type = function ( obj )
	local tp = getmetatable( obj )
	return type( tp ) == "string" and tp or type( obj )
end

--- Returns the next key and value pair in a table.
-- @name SF.DefaultEnvironment.next
-- @class function
-- @param tbl Table to get the next key-value pair of
-- @param k Previous key (can be nil)
-- @return Key or nil
-- @return Value or nil
SF.DefaultEnvironment.next = next

--- If the result of the first argument is false or nil, an error is thrown with the second argument as the message.
-- @name SF.DefaultEnvironment.assert
-- @class function
-- @param condition
-- @param msg
SF.DefaultEnvironment.assert = function ( condition, msg ) if not condition then SF.throw( msg or "assertion failed!", 2 ) end end
--- Same as Lua's unpack
-- @name SF.DefaultEnvironment.unpack
-- @class function
-- @param tbl
-- @return Elements of tbl
SF.DefaultEnvironment.unpack = unpack

--- Sets, changes or removes a table's metatable. Doesn't work on most internal metatables
-- @name SF.DefaultEnvironment.setmetatable
-- @class function
-- @param tbl The table to set the metatable of
-- @param meta The metatable to use
-- @return tbl with metatable set to meta
SF.DefaultEnvironment.setmetatable = setmetatable

--- Returns the metatable of an object. Doesn't work on most internal metatables
-- @param tbl Table to get metatable of
-- @return The metatable of tbl
SF.DefaultEnvironment.getmetatable = function( tbl )
	SF.CheckType( tbl, "table" )
	return getmetatable( tbl )
end

--- Generates the CRC checksum of the specified string. (https://en.wikipedia.org/wiki/Cyclic_redundancy_check)
-- @name SF.DefaultEnvironment.crc
-- @class function
-- @param stringToHash The string to calculate the checksum of
-- @return The unsigned 32 bit checksum as a string
SF.DefaultEnvironment.crc = util.CRC

--- Constant that denotes whether the code is executed on the client
-- @name SF.DefaultEnvironment.CLIENT
-- @class field
SF.DefaultEnvironment.CLIENT = CLIENT
--- Constant that denotes whether the code is executed on the server
-- @name SF.DefaultEnvironment.SERVER
-- @class field
SF.DefaultEnvironment.SERVER = SERVER

--- Returns if this is the first time this hook was predicted.
-- @name SF.DefaultEnvironment.isFirstTimePredicted
-- @class function
-- @return Boolean
SF.DefaultEnvironment.IsFirstTimePredicted = IsFirstTimePredicted

--- Returns the current count for this Think's CPU Time.
-- This value increases as more executions are done, may not be exactly as you want.
-- If used on screens, will show 0 if only rendering is done. Operations must be done in the Think loop for them to be counted.
-- @return Current quota used this Think
function SF.DefaultEnvironment.QuotaUsed ()
	return SF.instance.cpu_total
end

--- Gets the Average CPU Time in the buffer
-- @return Average CPU Time of the buffer.
function SF.DefaultEnvironment.QuotaAverage ()
	return SF.instance.cpu_average
end

--- Gets the CPU Time max.
-- CPU Time is stored in a buffer of N elements, if the average of this exceeds quotaMax, the chip will error.
-- @return Max SysTime allowed to take for execution of the chip in a Think.
function SF.DefaultEnvironment.QuotaMax ()
	return SF.instance.context.cpuTime.getMax()
end

--- Sets a CPU soft quota which will trigger a catchable error if the cpu goes over a certain amount.
-- @param quota The threshold where the soft error will be thrown. Ratio of current cpu to the max cpu usage. 0.5 is 50%
function SF.DefaultEnvironment.setSoftQuota(quota)
    SF.CheckType(quota, "number")
    self.cpu_softquota = quota
end

-- The below modules have the Gmod functions removed (the ones that begin with a capital letter),
-- as requested by Divran

-- Filters Gmod Lua files based on Garry's naming convention.
local function MergeTables ( lib, original )
	original = original or {}
	for name, func in pairs( lib ) do
		original[ name ] = func
	end
	return original
end

-- String library
local string_methods, string_metatable = SF.Typedef( "Library: string" )
MergeTables( string, string_methods )
string_metatable.__newindex = function () end

--- String library. http://wiki.garrysmod.com/page/Category:string
-- @name SF.DefaultEnvironment.string
-- @class table
SF.DefaultEnvironment.string = setmetatable( {}, string_metatable )

-- Math library
local math_methods, math_metatable = SF.Typedef( "Library: math" )
MergeTables( math, math_methods )
math_metatable.__newindex = function () end

--- Math library. http://wiki.garrysmod.com/page/Category:math
-- @name SF.DefaultEnvironment.math
-- @class table
SF.DefaultEnvironment.math = setmetatable( {}, math_metatable )

local os_methods, os_metatable = SF.Typedef( "Library: os" )
MergeTables( os, os_methods )
os_metatable.__newindex = function () end

--- OS library. http://wiki.garrysmod.com/page/Category:os
-- @name SF.DefaultEnvironment.os
-- @class table
SF.DefaultEnvironment.os = setmetatable( {}, os_metatable )

local table_methods, table_metatable = SF.Typedef( "Library: table" )
MergeTables( table,table_methods )
table_metatable.__newindex = function () end

--- Table library. http://wiki.garrysmod.com/page/Category:table
-- @name SF.DefaultEnvironment.table
-- @class table
SF.DefaultEnvironment.table = setmetatable( {}, table_metatable )

-- ------------------------- Functions ------------------------- --

--- Gets a list of all libraries
-- @return Table containing the names of each available library
function SF.DefaultEnvironment.GetLibraries ()
	local ret = {}
	for k,v in pairs( SF.Libraries.libraries ) do
		ret[ #ret + 1 ] = k
	end
	return ret
end

--- Checks if an object is valid, works similarly to the default GMod function
-- @param Thing The thing to check the validity of
-- @return Boolean
function SF.DefaultEnvironment.IsValid(Thing)
    return IsValid(Thing)
end



if SERVER then
	--- Prints a message to the player's chat.
	-- @shared
	-- @param ... Values to print
	function SF.DefaultEnvironment.print ( ... )
		local str = ""
		local tbl = { ... }
		for i = 1, #tbl do str = str .. tostring( tbl[ i ] ) .. ( i == #tbl and "" or "\t" ) end
		SF.instance.player:ChatPrint( str )
	end
else
	-- Prints a message to the player's chat.
	function SF.DefaultEnvironment.print ( ... )
		if SF.instance.player ~= LocalPlayer() then return end
		local str = ""
		local tbl = { ... }
		for i = 1 , #tbl do str = str .. tostring( tbl[ i ] ) .. ( i == #tbl and "" or "\t" ) end
		LocalPlayer():ChatPrint( str )
	end
end

local function printTableX ( target, t, indent, alreadyprinted )
	for k,v in SF.DefaultEnvironment.pairs( t ) do
		if SF.GetType( v ) == "table" and not alreadyprinted[ v ] then
			alreadyprinted[ v ] = true
			target:ChatPrint( string.rep( "\t", indent ) .. tostring( k ) .. ":" )
			printTableX( target, v, indent + 1, alreadyprinted )
		else
			target:ChatPrint( string.rep( "\t", indent ) .. tostring( k ) .. "\t=\t" .. tostring( v ) )
		end
	end
end

--- Prints a table to player's chat
-- @param tbl Table to print
function SF.DefaultEnvironment.PrintTable ( tbl )
	if CLIENT and SF.instance.player ~= LocalPlayer() then return end
	SF.CheckType( tbl, "table" )

	printTableX( ( SERVER and SF.instance.player or LocalPlayer() ), tbl, 0, { t = true } )
end


--- Runs an included script and caches the result.
-- Works pretty much like standard Lua require()
-- @param file The file to include. Make sure to --@include it
-- @return Return value of the script
function SF.DefaultEnvironment.require (file)
	SF.CheckType( file, "string" )
	local loaded = SF.instance.data.reqloaded
	if not loaded then
		loaded = {}
		SF.instance.data.reqloaded = loaded
	end

	if loaded[ file ] then
		return loaded[ file ]
	else
		local func = SF.instance.scripts[ file ]
		if not func then SF.throw( "Can't find file '" .. file .. "' ( Did you forget to --@include it? )", 2 ) end
		loaded[ file ] = func() or true
		return loaded[ file ]
	end
end

--- Runs an included script, but does not cache the result.
-- Pretty much like standard Lua dofile()
-- @param file The file to include. Make sure to --@include it
-- @return Return value of the script
function SF.DefaultEnvironment.dofile ( file )
	SF.CheckType( file, "string" )
	local func = SF.instance.scripts[ file ]
	if not func then SF.throw( "Can't find file '" .. file .. "' ( Did you forget to --@include it? )", 2 ) end
	return func()
end

--- GLua's loadstring
-- Works like loadstring, except that it executes by default in the main environment
-- @param str String to execute
-- @return Function of str
function SF.DefaultEnvironment.loadstring ( str )
	local func = CompileString( str, "SF: " .. tostring( SF.instance.env ), false )

	-- CompileString returns an error as a string, better check before setfenv
	if type( func ) == "function" then
		return setfenv( func, SF.instance.env )
	end

	return func
end

--- Lua's setfenv
-- Works like setfenv, but is restricted on functions
-- @param func Function to change environment of
-- @param tbl New environment
-- @return func with environment set to tbl
function SF.DefaultEnvironment.setfenv ( func, tbl )
	if type( func ) ~= "function" then SF.throw( "Main Thread is protected!", 2 ) end
	return setfenv( func, tbl )
end

--- Simple version of Lua's getfenv
-- Returns the current environment
-- @return Current environment
function SF.DefaultEnvironment.getfenv ()
	return getfenv()
end

local uncatchable = {
	["not enough memory"] = true,
	["stack overflow"] = true
}

--- Lua's pcall with SF throw implementation
-- Calls a function and catches an error that can be thrown while the execution of the call.
-- @param func Function to be executed and of which the errors should be caught of
-- @param arguments Arguments to call the function with.
-- @return If the function had no errors occur within it.
-- @return If an error occurred, this will be a string containing the error message. Otherwise, this will be the return values of the function passed in.
function SF.DefaultEnvironment.pcall (func, ...)
	local vret = { pcall(func, ...) }
	local ok, err = vret[1], vret[2]

	if ok then return unpack(vret) end

	if istable(err) then
		if err.uncatchable then
			error(err)
		end
	elseif uncatchable[err] then
		SF.Throw(err, 2, true)
	end

	return false, SF.Sanitize({err})[1]
end

local function xpcall_Callback (err)
	return {err, debug.traceback(tostring(err), 2)} -- only way to return 2 values; level 2 to branch
end

--- Lua's xpcall with SF throw implementation, and a traceback for debugging.
-- Attempts to call the first function. If the execution succeeds, this returns true followed by the returns of the function.
-- If execution fails, this returns false and the second function is called with the error message, and the stack trace.
-- @param func The function to call initially.
-- @param callback The function to be called if execution of the first fails; the error message and stack trace are passed.
-- @param ... Varargs to pass to the initial function.
-- @return Status of the execution; true for success, false for failure.
-- @return The returns of the first function if execution succeeded, otherwise the return values of the error callback.
function SF.DefaultEnvironment.xpcall (func, callback, ...)
	local vret = { xpcall(func, xpcall_Callback, ...) }
	local ok, errData = vret[1], vret[2]

	if ok then return unpack(vret) end

	local err, traceback = errData[1], errData[2]
	if istable(err) then
		if err.uncatchable then
			error(err)
		end
	elseif uncatchable[err] then
		SF.Throw(err, 2, true)
	end

	return false, callback(SF.Sanitize({err})[1], traceback)
end

--- Try to execute a function and catch possible exceptions
-- Similar to xpcall, but a bit more in-depth
-- @param func Function to execute
-- @param catch Function to execute in case func fails
function SF.DefaultEnvironment.try ( func, catch )
	local ok, err = pcall( func )
	if ok then return end

	if type( err ) == "table" then
		if err.uncatchable then
			error( err )
		end
	end
	catch( err )
end

--- Throws an exception
-- @param msg Message
-- @param level Which level in the stacktrace to blame. Defaults to one of invalid
-- @param uncatchable Makes this exception uncatchable
function SF.DefaultEnvironment.throw ( msg, level, uncatchable )
	local info = debug.getinfo( 1 + ( level or 1 ), "Sl" )
	local filename = info.short_src:match( "^SF:(.*)$" )
	if not filename then
		info = debug.getinfo( 2, "Sl" )
		filename = info.short_src:match( "^SF:(.*)$" )
	end
	local err = {
		uncatchable = false,
		file = filename,
		line = info.currentline,
		message = msg,
		uncatchable = uncatchable
	}
	error( err )
end

--- Throws a raw exception.
-- @param msg Exception message
function SF.DefaultEnvironment.error ( msg )
	error( msg or "an unspecified error occured", 2 )
end

--- Translates the specified position and angle into the specified coordinate system
-- @param pos The position that should be translated from the current to the new system
-- @param ang The angles that should be translated from the current to the new system
-- @param newSystemOrigin The origin of the system to translate to
-- @param newSystemAngles The angles of the system to translate to
-- @return localPos
-- @return localAngles
function SF.DefaultEnvironment.WorldToLocal(pos, ang, newSystemOrigin, newSystemAngles)
	SF.CheckType(pos, SF.Types["Vector"])
	SF.CheckType(ang, SF.Types["Angle"])
	SF.CheckType(newSystemOrigin, SF.Types["Vector"])
	SF.CheckType(newSystemAngles, SF.Types["Angle"])

	local localPos, localAngles = WorldToLocal(
		SF.UnwrapObject(pos),
		SF.UnwrapObject(ang),
		SF.UnwrapObject(newSystemOrigin),
		SF.UnwrapObject(newSystemAngles)
	)

	return SF.WrapObject(localPos), SF.WrapObject(localAngles)
end

--- Translates the specified position and angle from the specified local coordinate system
-- @param localPos The position vector that should be translated to world coordinates
-- @param localAng The angle that should be converted to a world angle
-- @param originPos The origin point of the source coordinate system, in world coordinates
-- @param originAngle The angles of the source coordinate system, as a world angle
-- @return worldPos
-- @return worldAngles
function SF.DefaultEnvironment.LocalToWorld(localPos, localAng, originPos, originAngle)
	SF.CheckType(localPos, SF.Types["Vector"])
	SF.CheckType(localAng, SF.Types["Angle"])
	SF.CheckType(originPos, SF.Types["Vector"])
	SF.CheckType(originAngle, SF.Types["Angle"])

	local worldPos, worldAngles = LocalToWorld(
		SF.UnwrapObject(localPos),
		SF.UnwrapObject(localAng),
		SF.UnwrapObject(originPos),
		SF.UnwrapObject(originAngle)
	)

	return SF.WrapObject(worldPos), SF.WrapObject(worldAngles)
end

--- Execute a console command
-- @param cmd Command to execute
function SF.DefaultEnvironment.concmd ( cmd )
	if CLIENT and SF.instance.player ~= LocalPlayer() then return end -- only execute on owner of screen
	SF.CheckType( cmd, "string" )
	SF.instance.player:ConCommand( cmd )
end

if CLIENT then
    --- Sets the chip's display name
	-- @client
	-- @param name Name
	function SF.DefaultEnvironment.SetStarfallName(name)
		SF.CheckType(name, "string")
		local e = SF.instance.data.entity
		if (e and e:IsValid()) then
			e.name = string.sub(name, 1, 256)
		end
	end

	--- Sets clipboard text. Only works on the owner of the chip.
	-- @param txt Text to set to the clipboard
	function SF.DefaultEnvironment.SetClipboardText(txt)
		if SF.instance.player ~= LocalPlayer() then return end
		SF.CheckType(txt, "string")
		SetClipboardText(txt)
	end

	--- Returns the local player's camera angles
	-- @client
	-- @return The local player's camera angles
	function SF.DefaultEnvironment.EyeAngles()
		return SF.WrapObject(LocalPlayer():EyeAngles())
	end

	--- Returns the local player's camera position
	-- @client
	-- @return The local player's camera position
	function SF.DefaultEnvironment.EyePos()
		return SF.WrapObject(LocalPlayer():EyePos())
	end

	--- Returns the local player's camera forward vector
	-- @client
	-- @return The local player's camera forward vector
	function SF.DefaultEnvironment.EyeVector()
		return SF.WrapObject(LocalPlayer():GetAimVector())
	end
end

-- ------------------------- Time ------------------------- --

--- Same as GLua's CurTime()
function SF.DefaultEnvironment.CurTime ()
	return CurTime()
end

--- Same as GLua's RealTime()
function SF.DefaultEnvironment.RealTime ()
	return RealTime()
end

--- Same as GLua's SysTime()
function SF.DefaultEnvironment.SysTime ()
	return SysTime()
end

--- Returns time between frames on client and ticks on server. Same thing as G.FrameTime in GLua
function SF.DefaultEnvironment.FrameTime ()
	return FrameTime()
end

-- ------------------------- Restrictions ------------------------- --
-- Restricts access to builtin type's metatables

local _R = debug.getregistry()
local function restrict( instance, hook, name, ok, err )
	_R.Vector.__metatable = "Vector"
	_R.Angle.__metatable = "Angle"
	_R.VMatrix.__metatable = "VMatrix"
end

local function unrestrict( instance, hook, name, ok, err )
	_R.Vector.__metatable = nil
	_R.Angle.__metatable = nil
	_R.VMatrix.__metatable = nil
end

SF.Libraries.AddHook( "prepare", restrict )
SF.Libraries.AddHook( "cleanup", unrestrict )

-- ------------------------- Hook Documentation ------------------------- --

--- Think hook. Called once per game tick
-- @name think
-- @class hook
-- @shared
