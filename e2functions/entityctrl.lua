/*-----------------------------------------
	Old functions
	Redirected to prop.lua 
	Do not change these.
-----------------------------------------*/

/************************************************************
****************          Dubby's extensions          ******************
************************************************************/

//Added so players can be affected to - feha
registerFunction("setVel", "e:v", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(validPhysics(rv1)) then
		local p = rv1:GetPhysicsObject()
		p:Wake()
		p:SetVelocity(Vector(rv2[1],rv2[2],rv2[3]))
	elseif (rv1:IsPlayer()) then
		vel = rv1:GetVelocity()
		setVel = Vector(rv2[1],rv2[2],rv2[3]) - vel
		rv1:SetVelocity(setVel)
	end
end)

registerFunction("setAngVel", "e:a", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(validPhysics(rv1)) then
		local p = rv1:GetPhysicsObject()
		p:Wake()
		local Sub = -1 * p:GetAngleVelocity()
		p:AddAngleVelocity(Sub + Vector(rv2[1],rv2[2],rv2[3]))
	end
end)

--just a variation of the above, but converts the vector into an angle
registerFunction("setAngVel", "e:v", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(validPhysics(rv1)) then
		local p = rv1:GetPhysicsObject()
		p:Wake()
		local Sub = -1 * p:GetAngleVelocity()
		p:AddAngleVelocity(Sub + Vector(rv2[1],rv2[3],rv2[2]))
	end
end)

--a more complex version of the above, designed to orient the physics object relative to the world orientation
registerFunction("setRotation", "e:v", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(!validPhysics(rv1)) then return end
	local p = rv1:GetPhysicsObject()
	local av = p:GetAngleVelocity() + Angle(rv2[1],rv2[3],rv2[2])
	p:Wake()
	av:Rotate(-1*rv1:EyeAngles())
	p:AddAngleVelocity(av)
end)

--variation of above, designed to take angle input
registerFunction("setRotation", "e:a", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(!validPhysics(rv1)) then return end
	local p = rv1:GetPhysicsObject()
	local av = p:GetAngleVelocity() + Angle(rv2[1],rv2[2],rv2[3])
	local v = Angle(av.pitch, av.roll, av.yaw)
	p:Wake()
	v:Rotate(-1*rv1:EyeAngles())
	p:AddAngleVelocity(av)
end)

registerFunction("setSolid", "e:n", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(rv1:IsPlayer()) then return end
	if(rv2==0) then
		rv1:DrawShadow(false)
		rv1:SetNotSolid(true)
		rv1:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
	end
	if(rv2==1) then
		rv1:DrawShadow(true)
		rv1:SetNotSolid(false)
		rv1:SetCollisionGroup(COLLISION_GROUP_NONE)
	end
end)
registerFunction("parentPly", "e:e", "", function(self, args)
	local op1, op2 = args[2], args[3]
	local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
	if(!IsValid(rv1)) then return end
	if(!validPhysics(rv1)) then return end
	rv1:SetParent(rv2)
end)