-- Vehicle type and functions

SF.Vehicles = {}

--- Vehicle type for GMod Vehicles<br>
-- They still behave exactly as <a href='./Entity.html'>Entities</a>, see Entities Type for defined functions
local vehicle_methods, vehicle_metamethods = SF.Typedef( "Vehicle", SF.Entities.Metatable )

local vwrap = SF.WrapObject

SF.Vehicles.Methods = vehicle_methods
SF.Vehicles.Metatable = vehicle_metamethods

--- Using a custom wrapper / unwrapper similar to the player wrapper

local function wrap ( obj )
	obj = SF.Entities.Wrap( obj )
	debug.setmetatable( obj, vehicle_metamethods )
	return obj
end

local unwrap = SF.Entities.Unwrap

SF.AddObjectWrapper( debug.getregistry().Vehicle, vehicle_metamethods, wrap )
SF.AddObjectUnwrapper( vehicle_metamethods, unwrap )

--- Adding the wrap / unwrap functions to the SF.Vehicles table
SF.Vehicles.Wrap = wrap
SF.Vehicles.Unwrap = unwrap

--- toString
--@shared
function vehicle_metamethods:__tostring ()
	local ent = unwrap( self )
	if not ent then
		return "(NULL Entity)"
	else
		return tostring( ent )
	end
end

--- Gets the entity that is driving the vehicle
-- @return The player driving the vehicle or nil if there is no driver
function vehicle_methods:GetDriver ()
	local ent = unwrap( self )
	return ent and SF.Players.Wrap( ent:GetDriver() ) or nil
end

--- Gets the entity that is the riding in the passenger seat of the vehicle
-- @param n The passenger number to get.
-- @return The passenger player of the vehicle or nil if there is no passenger
function vehicle_methods:GetPassenger ( n )
	SF.CheckType( n, "number" )

	local ent = unwrap( self )
	return ent and SF.Players.Wrap( ent:GetPassenger( n ) ) or nil
end