-------------------------------------------------------------------------------
-- Player Library Wrapper
-------------------------------------------------------------------------------

--- Player Library. Different methods for getting player data.
-- @shared
local player_library, _ = SF.Libraries.Register( "player" )

local vunwrap = SF.UnwrapObject
local wrap = SF.WrapObject

-- Register privileges
do
	local P = SF.Permissions
	P.registerPrivilege( "playerLib", "Player", "Allows the user to access the player library" )
end

local function convert ( results, func )
	if func then SF.CheckType( func, "function" ) end
	
	local t = {}
	local count = 1
	for i = 1, #results do
		local e = wrap( results[ i ] )
		if not func or func( e ) then
			t[ count ] = e
			count = count + 1
		end
	end
	return t
end

--- Finds all players (including bots)
-- @param filter Optional function to filter results, arguments: entity
-- @return An array of found players
function player_library.GetAll ( filter )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	return convert( player.GetAll(), filter )
end

--- Finds all bots
-- @param filter Optional function to filter results, arguments: entity
-- @return An array of found bots
function player_library.GetBots ( filter )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	return convert( player.GetBots(), filter )
end

--- Finds all human players (excludes bots)
-- @param filter Optional function to filter results, arguments: entity
-- @return An array of found bots
function player_library.GetHumans ( filter )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	return convert( player.GetHumans(), filter )
end

--- Gets a player by connection ID
-- @return Found Player
function player_library.GetByID ( connectionID )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	return wrap( player.GetByID( connectionID ) )
end

--- Gets a player by Steam ID
-- @return Found Player
function player_library.GetBySteamID ( steamID )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	return wrap( player.GetBySteamID( steamID ) )
end

--- Gets a player by SteamID64
-- @return Found Player
function player_library.GetBySteamID64 ( steamID )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	local ply = player.GetBySteamID64( steamID ) 
	
	if not ply then return false end
	return wrap( ply )
end

--- Gets a player by UniqueID
-- @return Found Player
function player_library.GetByUniqueID ( uniqueID )
	if not SF.Permissions.check( SF.instance.player, nil, "playerLib" ) then SF.throw( "Insufficient permissions", 2 ) end
	
	return wrap( player.GetByUniqueID( uniqueID ) )
end