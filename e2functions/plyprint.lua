-- Created by Nielk1

e2function number entity:plyPrint(string text)
	if not IsValid(this) then return 0 end
	if not this:IsPlayer() then return 0 end
	
//	if util.tobool( GetConVarNumber( "ulx_logChat" ) ) then
//		ulx.logString( self.player:Nick() .. " sent E2 plyPrint to " .. this:Nick() .. "> " .. text )
//	end
	
	this:ChatPrint(text)
	return 1
end

e2function number entity:plyPrint(print_type, string text)
	if not IsValid(this) then return 0 end
	if not this:IsPlayer() then return 0 end
	print_type = math.Clamp(print_type,1,4)

//	if util.tobool( GetConVarNumber( "ulx_logChat" ) ) then
//		ulx.logString( self.player:Nick() .. " sent E2 plyPrint to " .. this:Nick() .. "> " .. text )
//	end
	
	this:PrintMessage(print_type, text)
	return 1
end

e2function number entity:plyHint(string text, duration)
	if not IsValid(this) then return 0 end
	WireLib.AddNotify(this, text, NOTIFY_GENERIC, math.Clamp(duration,0.7,7))
	
//	if util.tobool( GetConVarNumber( "ulx_logChat" ) ) then
//		ulx.logString( self.player:Nick() .. " sent E2 plyHint to " .. this:Nick() .. "> " .. text )
//	end
	
	return 1
end


e2function number entity:plyPrintSimpleColor(string text, vector color_vec)
	if not IsValid(this) then return 0 end
	if not this:IsPlayer() then return 0 end

//	if util.tobool( GetConVarNumber( "ulx_logChat" ) ) then
//		ulx.logString( self.player:Nick() .. " sent E2 plyPrintSimpleColor to " .. this:Nick() .. "> " .. text )
//	end
	
    umsg.Start("wire_expression2_nielk1_plyPrintSimpleColor", this )
        umsg.String(text)
        umsg.Char(math.Clamp(math.floor(color_vec[1]),0,255)-128 )
		umsg.Char(math.Clamp(math.floor(color_vec[2]),0,255)-128 )
		umsg.Char(math.Clamp(math.floor(color_vec[3]),0,255)-128 )
    umsg.End()
	return 1
end