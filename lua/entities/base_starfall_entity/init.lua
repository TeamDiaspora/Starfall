AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")
include("starfall/SFLib.lua")
assert(SF, "Starfall didn't load correctly!")
local context = SF.CreateContext()
util.AddNetworkString("starfall_download")
util.AddNetworkString("starfall_update")
util.AddNetworkString("starfall_used")

local function sendCode(starfall, owner, files, mainfile, recipient)
    net.Start("starfall_download")
    net.WriteEntity(starfall)
    net.WriteEntity(owner)
    net.WriteString(mainfile)

    if recipient then
        net.Send(recipient)
    else
        net.Broadcast()
    end

    local fname = next(files)

    while fname do
        local fdata = files[fname]
        local offset = 1
        repeat
            net.Start("starfall_download")
            net.WriteBit(false)
            net.WriteString(fname)
            local data = fdata:sub(offset, offset + 60000)
            net.WriteString(data)

            if recipient then
                net.Send(recipient)
            else
                net.Broadcast()
            end

            offset = offset + #data + 1
        until offset > #fdata
        fname = next(files, fname)
    end

    net.Start("starfall_download")
    net.WriteBit(true)

    if recipient then
        net.Send(recipient)
    else
        net.Broadcast()
    end
end

local requests = {}

local function sendCodeRequest(ply, starfallid)
    local starfall = Entity(starfallid)

    if not starfall.mainfile or not starfall.client then
        if not requests[starfallid] then
            requests[starfallid] = {}
        end

        if requests[starfallid][player] then return end
        requests[starfallid][ply] = true

        return
    elseif starfall.mainfile then
        if requests[starfallid] then
            requests[starfallid][ply] = nil
        end

        sendCode(starfall, starfall.owner, starfall.files, starfall.mainfile, ply)
    end
end

local function retryCodeRequests()
    for starfallid, plys in pairs(requests) do
        for ply, _ in pairs(requests[starfallid]) do
            sendCodeRequest(ply, starfallid)
        end
    end
end

net.Receive("starfall_download", function(len, ply)
    local starfall = net.ReadEntity()
    sendCodeRequest(ply, starfall:EntIndex())
end)

function ENT:CodeSent(ply, files, mainfile)
    if ply ~= self.owner then return end
    self.files = files
    self.mainfile = mainfile
    self.server = true
    local ppdata = {}
    SF.Preprocessor.ParseDirectives(mainfile, files[mainfile], {}, ppdata)

    if self.mainfile ~= nil and ppdata.shared then
        self.client = true
        net.Start("starfall_update")
        net.WriteEntity(self)

        for k, v in pairs(files) do
            net.WriteBit(false)
            net.WriteString(k)
            net.WriteString(util.CRC(v))
        end

        net.WriteBit(true)
        net.Broadcast()
    end

    local ok, instance = SF.Compiler.Compile(files, context, mainfile, ply, {
        entity = self
    })

    if not ok then
        self:Error(instance)

        return
    end

    instance.runOnError = function(inst, ...)
        self:Error(...)
    end

    if self.instance then
        self.instance:deinitialize()
        self.instance = nil
    end

    self.instance = instance
    local ok, msg, traceback = instance:initialize()

    if not ok then
        self:Error(msg, traceback)

        return
    end

    self:SetNWInt("State", self.States.Normal)
end

function ENT:Initialize()
    baseclass.Get("base_gmodentity").Initialize(self)
    self:PhysicsInit(SOLID_VPHYSICS)
    self:SetMoveType(MOVETYPE_VPHYSICS)
    self:SetSolid(SOLID_VPHYSICS)
    self.instance = nil
    self:SetNWInt("State", self.States.None)
end

function ENT:OnRemove()
    if not self.instance then return end
    hook.Run("sf_deinitialize", self:EntIndex())
    self:runScriptHook("Removed")

    if self.instance then
        self.instance:deinitialize()
    end

    self.instance = nil
end

local i = 0

function ENT:UpdateCodeRetries()
    i = i + 1

    if i % 22 == 0 then
        retryCodeRequests()
        i = 0
    end
end

function ENT:Think()
    self:UpdateCodeRetries()
    self:NextThink(CurTime())

    if self.instance and not self.instance.error then
        local bufferAvg = self.instance:movingCPUAverage()
        self:SetNWInt("CPUus", math.Round(bufferAvg * 1000000))
        self:SetNWFloat("CPUpercent", math.floor(bufferAvg / self.instance.context.cpuTime.getMax() * 100))
        self.instance.cpu_total = 0
        self.instance.cpu_average = bufferAvg
        self:runScriptHook("think")
    end

    return true
end

-- Sends a net message to all clients about the use.
function ENT:Use(activator)
    if activator:IsPlayer() then
        net.Start("starfall_used")
        net.WriteEntity(self)
        net.WriteEntity(activator)
        net.Broadcast()
    end

    self:runScriptHook("starfallUsed", SF.Entities.Wrap(activator))
end

function ENT:OnRestore()
    if WireLib and WireLib.Restored then
        WireLib.Restored(self)
    end
end

function ENT:PreEntityCopy()
    local DupeInfo = self:BuildDupeInfo()
    duplicator.ClearEntityModifier(self, "WireDupeInfo")

    if DupeInfo and DupeInfo.Wire then
        duplicator.StoreEntityModifier(self, "WireDupeInfo", DupeInfo.Wire)
    end

    duplicator.ClearEntityModifier(self, "SFDupeInfo")

    if DupeInfo and DupeInfo.Starfall then
        duplicator.StoreEntityModifier(self, "SFDupeInfo", DupeInfo.Starfall)
    end
end

function ENT:PostEntityPaste(ply, ent, CreatedEntities)
    if ent.EntityMods then
        ent:ApplyDupeInfo(ply, ent, ent.EntityMods, function(id) return CreatedEntities[id] end)
    end
end

function ENT:BuildDupeInfo()
    local Info = {}

    if WireLib then
        Info.Wire = WireLib.BuildDupeInfo(self)
    end

    Info.Starfall = SF.SerializeCode(self.files, self.mainfile)

    return Info
end

function ENT:ApplyDupeInfo(Player, Entity, Info, GetEntByID)
    self.owner = Player
    self.Owner = Player
    local SFData = Info.SFDupeInfo

    if SFData then
        if SFData.starfall then
            SFData = SFData.starfall
        end

        local Code, Main = SF.DeserializeCode(SFData)
        self:CodeSent(Player, Code, Main)
    end

    if WireLib then
        WireLib.ApplyDupeInfo(Player, Entity, Info.WireDupeInfo, GetEntByID)
    end
end