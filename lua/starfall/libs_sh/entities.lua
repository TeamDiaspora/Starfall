-------------------------------------------------------------------------------
-- Shared entity library functions
-------------------------------------------------------------------------------

SF.Entities = {}

--- Entity type
-- @shared
local ents_methods, ents_metamethods = SF.Typedef( "Entity" )
local wrap, unwrap = SF.CreateWrapper( ents_metamethods, true, true, debug.getregistry().Entity )

local vwrap, vunwrap = SF.WrapObject, SF.UnwrapObject

--- Entities Library
-- @shared
local ents_lib, _ = SF.Libraries.Register( "entities" )

-- Register privileges
do
	local P = SF.Permissions
	P.registerPrivilege( "entities.setRenderProperty", "Set Render Property", "Allows the user to change the rendering properties of an entity" )
end

local materialBlacklist = {
	[ "pp/copy" ] = true
}

-- ------------------------- Internal functions ------------------------- --

SF.Entities.Wrap = wrap
SF.Entities.Unwrap = unwrap
SF.Entities.Methods = ents_methods
SF.Entities.Metatable = ents_metamethods
SF.Entities.Library = ents_lib

--- Returns true if valid and is not the world, false if not
-- @param entity Entity to check
function SF.Entities.IsValid ( entity )
	return IsValid(entity)
end
local isValid = SF.Entities.IsValid

--- Gets the physics object of the entity
-- @return The physobj, or nil if the entity isn't valid or isn't vphysics
function SF.Entities.GetPhysObject ( ent )
	return ( isValid( ent ) and ent:GetMoveType() == MOVETYPE_VPHYSICS and ent:GetPhysicsObject() ) or nil
end
local getPhysObject = SF.Entities.GetPhysObject

-- ------------------------- Library functions ------------------------- --

--- Returns the entity representing a processor that this script is running on.
-- May be nil
-- @return Starfall entity
function ents_lib.self ()
	local ent = SF.instance.data.entity
	if ent then
		return SF.Entities.Wrap( ent )
	else return nil end
end

--- Returns whoever created the script
-- @return Owner entity
function ents_lib.owner ()
	return SF.WrapObject( SF.instance.player )
end

--- Same as ents_lib.owner() on the server. On the client, returns the local player
-- @name ents_lib.player
-- @class function
-- @return Either the owner (server) or the local player (client)
if SERVER then
	ents_lib.player = ents_lib.owner
else
	function ents_lib.player ()
		return SF.WrapObject( LocalPlayer() )
	end
end

--- Returns the entity with index 'num'
-- @name ents_lib.entity
-- @class function
-- @param num Entity index
-- @return entity
function ents_lib.entity ( num )
	SF.CheckType( num, "number" )

	return SF.WrapObject( Entity( num ) )
end

-- ------------------------- Methods ------------------------- --

--- To string
-- @shared
function ents_metamethods:__tostring ()
	local ent = unwrap( self )
	if not ent then return "(null entity)"
	else return tostring( ent ) end
end

--- Gets the attachment index the entity is parented to
-- @shared
-- @return number index of the attachment the entity is parented to or 0
function ents_methods:GetParentAttachment()
	local ent = unwrap(self)
	return ent:GetParentAttachment()
end

--- Gets the attachment index via the entity and it's attachment name
-- @shared
-- @param name
-- @return number of the attachment index, or 0 if it doesn't exist
function ents_methods:LookupAttachment(name)
	local ent = unwrap(self)
	return ent:LookupAttachment(name)
end

--- Gets the position and angle of an attachment
-- @shared
-- @param index The index of the attachment
-- @return vector position, and angle orientation
function ents_methods:GetAttachment(index)
	local ent = unwrap(self)
	if ent then
		local t = ent:GetAttachment(index)
		return SF.Sanitize(t)
	end
end

--- Returns a table of attachments
-- @shared
-- @return table of attachment id and attachment name or nil
function ents_methods:GetAttachments()
	local ent = unwrap(self)
	return ent:GetAttachments()
end

--- Converts a ragdoll bone id to the corresponding physobject id
-- @param boneid The ragdoll boneid
-- @return The physobj id
function ents_methods:TranslateBoneToPhysBone(boneid)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.Throw("Entity is not valid.", 2) end
	return ent:TranslateBoneToPhysBone(boneid)
end

--- Converts a physobject id to the corresponding ragdoll bone id
-- @param boneid The physobject id
-- @return The ragdoll bone id
function ents_methods:TranslatePhysBoneToBone(boneid)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.Throw("Entity is not valid.", 2) end
	return ent:TranslatePhysBoneToBone(boneid)
end

--- Gets the number of physicsobjects of an entity
-- @return The number of physics objects on the entity
function ents_methods:GetPhysicsObjectCount()
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.Throw("Entity is not valid.", 2) end
	return ent:GetPhysicsObjectCount()
end

--- Gets the main physics objects of an entity
-- @return The main physics object of the entity
function ents_methods:GetPhysicsObject()
	local ent = unwrap(self)
	if not ((ent and ent:IsValid()) or ent:IsWorld()) then SF.Throw("Entity is not valid.", 2) end
	return SF.WrapObject(ent:GetPhysicsObject())
end

--- Gets a physics objects of an entity
-- @param id The physics object id (starts at 0)
-- @return The physics object of the entity
function ents_methods:GetPhysicsObjectNum(id)
    SF.CheckType(id, "number")
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.Throw("Entity is not valid.", 2) end
	return SF.WrapObject(ent:GetPhysicsObjectNum(id))
end

--- Sets the color of the entity
-- @shared
-- @param clr New color
function ents_methods:SetColor ( clr )
	SF.CheckType( clr, SF.Types[ "Color" ] )

	local this = unwrap( self )
	if IsValid( this ) then
		if not SF.Permissions.check( SF.instance.player, this, "entities.setRenderProperty" ) then return end
		this:SetColor( clr )
		this:SetRenderMode( clr.a == 255 and RENDERMODE_NORMAL or RENDERMODE_TRANSALPHA )
	end
end

--- Sets the whether an entity should be drawn or not
-- @shared
-- @param draw Whether to draw the entity or not.
function ents_methods:SetNoDraw(draw)
	SF.CheckType(self, ents_metamethods)

	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.Throw("Entity is not valid", 2) end
    if not SF.Permissions.check( SF.instance.player, this, "entities.setRenderProperty" ) then return end

	ent:SetNoDraw(draw and true or false)
end

--- Sets the Render Mode of the entity
-- @shared
-- @param mode One of the <a href="http://wiki.garrysmod.com/page/Enums/RENDERMODE">RenderMode Enumerations</a>
function ents_methods:SetRenderMode ( mode )
    SF.CheckType( mode, "number" )

    local this = unwrap( self )
    if IsValid( this ) then
        if not SF.Permissions.check( SF.instance.player, this, "entities.setRenderProperty" ) then return end
        this.SetRenderMode( mode )
    end
end
--- Sets the renderfx of the entity
-- @shared
-- @class function
-- @param renderfx Number, renderfx to use. http://wiki.garrysmod.com/page/Enums/kRenderFx
function ents_methods:setRenderFX(renderfx)
	SF.CheckType(self, ents_metamethods)
	SF.CheckType(renderfx, "number")

	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.Throw("Entity is not valid", 2) end
    if not SF.Permissions.check( SF.instance.player, this, "entities.setRenderProperty" ) then return end

	ent:SetRenderFX(renderfx)
	if SERVER then duplicator.StoreEntityModifier(ent, "colour", { RenderFX = renderfx }) end
end


--- Gets the color of an entity
-- @shared
-- @return Color
function ents_methods:GetColor ()
	local this = unwrap( self )
	return this:GetColor()
end

--- Casts a hologram entity into the hologram type
-- @shared
-- @return Hologram type
function ents_methods:ToHologram()
	SF.CheckType(self, ents_metamethods)
	if not unwrap(self).IsSFHologram then SF.Throw("The entity isn't a hologram", 2) end
	debug.setmetatable(self, SF.Holograms.Metatable)
	return self
end

--- Checks if an entity is valid.
-- @shared
-- @return True if valid, false if not
function ents_methods:IsValid ()
	SF.CheckType( self, ents_metamethods )
	return isValid( unwrap( self ) )
end

--- Checks if an entity is world.
-- @shared
-- @return True if world, false if not
function ents_methods:IsWorld()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsWorld()
end

--- Checks if an entity is a player.
-- @shared
-- @return True if player, false if not
function ents_methods:IsPlayer()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsPlayer()
end

--- Checks if an entity is a weapon.
-- @shared
-- @return True if weapon, false if not
function ents_methods:IsWeapon()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsWeapon()
end

--- Checks if an entity is a vehicle.
-- @shared
-- @return True if vehicle, false if not
function ents_methods:IsVehicle()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsVehicle()
end

--- Checks if an entity is an npc.
-- @shared
-- @return True if npc, false if not
function ents_methods:IsNPC()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsNPC()
end

--- Checks if the entity ONGROUND flag is set
-- @shared
-- @return Boolean if it's flag is set or not
function ents_methods:IsOnGround()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsOnGround()
end

--- Returns if the entity is ignited
-- @shared
-- @return Boolean if the entity is on fire or not
function ents_methods:IsOnFire()
	SF.CheckType(self, ents_metamethods)
	return unwrap(self):IsOnFire()
end

--- Returns the chip's name of E2s or starfalls
function ents_methods:GetGateName()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if ent.GetGateName then
		return ent:GetGateName()
	else
		return "N/A"
	end
end

--- Returns the EntIndex of the entity
-- @shared
-- @return The numerical index of the entity
function ents_methods:EntIndex ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return ent:EntIndex()
end

--- Returns the class of the entity
-- @shared
-- @return The string class name
function ents_methods:GetClass ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return ent:GetClass()
end

--- Gets the parent of the entity
-- @shared
-- @return Parent
function ents_methods:GetParent()
	SF.CheckType(self, ents_metamethods)
    local ent = unwrap(self)
    if not isValid(ent) then return nil, "invalid entity" end
    local Parent = ent:GetParent()

    if not IsValid(Parent) then
        return nil
    end

	return wrap(Parent)
end

--- Gets the children (the parented entities) of an entity
-- @shared
-- @return table of parented children
function ents_methods:GetChildren()
	local ent = unwrap(self)
	return SF.Sanitize(ent:GetChildren())
end

--- Returns the position of the entity
-- @shared
-- @return The position vector
function ents_methods:GetPos ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:GetPos() )
end

--- Returns how submerged the entity is in water
-- @shared
-- @return The water level. 0 none, 1 slightly, 2 at least halfway, 3 all the way
function ents_methods:WaterLevel()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	return ent:WaterLevel()
end

--- Returns the ragdoll bone index given a bone name
-- @shared
-- @param name The bone's string name
-- @return The bone index
function ents_methods:LookupBone(name)
    SF.CheckType(name, "string")
	return unwrap(self):LookupBone(name)
end

--- Returns the matrix of the entity's bone. Note: this method is slow/doesnt work well if the entity isn't animated.
-- @shared
-- @param bone Bone index. (def 0)
-- @return The matrix
function ents_methods:GetBoneMatrix(bone)
	SF.CheckType(self, ents_metamethods)
	if bone == nil then bone = 0 else SF.CheckType(bone, "number") end

	local ent = unwrap(self)
	return SF.WrapObject(ent:GetBoneMatrix(bone))
end

--- Returns the world transform matrix of the entity
-- @shared
-- @return The matrix
function ents_methods:GetWorldTransformMatrix()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	return SF.WrapObject(ent:GetWorldTransformMatrix())
end

--- Returns the number of an entity's bones
-- @shared
-- @return Number of bones
function ents_methods:GetBoneCount()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	return ent:GetBoneCount()
end

--- Returns the name of an entity's bone
-- @shared
-- @param bone Bone index. (def 0)
-- @return Name of the bone
function ents_methods:GetBoneName(bone)
	SF.CheckType(self, ents_metamethods)
	if bone == nil then bone = 0 else SF.CheckType(bone, "number") end
	local ent = unwrap(self)
	return ent:GetBoneName(bone)
end

--- Returns the parent index of an entity's bone
-- @shared
-- @param bone Bone index. (def 0)
-- @return Parent index of the bone
function ents_methods:GetBoneParent(bone)
	SF.CheckType(self, ents_metamethods)
	if bone == nil then bone = 0 else SF.CheckType(bone, "number") end
	local ent = unwrap(self)
	return ent:GetBoneParent(bone)
end

--- Returns the bone's position and angle in world coordinates
-- @shared
-- @param bone Bone index. (def 0)
-- @return Position of the bone
-- @return Angle of the bone
function ents_methods:GetBonePosition(bone)
	SF.CheckType(self, ents_metamethods)
	if bone == nil then bone = 0 else SF.CheckType(bone, "number") end
	local ent = unwrap(self)
	local pos, ang = ent:GetBonePosition(bone)
	return SF.WrapObject(pos), SF.WrapObject(ang)
end

--- Returns the x, y, z size of the entity's outer bounding box (local to the entity)
-- @shared
-- @return The outer bounding box size
function ents_methods:OBBSize ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:OBBMaxs() - ent:OBBMins() )
end

--- Returns the x, y, z size of the entity's maximum bounding box vertex (local to the entity)
-- @shared
-- @return The entity's maximum bounding box vertex
function ents_methods:OBBMaxs ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:OBBMaxs() )
end

--- Returns the x, y, z size of the entity's minimum bounding box vertex (local to the entity)
-- @shared
-- @return The entity's minimum bounding box vertex
function ents_methods:OBBMins ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:OBBMins() )
end

--- Returns the local position of the entity's outer bounding box
-- @shared
-- @return The position vector of the outer bounding box center
function ents_methods:OBBCenter ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:OBBCenter() )
end

--- Returns the world position of the entity's outer bounding box
-- @shared
-- @return The position vector of the outer bounding box center
function ents_methods:OBBCenterW ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:LocalToWorld( ent:OBBCenter() ) )
end

--- Returns the local position of the entity's mass center
-- @shared
-- @return The position vector of the mass center
function ents_methods:GetMassCenter ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	local phys = getPhysObject( ent )
	if not phys or not phys:IsValid() then return nil, "entity has no physics object or is not valid" end
	return SF.WrapObject( phys:GetMassCenter() )
end

--- Returns the world position of the entity's mass center
-- @shared
-- @return The position vector of the mass center
function ents_methods:GetMassCenterW ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	local phys = getPhysObject( ent )
	if not phys or not phys:IsValid() then return nil, "entity has no physics object or is not valid" end
	return SF.WrapObject( ent:LocalToWorld( phys:GetMassCenter() ) )
end

--- Returns the angle of the entity
-- @shared
-- @return The angle
function ents_methods:GetAngles ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:GetAngles() )
end

--- Returns the mass of the entity
-- @shared
-- @return The numerical mass
function ents_methods:GetMass ()
	SF.CheckType( self, ents_metamethods )

	local ent = unwrap( self )
	local phys = getPhysObject( ent )
	if not phys or not phys:IsValid() then return nil, "entity has no physics object or is not valid" end

	return phys:GetMass()
end

--- Returns the principle moments of inertia of the entity
-- @shared
-- @return The principle moments of inertia as a vector
function ents_methods:GetInertia ()
	SF.CheckType( self, ents_metamethods )

	local ent = unwrap( self )
	local phys = getPhysObject( ent )
	if not phys or not phys:IsValid() then return nil, "entity has no physics object or is not valid" end

	return phys:GetInertia()
end

--- Returns the velocity of the entity
-- @shared
-- @return The velocity vector
function ents_methods:GetVelocity ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:GetVelocity() )
end

--- Returns the angular velocity of the entity
-- @shared
-- @return The angular velocity vector
function ents_methods:GetAngleVelocity ()
	SF.CheckType( self, ents_metamethods )
	local phys = getPhysObject( unwrap( self ) )
	if not phys or not phys:IsValid() then return nil, "entity has no physics object or is not valid" end
	return SF.WrapObject( phys:GetAngleVelocity() )
end

--- Converts a vector in entity local space to world space
-- @shared
-- @param data Local space vector
-- @return data as world space vector
function ents_methods:LocalToWorld ( data )
	SF.CheckType( self, ents_metamethods )
	SF.CheckType( data, SF.Types[ "Vector" ] )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end

	return SF.WrapObject( ent:LocalToWorld( vunwrap( data ) ) )
end

--- Converts an angle in entity local space to world space
-- @shared
-- @param data Local space angle
-- @return data as world space angle
function ents_methods:LocalToWorldAngles ( data )
	SF.CheckType( self, ents_metamethods )
	SF.CheckType( data, SF.Types[ "Angle" ] )
	local ent = unwrap( self )
	local data = SF.UnwrapObject( data )
	if not isValid( ent ) then return nil, "invalid entity" end

	return SF.WrapObject( ent:LocalToWorldAngles( data ) )
end

--- Converts a vector in world space to entity local space
-- @shared
-- @param data World space vector
-- @return data as local space vector
function ents_methods:WorldToLocal ( data )
	SF.CheckType( self, ents_metamethods )
	SF.CheckType( data, SF.Types[ "Vector" ] )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end

	return SF.WrapObject( ent:WorldToLocal( vunwrap( data ) ) )
end

--- Converts an angle in world space to entity local space
-- @shared
-- @param data World space angle
-- @return data as local space angle
function ents_methods:WorldToLocalAngles ( data )
	SF.CheckType( self, ents_metamethods )
	SF.CheckType( data, SF.Types[ "Angle" ] )
	local ent = unwrap( self )
	local data = SF.UnwrapObject( data )
	if not isValid( ent ) then return nil, "invalid entity" end

	return SF.WrapObject( ent:WorldToLocalAngles( data ) )
end

--- Gets the animation number from the animation name
-- @param animation Name of the animation
-- @return Animation index or -1 if invalid
function ents_methods:LookupSequence(animation)
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end

	SF.CheckType(animation, "string")

	return ent:LookupSequence(animation)
end

--- Get the length of an animation
-- @param id (Optional) The id of the sequence, or will default to the currently playing sequence
-- @return Length of the animation in seconds
function ents_methods:SequenceDuration(id)
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end

	if id~=nil then SF.CheckType(id, "number")end

	return ent:SequenceDuration(id)
end

--- Set the pose value of an animation. Turret/Head angles for example.
-- @param pose Name of the pose parameter
-- @param value Value to set it to.
function ents_methods:SetPoseParameter(pose, value)
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end
	if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end

	ent:SetPoseParameter(pose, value)
end

--- Get the pose value of an animation
-- @param pose Pose parameter name
-- @return Value of the pose parameter
function ents_methods:GetPoseParameter(pose)
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end

	return ent:GetPoseParameter(pose)
end

--- Returns a table of flexname -> flexid pairs for use in flex functions.
function ents_methods:GetFlexes()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end

	local flexes = {}
	for i = 0, ent:GetFlexNum()-1 do
		flexes[ent:GetFlexName(i)] = i
	end
	return flexes
end

--- Sets the weight (value) of a flex.
-- @param flexid The id of the flex
-- @param weight The weight of the flex
function ents_methods:SetFlexWeight(flexid, weight)
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end

    SF.CheckType(flexid, "number")
    SF.CheckType(weight, "number")
	flexid = math.floor(flexid)

	if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end
	if flexid < 0 or flexid >= ent:GetFlexNum() then
		SF.throw("Invalid flex: "..flexid, 2)
	end

	ent:SetFlexWeight(flexid, weight)
end

--- Sets the scale of all flexes of an entity
function ents_methods:SetFlexScale(scale)
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	if not (ent and ent:IsValid()) then SF.throw("The entity is invalid", 2) end

    SF.CheckType(scale, "number")

    if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end

	ent:SetFlexScale(scale)
end

--- Gets the model of an entity
-- @shared
-- @return Model of the entity
function ents_methods:GetModel ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return ent:GetModel()
end

--- Gets the max health of an entity
-- @shared
-- @return Max Health of the entity
function ents_methods:GetMaxHealth()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	return ent:GetMaxHealth()
end

--- Gets the health of an entity
-- @shared
-- @return Health of the entity
function ents_methods:GetHealth()
	SF.CheckType(self, ents_metamethods)
	local ent = unwrap(self)
	return ent:Health()
end

--- Gets the entitiy's eye angles
-- @shared
-- @return Angles of the entity's eyes
function ents_methods:EyeAngles ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:EyeAngles() )
end

--- Gets the entity's eye position
-- @shared
-- @return Eye position of the entity
-- @return In case of a ragdoll, the position of the other eye
function ents_methods:EyePos ()
	SF.CheckType( self, ents_metamethods )
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.WrapObject( ent:EyePos() )
end

--- Gets an entities' material
-- @shared
-- @class function
-- @return Material
function ents_methods:GetMaterial ()
    local ent = unwrap( self )
    if not isValid( ent ) then return nil, "invalid entity" end
    return ent:GetMaterial() or ""
end

--- Sets an entities' material
-- @shared
-- @class function
-- @param material, string, New material name.
-- @return The Entity being modified.
function ents_methods:SetMaterial ( material )
    SF.CheckType( material, "string" )
    if materialBlacklist[ material ] then SF.throw( "This material has been blacklisted", 2 ) end
    if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end

    local ent = unwrap( self )
    if not isValid( ent ) then return nil, "invalid entity" end
    ent:SetMaterial( material )
    return wrap( ent )
end

function ents_methods:GetMaterials ()
	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return SF.Sanitize( ent:GetMaterials() )
end

function ents_methods:GetSubMaterial ( ind )
	SF.CheckType( ind, "number" )

	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end
	return ent:GetSubMaterial( ind - 1 ) or ""
end

function ents_methods:SetSubMaterial ( ind, mat )
	-- If both are nil, it acts as a reset
	if ind ~= nil and mat ~= nil then
		SF.CheckType( ind, "number" )
		SF.CheckType( mat, "string" )
	end

	local ent = unwrap( self )
	if not isValid( ent ) then return nil, "invalid entity" end

	if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end
	if materialBlacklist[ mat ] then SF.throw( "Material: " .. ( mat or "" ) .. " is blacklisted!", 2 ) end

	ent:SetSubMaterial( ind and ind - 1 or nil, mat )
end

--- Sets an entities' bodygroup
-- @shared
-- @class function
-- @param bodygroup Number, The ID of the bodygroup you're setting.
-- @param value Number, The value you're setting the bodygroup to.
-- @return The Entity being modified.
function ents_methods:SetBodyGroup ( bodygroup, value )
    SF.CheckType( bodygroup, "number" )
    SF.CheckType( value, "number" )
    if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end

    local ent = unwrap( self )
    if not isValid( ent ) then return nil, "invalid entity" end

    ent:SetBodyGroup( bodygroup, value )

    return wrap( ent )
end

--- Sets the skin of the entity
-- @shared
-- @class function
-- @param skinIndex Number, Index of the skin to use.
-- @return The Entity being modified.
function ents_methods:SetSkin ( skinIndex )
    SF.CheckType( skinIndex, "number" )

    local ent = unwrap( self )
    if not isValid( ent ) then return nil, "invalid entity" end
    if not SF.Permissions.check( SF.instance.player, ent, "entities.setRenderProperty" ) then return end

    ent:SetSkin( skinIndex )
    return wrap( ent )
end

--- Gets the entities up vector
function ents_methods:GetUp ()
	return SF.WrapObject( unwrap( self ):GetUp() )
end

--- Gets the entities right vector
function ents_methods:GetRight ()
	return SF.WrapObject( unwrap( self ):GetRight() )
end

--- Gets the entities forward vector
function ents_methods:GetForward ()
	return SF.WrapObject( unwrap( self ):GetForward() )
end
