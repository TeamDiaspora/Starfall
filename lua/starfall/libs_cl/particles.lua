---------------------------------------------------
-- Starfall library for CLuaEmitter
--------------------------------------------------
SF.CLuaEmitter = {}
SF.CLuaParticle = {}

--- CLuaEmitter type
--@client
local cle_methods, cle_metamethods = SF.Typedef("CLuaEmitter",FindMetaTable("CLuaEmitter"))
local clewrap, cleunwrap = SF.CreateWrapper( cle_metamethods, true, false, FindMetaTable("CLuaEmitter") )
SF.CLuaEmitter.Wrap = clewrap
SF.CLuaEmitter.Unwrap = cleunwrap
SF.CLuaEmitter.Methods = cle_methods
SF.CLuaEmitter.Metamethods = cle_metamethods

--- CLuaParticle type
--@client
local clp_methods, clp_metamethods = SF.Typedef("CLuaParticle",FindMetaTable("CLuaParticle"))
local clpwrap, clpunwrap = SF.CreateWrapper(clp_metamethods,true,false,FindMetaTable("CLuaParticle"))
SF.CLuaParticle.Wrap = clpwrap
SF.CLuaParticle.Unwrap = clpunwrap
SF.CLuaParticle.Methods = cle_methods
SF.CLuaParticle.Metamethods = cle_metamethods

local vwrap, vunwrap = SF.Vectors.Wrap , SF.Vectors.Unwrap
local awrap, aunwrap = SF.Angles.Wrap  , SF.Angles.Unwrap
---------------------------------------------------------------------------
----------------------------Emitter Functions------------------------------
---------------------------------------------------------------------------

--- Creates a new CLuaEmitter
-- @name SF.DefaultEnvironment.ParticleEmitter
-- @class function
-- @param position The start position of the emitter, This is only used to determine particle drawing order for translucent particles.
-- @param use3D Whenever to render the particles in 2D or 3D mode.
-- @return CLuaEmitter The new particle emitter.
function SF.DefaultEnvironment.ParticleEmitter(position,use3D)
    return clewrap(ParticleEmitter(vunwrap(position),use3D))
end
--- Creates a new <a href="https://wiki.garrysmod.com/page/Category:CLuaParticle">CLuaParticle</a> with the given material and position.
-- @param material The material string to use.
-- @param position The position to spawn the particle on.
-- @return CLuaParticle The created particle.
function cle_methods:Add(material, position)
    return clpwrap( cleunwrap(self):Add(material, vunwrap(position)) )
end

--- Manually renders all particles the emitter has created.
function cle_methods:Draw()
    cleunwrap(self):Draw()
end

--- Removes the emitter and all its particles.
function cle_methods:Finish()
    cleunwrap(self):Finish()
end

--- Returns the ammount of active particles of this emitter.
-- @return number The amount of active particles of this emitter.
function cle_methods:GetNumActiveParticles()
    cleunwrap(self):GetNumActiveParticles()
end

--- Returns the position of this emitter. This is set when creating the emitter with ParticleEmitter.
-- @return Vector Position of this particle emitter.
function cle_methods:GetPos()
    return vwrap(cleunwrap(self):GetPos())
end

--- Returns whether this emitter is 3D or not. This is set when creating the emitter with ParticleEmitter. 
-- @return boolean Weather this emitter is 3D or not.
function cle_methods:Is3D()
    return cleunwrap(self):Is3D()
end

--- Sets the bounding box for this emitter.
-- Usually the bounding box is automatically determined by the particles, but this function overrides it. 
-- @param mins Vector - The minimum position of the box.
-- @param maxs Vector - The maximum position of the box.
function cle_methods:SetBBox(mins,maxs)
    cleunwrap(self):SetBBox(vunwrap(mins),vunwrap(maxs))
end

--- This function sets the the distance between the render camera and the emitter at which the particles should start fading and at which distance fade ends ( alpha becomes 0 ). 
-- @param distanceMin Min distance where the alpha becomes 0.
-- @param distanceMax Max distance where the alpha starts fading.
function cle_methods:SetNearClip(distanceMin, distanceMax)
    cleunwrap(self):SetNearClip(distanceMin, distanceMax)
end

--- Prevents all particles of the emitter from automatically drawing. 
-- @param noDraw Whether we should draw the particles ( false ) or not ( true )
function cle_methods:SetNoDraw(noDraw)
    cleunwrap(self):SetNoDraw(noDraw)
end

--- The function name has not much in common with its actual function, it applies a radius to every particles that affects the building of the bounding box, as it, usually is constructed by the particle that has the lowest x, y and z and the highest x, y and z, this function just adds/subtracts the radius and inflates the bounding box. 
-- @param radius Particle radius.
function cle_methods:SetParticleCullRadius(radius)
    cleunwrap(self):SetParticleCullRadius( radius ) 
end

--- Sets the position of the particle emitter. 
-- @param position Vector - New position.
function cle_methods:SetPos(position)
    cleunwrap(self):SetPos(vunwrap(position))
end

---------------------------------------------------------------------------
----------------------------Particle Functions-----------------------------
---------------------------------------------------------------------------

--- Returns the air resistance of the particle. 
-- @return number The air resistance of the particle
function clp_methods:GetAirResistance()
    return clpunwrap(self):GetAirResistance()
end

--- Returns the current orientation of the particle. 
-- @return Angle The angles of the particle
function clp_methods:GetAngles()
    return awrap(clpunwrap(self):GetAngles())
end

--- Returns the angular velocity of the particle 
-- @return Angle The angular velocity of the particle
function clp_methods:GetAngleVelocity()
    return awrap(clpunwrap(self):GetAngleVelocity())
end

--- Returns the 'bounciness' of the particle. 
-- @return number <div style="margin-left: 32px;">The 'bounciness' of the particle<p>2 means it will gain 100% of its previous velocity,<br/>1 means it will not lose velocity,<br/>0.5 means it will lose half of its velocity with each bounce.<br/></p><br/></div>
function clp_methods:GetBounce()
    return clpunwrap(self):GetBounce()
end

--- 
-- @param Returns the color of the particle. 
-- @return number Red part of the color
-- @return number Green part of the color
-- @return number Blue part of the color
function clp_methods:GetColor()
    return clpunwrap(self):GetColor()
end

--- Returns the amount of time in seconds after which the particle will be destroyed. 
-- @return number The amount of time in seconds after which the particle will be destroyed
function clp_methods:GetDieTime()
    return clpunwrap(self):GetDieTime()
end

--- Returns the alpha value that the particle will reach on its death. 
-- @return number The alpha value the particle will fade to
function clp_methods:GetEndAlpha()
    return clpunwrap(self):GetEndAlpha()
end

--- Returns the length that the particle will reach on its death. 
-- @return number The length the particle will reach
function clp_methods:GetEndLength()
    return clpunwrap(self):GetEndLength()
end
--- Returns the size that the particle will reach on its death. 
-- @return number The size the particle will reach.
function clp_methods:GetEndSize()
    return clpunwrap(self):GetEndSize()
end

--- Returns the gravity of the particle. 
-- @return Vector The gravity of the particle.
function clp_methods:GetGravity()
    return vwrap(clpunwrap(self):GetGravity())
end

--- Returns the 'life time' of the particle, how long the particle existed since its creation. 
-- This value will always be between 0 and CLuaParticle:GetDieTime.
-- It changes automatically as time goes
-- It can be manipulated using CLuaParticle:SetLifeTime.
-- If the life time of the particle will be more than CLuaParticle:GetDieTime, it will be removed. 
-- @return number How long the particle existed, in seconds.
function clp_methods:GetLifeTime()
    return clpunwrap(self):GetLifeTime()
end

--- Returns the absolute position of the particle. 
-- @return Vector The absolute position of the particle.
function clp_methods:GetPos()
    return vwrap(clpunwrap(self):GetPos())
end

--- Returns the current rotation of the particle in radians, this should only be used for 2D particles. 
-- @return number The current rotation of the particle in radians
function clp_methods:GetRoll()
    return clpunwrap(self):GetRoll()
end

--- Returns the current rotation speed of the particle in radians, this should only be used for 2D particles. 
-- @return number The current rotation speed of the particle in radians
function clp_methods:GetRollDelta()
    return clpunwrap(self):GetRollDelta()
end

--- Returns the alpha value which the particle has when it's created. 
-- @return number The alpha value which the particle has when it's created.
function clp_methods:GetStartAlpha()
    return clpunwrap(self):GetStartAlpha()
end

--- Returns the length which the particle has when it's created. 
-- @return number The length which the particle has when it's created.
function clp_methods:GetStartLength()
    return clpunwrap(self):GetStartLength()
end

--- Returns the size which the particle has when it's created. 
-- @return number The size which the particle has when it's created.
function clp_methods:GetStartSize()
    return clpunwrap(self):GetStartSize()
end

--- Returns the current velocity of the particle. 
-- @return Vector The current velocity of the particle.
function clp_methods:GetVelocity()
    return vwrap(clpunwrap(self):GetVelocity())
end

--- Sets the air resistance of the the particle. 
-- @param airResistance New air resistance.
function clp_methods:SetAirResistance(airResistance)
    clpunwrap(self):SetAirResistance(airResistance)
end

--- Sets the angles of the particle. 
-- @param ang New angle.
function clp_methods:SetAngles( ang ) 
    clpunwrap(self):SetAngles( aunwrap(ang) ) 
end

--- Sets the angular velocity of the the particle. 
-- @param angVel New angular velocity
function clp_methods:SetAngleVelocity( angVel ) 
    clpunwrap(self):SetAngleVelocity( aunwrap(angVel) ) 
end

--- Sets the 'bounciness' of the the particle. 
-- @param bounce <div style="margin-left: 32px;">New 'bounciness' of the particle<p>2 means it will gain 100% of its previous velocity,<br/>1 means it will not lose velocity,<br/>0.5 means it will lose half of its velocity with each bounce.<br/></p><br/></div>
function clp_methods:SetBounce( bounce ) 
    clpunwrap(self):SetBounce( bounce ) 
end

--- Sets the whether the particle should collide with the world or not. 
-- @param shouldCollide Whether the particle should collide with the world or not
function clp_methods:SetCollide( shouldCollide ) 
    clpunwrap(self):SetCollide( shouldCollide ) 
end

--- Sets the function that gets called whenever the particle collides with the world. See <a href="http://wiki.garrysmod.com/page/CLuaParticle/SetCollideCallback">GarryWiki</a>.
-- @param func <div class="argument"><p><span class="arg_number"></span> <span class="arg_chunk"><a href="http://wiki.garrysmod.com/page/Category:function" title="Category:function">function</a> collideFunc</span></p><div style="margin-left: 32px;">Collide callback, the arguments are:<br><br><a href="http://wiki.garrysmod.com/page/Category:CLuaParticle" title="Category:CLuaParticle">CLuaParticle</a> particle - The particle itself<br><a href="http://wiki.garrysmod.com/page/Category:Vector" title="Category:Vector">Vector</a> hitPos - Position of the collision<br><a href="http://wiki.garrysmod.com/page/Category:Vector" title="Category:Vector">Vector</a> hitNormal - Direction of the collision, perpendicular to the hit surface</div></div>
function clp_methods:SetCollideCallback(func)
    if not type(func) == "function" then return end

    local instance = SF.instance
    clpunwrap(self):SetCollideCallback(function(...)
        if instance and IsValid(instance.data.entity) and not instance.error then
            local ok, ret, traceback = instance:runFunction(func, SF.Sanitize(...))
            if not ok then
                instance:Error("Particle Set Collide Callback Function errored with: "..ret, traceback)
            end
 
            return ret
        end
    end)
end

--- Sets the color of the particle. 
-- @param r The Red Component.
-- @param g The Red Component.
-- @param b The Red Component.
function clp_methods:SetColor( r, g, b ) 
    clpunwrap(self):SetColor( r, g, b )
end

--- Sets the time where the particle will be removed. 
-- @param dieTime The new die time.
function clp_methods:SetDieTime( dieTime ) 
    clpunwrap(self):SetDieTime( dieTime ) 
end

--- Sets the alpha value of the particle that it will reach when it dies. 
-- @param endAlpha The new alpha value of the particle that it will reach when it dies.
function clp_methods:SetEndAlpha( endAlpha ) 
    clpunwrap(self):SetEndAlpha( endAlpha ) 
end

--- Sets the length of the particle that it will reach when it dies. 
-- @param endLength The new length of the particle that it will reach when it dies.
function clp_methods:SetEndLength( endLength ) 
    clpunwrap(self):SetEndLength( endLength ) 
end

--- Sets the size of the particle that it will reach when it dies. 
-- @param endSize The new size of the particle that it will reach when it dies.
function clp_methods:SetEndSize( endSize ) 
    clpunwrap(self):SetEndSize( endSize ) 
end

--- Sets the directional gravity aka. acceleration of the particle. 
-- @param gravity The directional gravity.
function clp_methods:SetGravity( gravity ) 
    clpunwrap(self):SetGravity( vunwrap(gravity) )
end

--- Sets the 'life time' of the particle, how long the particle existed since its creation. 
-- This value should always be between 0 and CLuaParticle:GetDieTime.
-- It changes automatically as time goes.
-- If the life time of the particle will be more than CLuaParticle:GetDieTime, it will be removed. 
-- @param lifeTime The new life time of the particle.
-- @return
function clp_methods:SetLifeTime( lifeTime ) 
    clpunwrap(self):SetLifeTime( lifeTime ) 
end

--- Sets whether the particle should be lighted. 
-- @param useLighting Whether the particle should be lighted.
function clp_methods:SetLighting( useLighting ) 
    clpunwrap(self):SetLighting( useLighting ) 
end

--- Sets when the particles think function should be called next, this uses the synchronized server time returned by CurTime. 
-- @param nextThink Next think time.
function clp_methods:SetNextThink( nextThink )
    clpunwrap(self):SetNextThink( nextThink )
end

--- Sets the absolute position of the particle. 
-- @param pos The new particle position.
function clp_methods:SetPos( pos ) 
    clpunwrap(self):SetPos( vunwrap(pos) ) 
end

--- Sets the roll of the particle in radians. This should only be used for 2D particles. 
-- @param roll The new rotation of the particle in radians.
function clp_methods:SetRoll( roll )
    clpunwrap(self):SetRoll( roll )
end

--- Sets the rotation speed of the particle in radians. This should only be used for 2D particles. 
-- @param rollDelta The new rotation speed of the particle in radians.
function clp_methods:SetRollDelta( rollDelta ) 
    clpunwrap(self):SetRollDelta( rollDelta ) 
end

--- Sets the initial alpha value of the particle. 
-- @param startAlpha Initial alpha.
function clp_methods:SetStartAlpha( startAlpha ) 
    clpunwrap(self):SetStartAlpha( startAlpha ) 
end

--- Sets the initial length value of the particle. 
-- @param startLength Initial length.
function clp_methods:SetStartLength( startLength ) 
    clpunwrap(self):SetStartLength( startLength ) 
end

--- Sets the initial size value of the particle. 
-- @param startSize Initial size.
function clp_methods:SetStartSize( startSize ) 
    clpunwrap(self):SetStartSize( startSize ) 
end

--- Sets the think function of the particle. See <a href="http://wiki.garrysmod.com/page/CLuaParticle/SetThinkFunction">GarryWiki</a>.
--@param func <span class="arg_chunk"><a href="http://wiki.garrysmod.com/page/Category:function" title="Category:function">function</a> thinkFunc</span></p><div style="margin-left: 32px;">Think function. It has only one argument:<br><a href="http://wiki.garrysmod.com/page/Category:CLuaParticle" title="Category:CLuaParticle">CLuaParticle</a> particle - The particle the think hook is set on</div>
function clp_methods:SetThinkFunction(func)
    if not type(func) == "function" then return end

    local instance = SF.instance
    clpunwrap(self):SetThinkFunction(function(...)
        if instance and IsValid(instance.data.entity) and not instance.error then
            local ok, ret, traceback = instance:runFunction(func, SF.Sanitize(...))
            if not ok then
                instance:Error("Particle Think Function errored with: "..ret, traceback)
            end
 
            return ret
        end
    end)
end

--- Sets the velocity of the particle. 
-- @param vel The new velocity of the particle.
function clp_methods:SetVelocity( vel ) 
    clpunwrap(self):SetVelocity( vunwrap(vel) ) 
end


--- Scales the velocity based on the particle speed. 
-- @param doScale Use velocity scaling.
function clp_methods:SetVelocityScale(doScale)
    clpunwrap(self):SetVelocityScale(doScale)
end
