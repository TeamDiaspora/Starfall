/*Kinect Bone Position Functions*/

AddCSLuaFile()

__e2setcost(30)


e2function table entity:kBones()
		

		local DEFAULT = {n={},ntypes={},s={},stypes={},size=0,istable=true}
		local tmp = table.Copy(DEFAULT)
		local size = 20
		
		if not IsValid(this) then return tmp end
		if not this:IsPlayer() then return tmp end

		tmp.s["HIP"] = this:MotionSensorPos(SENSORBONE.HIP)
		tmp.stypes["HIP"] = "v"

		tmp.s["SPNE"] = this:MotionSensorPos(SENSORBONE.SPINE)
		tmp.stypes["SPNE"] = "v"

		tmp.s["SHLD"] = this:MotionSensorPos(SENSORBONE.SHOULDER)
		tmp.stypes["SHLD"] = "v"
		
		tmp.s["HEAD"] = this:MotionSensorPos(SENSORBONE.HEAD)
		tmp.stypes["HEAD"] = "v"
		
		tmp.s["LSHD"] = this:MotionSensorPos(SENSORBONE.SHOULDER_LEFT)
		tmp.stypes["LSHD"] = "v"
		
		tmp.s["LELB"] = this:MotionSensorPos(SENSORBONE.ELBOW_LEFT)
		tmp.stypes["LELB"] = "v"
		
		tmp.s["LWRST"] = this:MotionSensorPos(SENSORBONE.WRIST_LEFT)
		tmp.stypes["LWRST"] = "v"
		
		tmp.s["LHAND"] = this:MotionSensorPos(SENSORBONE.HAND_LEFT)
		tmp.stypes["LHAND"] = "v"
		
		tmp.s["RSHD"] = this:MotionSensorPos(SENSORBONE.SHOULDER_RIGHT)
		tmp.stypes["RSHD"] = "v"
		
		tmp.s["RELB"] = this:MotionSensorPos(SENSORBONE.ELBOW_RIGHT)
		tmp.stypes["RELB"] = "v"
		
		tmp.s["RWRST"] = this:MotionSensorPos(SENSORBONE.WRIST_RIGHT)
		tmp.stypes["RWRST"] = "v"
		
		tmp.s["RHAND"] = this:MotionSensorPos(SENSORBONE.HAND_RIGHT)
		tmp.stypes["RHAND"] = "v"
		
		tmp.s["LHIP"] = this:MotionSensorPos(SENSORBONE.HIP_LEFT)
		tmp.stypes["LHIP"] = "v"
		
		tmp.s["LKNE"] = this:MotionSensorPos(SENSORBONE.KNEE_LEFT)
		tmp.stypes["LKNE"] = "v"
		
		tmp.s["LANK"] = this:MotionSensorPos(SENSORBONE.ANKLE_LEFT)
		tmp.stypes["LANK"] = "v"
		
		tmp.s["LFOOT"] = this:MotionSensorPos(SENSORBONE.FOOT_LEFT)
		tmp.stypes["LFOOT"] = "v"
		
		tmp.s["RHIP"] = this:MotionSensorPos(SENSORBONE.HIP_RIGHT)
		tmp.stypes["RHIP"] = "v"
		
		tmp.s["RKNE"] = this:MotionSensorPos(SENSORBONE.KNEE_RIGHT)
		tmp.stypes["RKNE"] = "v"
		
		tmp.s["RANK"] = this:MotionSensorPos(SENSORBONE.ANKLE_RIGHT)
		tmp.stypes["RANK"] = "v"
		
		tmp.s["RFOOT"] = this:MotionSensorPos(SENSORBONE.FOOT_RIGHT)
		tmp.stypes["RFOOT"] = "v"
		
		tmp.size = size
		return tmp
end