//////////////////////////////////////////////////////
//	Player Key Extension for E2
//	By Dubby
//////////////////////////////////////////////////////
//	return values:
//	0 = Inactive
//	1 = Was Pressed
//	2 = Is Depressed
//	3 = Was Released
//	4 = Was Active Last Frame
//////////////////////////////////////////////////////

function GetKeyStatus(ply, key)
	if ply:KeyPressed(key) then
		return 1
	elseif ply:KeyDown(key) then
		return 2
	elseif ply:KeyReleased(key) then
		return 3
	elseif ply:KeyDownLast(key) then
		return 4
	end
	return 0
end

e2function number entity:keyAttack1()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_ATTACK)
	end
	return 0
end

e2function number entity:keyAttack2()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_ATTACK2)
	end
	return 0
end

e2function number entity:keyUse()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_USE)
	end
	return 0
end

e2function number entity:keyJump()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_JUMP)
	end
	return 0
end

e2function number entity:keyDuck()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_DUCK)
	end
	return 0
end

e2function number entity:keyForward()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_FORWARD)
	end
	return 0
end

e2function number entity:keyBack()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_BACK)
	end
	return 0
end

e2function number entity:keyCancel()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_CANCEL)
	end
	return 0
end

e2function number entity:keyTurnLeft()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_LEFT)
	end
	return 0
end

e2function number entity:keyTurnRight()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_RIGHT)
	end
	return 0
end

e2function number entity:keyLeft()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_MOVELEFT)
	end
	return 0
end

e2function number entity:keyRight()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_MOVERIGHT)
	end
	return 0
end

e2function number entity:keyRun()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_RUN)
	end
	return 0
end

e2function number entity:keyReload()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_RELOAD)
	end
	return 0
end

e2function number entity:keyAlt1()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_ALT1)
	end
	return 0
end

e2function number entity:keyAlt2()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_ALT2)
	end
	return 0
end

e2function number entity:keyScore()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_SCORE)
	end
	return 0
end

e2function number entity:keySpeed()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_SPEED)
	end
	return 0
end

e2function number entity:keyWalk()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_WALK)
	end
	return 0
end

e2function number entity:keyZoom()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_ZOOM)
	end
	return 0
end

e2function number entity:keyWeapon1()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_WEAPON1)
	end
	return 0
end

e2function number entity:keyWeapon2()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_WEAPON2)
	end
	return 0
end

e2function number entity:keyBullrush()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_BULLRUSH)
	end
	return 0
end

e2function number entity:keyGrenade1()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_GRENADE1)
	end
	return 0
end

e2function number entity:keyGrenade2()
	if not IsValid(this) then return 0 end
	if this:IsPlayer() then
		return GetKeyStatus(this, IN_GRENADE2)
	end
	return 0
end