-------------------------------------------------------------------------------
-- Player functions.
-------------------------------------------------------------------------------

SF.Players = {}
--- Player type
local player_methods, player_metamethods = SF.Typedef( "Player", SF.Entities.Metatable )

local vwrap = SF.WrapObject

SF.Players.Methods = player_methods
SF.Players.Metatable = player_metamethods

--- Custom wrapper/unwrapper is necessary for player objects
-- wrapper
local dsetmeta = debug.setmetatable
local function wrap ( object )
	object = SF.Entities.Wrap( object )
	dsetmeta( object, player_metamethods )
	return object
end

local unwrap = SF.Entities.Unwrap

SF.AddObjectWrapper( debug.getregistry().Player, player_metamethods, wrap )

-- unwrapper
SF.AddObjectUnwrapper( player_metamethods, unwrap )

--- Adding the wrap / unwrap functions to the SF.Vehicles table
SF.Players.Wrap = wrap
SF.Players.Unwrap = unwrap

--- To string
-- @shared
function player_metamethods:__tostring ()
	local ent = SF.Entities.Unwrap( self )
	if not ent then return "(null entity)"
	else return tostring( ent ) end
end


-- ------------------------------------------------------------------------- --
--- Returns whether the player is alive
-- @shared
-- @return True if player alive
function player_methods:Alive ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Alive()
end

--- Returns the players armor
-- @shared
-- @return Armor
function player_methods:Armor ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Armor()
end

--- Returns whether the player is crouching
-- @shared
-- @return True if player crouching
function player_methods:Crouching ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Crouching()
end

--- Returns the amount of deaths of the player
-- @shared
-- @return Amount of deaths
function player_methods:Deaths ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Deaths()
end

--- Returns whether the player's flashlight is on
-- @shared
-- @return True if player has flashlight on
function player_methods:FlashlightIsOn ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:FlashlightIsOn()
end

--- Returns the amount of kills of the player
-- @shared
-- @return Amount of kills
function player_methods:Frags ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Frags()
end

--- Returns the name of the player's active weapon
-- @shared
-- @return Name of weapon
function player_methods:GetActiveWeapon ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:GetActiveWeapon():IsValid() and ent:GetActiveWeapon():GetClass()
end

--- Returns the player's aim vector
-- @shared
-- @return Aim vector
function player_methods:GetAimVector ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and vwrap( ent:GetAimVector() )
end

--- Returns the player's field of view
-- @shared
-- @return Field of view
function player_methods:GetFOV ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:GetFOV()
end

--- Returns the player's jump power
-- @shared
-- @return Jump power
function player_methods:GetJumpPower ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:GetJumpPower()
end

--- Returns the player's maximum speed
-- @shared
-- @return Maximum speed
function player_methods:GetMaxSpeed ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:GetMaxSpeed()
end

--- Returns the player's name
-- @shared
-- @return Name
function player_methods:GetName ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:GetName()
end

--- Returns the player's running speed
-- @shared
-- @return Running speed
function player_methods:GetRunSpeed ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:GetRunSpeed()
end

--- Returns the player's shoot position
-- @shared
-- @return Shoot position
function player_methods:GetShootPos ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and vwrap( ent:GetShootPos() )
end

--- Returns whether the player is in a vehicle
-- @shared
-- @return True if player in vehicle
function player_methods:InVehicle ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:InVehicle()
end

--- Returns the vehicle the player is currently in, nil otherwise.
-- @shared
function player_methods:GetVehicle ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return SF.Entities.Wrap(ent:GetVehicle())
end

--- Returns whether the player is an admin
-- @shared
-- @return True if player is admin
function player_methods:IsAdmin ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsAdmin()
end

--- Returns whether the player is a bot
-- @shared
-- @return True if player is a bot
function player_methods:IsBot ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsBot()
end

--- Returns whether the player is connected
-- @shared
-- @return True if player is connected
function player_methods:IsConnected ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsConnected()
end

--- Returns whether the player is frozen
-- @shared
-- @return True if player is frozen
function player_methods:IsFrozen ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsFrozen()
end

--- Returns whether the player is an NPC
-- @shared
-- @return True if player is an NPC
function player_methods:IsNPC ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsNPC()
end

--- Returns whether the player is a player
-- @shared
-- @return True if player is player
function player_methods:IsPlayer ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsPlayer()
end

--- Returns whether the player is a super admin
-- @shared
-- @return True if player is super admin
function player_methods:IsSuperAdmin ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsSuperAdmin()
end

--- Returns whether the player belongs to a usergroup
-- @shared
-- @param group Group to check against
-- @return True if player belongs to group
function player_methods:IsUserGroup ( group )
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:IsUserGroup( group )
end

--- Returns the player's current ping
-- @shared
-- @return ping
function player_methods:Ping ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Ping()
end

--- Returns the player's steam ID
-- @shared
-- @return steam ID
function player_methods:SteamID ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:SteamID()
end

--- Returns the player's community ID
-- @shared
-- @return community ID
function player_methods:SteamID64 ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:SteamID64()
end

--- Returns the player's current team
-- @shared
-- @return team
function player_methods:Team ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:Team()
end

--- Returns the name of the player's current team
-- @shared
-- @return team name
function player_methods:TeamName ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and team.GetName(ent:Team())
end

--- Returns the player's unique ID
-- @shared
-- @return unique ID
function player_methods:UniqueID ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:UniqueID()
end

--- Returns the player's user ID
-- @shared
-- @return user ID
function player_methods:UserID ()
	SF.CheckType( self, player_metamethods )
	local ent = SF.Entities.Unwrap( self )
	return ent and ent:UserID()
end

--- Returns a table with information of what the player is looking at
-- @shared
-- @return table trace data
function player_methods:GetEyeTrace ()
	if not SF.Permissions.check( SF.instance.player, SF.UnwrapObject( self ), "trace" ) then SF.throw( "Insufficient permissions", 2 ) end
	return SF.Sanitize( SF.UnwrapObject( self ):GetEyeTrace() )
end

if SERVER then
	util.AddNetworkString("starfall_PlyPrintSimpleColor")
	--- Sends a colored string to the defined player's chatbox.
	function player_methods:PlyPrintSimpleColor(text, color)
		SF.CheckType( self, player_metamethods )
		local ply = SF.Entities.Unwrap( self )
		--if not IsValid( ply ) then return false end
		--if not ply:IsPlayer() then return false end

		net.Start("starfall_PlyPrintSimpleColor")
			net.WriteString(text)
			net.WriteColor(SF.Color.Unwrap(color))
		net.Send(ply)
		return true
	end
end

if CLIENT then
	--- Returns the relationship of the player to the local client
	-- @return One of: "friend", "blocked", "none", "requested"
	function player_methods:GetFriendStatus ()
		SF.CheckType( self, player_metamethods )
		local ent = SF.Entities.Unwrap( self )
		return ent and ent:GetFriendStatus()
	end
	
	--- Returns whether the local player has muted the player
	-- @return True if the player was muted
	function player_methods:IsMuted ()
		SF.CheckType( self, player_metamethods )
		local ent = SF.Entities.Unwrap( self )
		return ent and ent:IsMuted()
	end
	
	--Clientside stuff for PlyPrintSimpleColor to work correctly.
	net.Receive("starfall_PlyPrintSimpleColor", function( len, ply )
		local textLine = net.ReadString()
		local color = net.ReadColor()
	
		chat.AddText2("ALL",color, textLine)
	end)
	
	--- Adds text to your chat box (which only you can read). 
    -- @param ... <div style="margin-left: 32px;">The arguments. Arguments can be:<ul><li> <a href="http://wiki.garrysmod.com/page/Category:table" title="Category:table">table</a> - <a href="http://wiki.garrysmod.com/page/Structures/Color" title="Structures/Color">Color structure</a>. Will set the color for all following strings until the next Color argument.</li><li> <a href="http://wiki.garrysmod.com/page/Category:string" title="Category:string">string</a> - Text to be added to the chat box.</li><li> <a href="http://wiki.garrysmod.com/page/Category:Player" title="Category:Player">Player</a> - Adds the name of the player in the player's team color to the chat box.</li><li> <a href="http://wiki.garrysmod.com/page/Category:any" title="Category:any">any</a> - Any other type, such as <a href="http://wiki.garrysmod.com/page/Category:Entity" title="Category:Entity">Entity</a> will be converted to string and added as text.</div></li></ul></div>
    function player_methods:ChatAddText(...)
        if SF.instance.player == LocalPlayer() then
            local ret = {}
            for k,v in pairs({...}) do
                local temp = v
                if type(v) == "table" then
                    if getmetatable(v) == "Entity" then 
                        temp = SF.Entities.Unwrap(v)
                    elseif getmetatable(v) == "Player" then
                        temp = SF.Players.Unwrap(v)
                    end
                end
                table.insert(ret,temp)
            end
            chat.AddText(unpack(ret))
        end
    end
end