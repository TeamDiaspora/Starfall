---------------------------------------------------------------------------------
-------- shared library for IMaterial. -----------------------------------------
-------------------------------------------------------------------------------
SF.IMaterial = {}

---IMaterial
--@shared
local methods, metamethods = SF.Typedef("IMaterial",FindMetaTable("IMaterial"))
local wrap, unwrap = SF.CreateWrapper( metamethods, true, false, FindMetaTable("IMaterial"))

SF.IMaterial.Wrap = wrap
SF.IMaterial.Unwrap = unwrap
SF.IMaterial.Methods = methods
SF.IMaterial.Metamethods = metamethods

--- Either returns the material with the given name, or loads the material interpreting the first argument as the path. 
-- @name SF.DefaultEnvironment.Material
-- @param materialName String The material name/path.
-- @param pngParameters A string containing keywords which will be used to add material parameters. 
-- list of params:
-- vertexlitgeneric
-- unlitgeneric
-- nocull
-- alphatest
-- mips
-- noclamp
-- smooth
-- @return IMaterial Generated material
-- @return number How long it took for the function to run
function SF.DefaultEnvironment.Material(materialName,pngParameters ) 
    return wrap(Material(materialName,pngParameters))
end
