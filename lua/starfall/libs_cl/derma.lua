SF.DPanels = {}
local dp_methods, dp_metamethods = SF.Typedef( "DPanel")
local wrap, unwrap = SF.CreateWrapper( dp_metamethods, true, false, FindMetaTable("Panel") )
local PanelTypes = {"DAdjustableModelPanel", "DAlphaBar", "DBinder", "DBubbleContainer", "DButton", "DCategoryList", "DCheckBox", "DCheckBoxLabel", "DCollapsibleCategory", "DColorButton", "DColorCombo", "DColorCube", "DColorMixer", "DColorPalette", "DColumnSheet", "DComboBox", "DDrawer", "DEntityProperties", "DExpandButton", "DFileBrowser", "DForm", "DFrame", "DGrid", "DHorizontalDivider", "DHorizontalScroller", "DHTML", "DHTMLControls", "DIconBrowser", "DIconLayout", "DImage", "DImageButton", "DKillIcon", "DLabel", "DLabelEditable", "DLabelURL", "DListBox", "DListLayout", "DListView", "DMenu", "DMenuBar", "DModelPanel", "DModelSelect", "DModelSelectMulti", "DNotify", "DNumberScratch", "DNumberWang", "DNumSlider", "DPanel", "DPanelList", "DPanelOverlay", "DPanelSelect", "DProgress", "DProperties", "DPropertySheet", "DRGBPicker", "DScrollPanel", "DSizeToContents", "DSlider", "DSprite", "DTextEntry", "DTileLayout", "DTree", "DVerticalDivider", "DVScrollBar", "ImageCheckBox", "Material", "SlideBar", "SpawnIcon"}

SF.DPanels.Wrap = wrap
SF.DPanels.Unwrap = unwrap
SF.DPanels.Methods = dp_methods
SF.DPanels.Metamethods = dp_metamethods

local vgui_library, _ = SF.Libraries.Register( "vgui" )

local function CanRenderOnClient()
    if not SF.instance then
        error("starfall instance not available, wtf?")
    end

    if (LocalPlayer():InVehicle() and FindPlayerByName(NADMOD.PropOwners[LocalPlayer():GetVehicle():EntIndex()]) or LocalPlayer()) ~= SF.instance.player then return false end
    return true
end

function vgui_library.Create(Type, Parent)
    if not table.HasValue(PanelTypes, Type) then
        error("tried to create invalid vgui element")
    end

    if CanRenderOnClient() then
        return wrap(vgui.Create(Type, Parent ~= nil and unwrap(Parent) or nil))
    end
end

function dp_metamethods.__newindex(tbl, key, value)
    if type(key) == "string" and type(value) == "function" then
        local obj = SF.UnwrapObject(tbl)
        local instance = SF.instance
        obj.__newindex(obj, key, function(...)
            if instance and IsValid(instance.data.entity) and not instance.error then
                local tbl = {instance:runFunction(value, SF.Sanitize(...))}
                local ok = tbl[1]
                if not ok then
                    instance:Error("Derma Panel Function "..key.." errored with: "..tbl[2], tbl[3])
                    return
                end

                return unpack(tbl, 2)
            end
        end)
    end

    rawset(tbl, key, value)
end

--- __index metamethod
function dp_metamethods.__index ( t, k )
	if type(k) == "string" then
        local obj = unwrap(t)
        if type(obj[k]) == "Panel" then
		    return wrap(obj[k])
        end
	end
	return dp_metamethods.__methods[k]
end

--- tostring metamethod
-- @return string representing the vector.
function dp_metamethods:__tostring ()
	return unwrap( self ):__tostring()
end

-- Automatically wrap ALL of the functions that we can possibly call on this object type :D
local FunctionNames = {}
for i,k in pairs(PanelTypes) do
    local PanelMethods = vgui.GetControlTable(k)
    for name,func in pairs(PanelMethods) do
        if isfunction(func) then
            FunctionNames[name] = true
        end
    end
end

for FunctionName in pairs(FunctionNames) do
    dp_methods[FunctionName] = function(self, ...)
        -- Make sure any functions get wrapped so they know which sf instance to use
        local Args = {...}
        local ToFunc = {}
        local instance = SF.instance

        for i, k in pairs(Args) do
            if isfunction(k) then
                ToFunc[i] = function(...)
                    if instance and IsValid(instance.data.entity) and not instance.error then
                        local tbl = {instance:runFunction(k, SF.Sanitize(...))}
                        local ok = tbl[1]

                        if not ok then
                            instance:Error("Derma Panel callback for " .. FunctionName .. " errored with: " .. tbl[2], tbl[3])
                            return
                        end

                        return unpack(tbl, 2)
                    end
                end
            end
        end

        -- Apply the modifications back to the arguments
        for i, k in pairs(ToFunc) do
            Args[i] = k
        end

        if not isfunction(unwrap(self)[FunctionName]) then
            error(string.format("%s is not a function", FunctionName))
        end

        return SF.Sanitize(unwrap(self)[FunctionName](unwrap(self), unpack(Args)))
    end
end

-- This is a giant pile of methods :D
function dp_methods:CursorPos(...)
	return unwrap(self):CursorPos(...)
end

function dp_methods:SlideDown(...)
	return unwrap(self):SlideDown(...)
end

function dp_methods:GetPos(...)
	return unwrap(self):GetPos(...)
end

function dp_methods:IsVisible(...)
	return unwrap(self):IsVisible(...)
end

function dp_methods:LoadGWENFile(...)
	return unwrap(self):LoadGWENFile(...)
end

function dp_methods:DragMousePress(...)
	return unwrap(self):DragMousePress(...)
end

function dp_methods:Add(...)
	return unwrap(self):Add(...)
end

function dp_methods:IsLoading(...)
	return unwrap(self):IsLoading(...)
end

function dp_methods:ToggleSelection(...)
	return unwrap(self):ToggleSelection(...)
end

function dp_methods:ParentToHUD(...)
	return unwrap(self):ParentToHUD(...)
end

function dp_methods:GetDock(...)
	return unwrap(self):GetDock(...)
end

function dp_methods:SetPaintBorderEnabled(...)
	return unwrap(self):SetPaintBorderEnabled(...)
end

function dp_methods:SetTextInset(...)
	return unwrap(self):SetTextInset(...)
end

function dp_methods:KillFocus(...)
	return unwrap(self):KillFocus(...)
end

function dp_methods:SetWidth(...)
	return unwrap(self):SetWidth(...)
end

function dp_methods:GotoTextEnd(...)
	return unwrap(self):GotoTextEnd(...)
end

function dp_methods:GetSize(...)
	return unwrap(self):GetSize(...)
end

function dp_methods:SizeToChildren(...)
	return unwrap(self):SizeToChildren(...)
end

function dp_methods:SetMinimumSize(...)
	return unwrap(self):SetMinimumSize(...)
end

function dp_methods:__index(...)
	return unwrap(self):__index(...)
end

function dp_methods:SetTooltipPanel(...)
	return unwrap(self):SetTooltipPanel(...)
end

function dp_methods:HasChildren(...)
	return unwrap(self):HasChildren(...)
end

function dp_methods:DrawTextEntryText(...)
	return unwrap(self):DrawTextEntryText(...)
end

function dp_methods:Hide(...)
	return unwrap(self):Hide(...)
end

function dp_methods:Stop(...)
	return unwrap(self):Stop(...)
end

function dp_methods:Refresh(...)
	return unwrap(self):Refresh(...)
end

function dp_methods:Remove(...)
	return unwrap(self):Remove(...)
end

function dp_methods:IsMarkedForDeletion(...)
	return unwrap(self):IsMarkedForDeletion(...)
end

function dp_methods:CutSelected(...)
	return unwrap(self):CutSelected(...)
end

function dp_methods:GetCookie(...)
	return unwrap(self):GetCookie(...)
end

function dp_methods:InvalidateParent(...)
	return unwrap(self):InvalidateParent(...)
end

function dp_methods:GetBounds(...)
	return unwrap(self):GetBounds(...)
end

function dp_methods:SetSize(...)
	return unwrap(self):SetSize(...)
end

function dp_methods:MoveAbove(...)
	return unwrap(self):MoveAbove(...)
end

function dp_methods:GetSkin(...)
	return unwrap(self):GetSkin(...)
end

function dp_methods:GetZPos(...)
	return unwrap(self):GetZPos(...)
end

function dp_methods:SaveUndoState(...)
	return unwrap(self):SaveUndoState(...)
end

function dp_methods:LocalCursorPos(...)
	return unwrap(self):LocalCursorPos(...)
end

function dp_methods:SetSteamID(...)
	return unwrap(self):SetSteamID(...)
end

function dp_methods:InsertClickableTextEnd(...)
	return unwrap(self):InsertClickableTextEnd(...)
end

function dp_methods:CopyPos(...)
	return unwrap(self):CopyPos(...)
end

function dp_methods:SetAchievement(...)
	return unwrap(self):SetAchievement(...)
end

function dp_methods:SetSelectionCanvas(...)
	return unwrap(self):SetSelectionCanvas(...)
end

function dp_methods:CopyBounds(...)
	return unwrap(self):CopyBounds(...)
end

function dp_methods:Distance(...)
	return unwrap(self):Distance(...)
end

function dp_methods:IsMouseInputEnabled(...)
	return unwrap(self):IsMouseInputEnabled(...)
end

function dp_methods:Center(...)
	return unwrap(self):Center(...)
end

function dp_methods:SetZPos(...)
	return unwrap(self):SetZPos(...)
end

function dp_methods:SetExpensiveShadow(...)
	return unwrap(self):SetExpensiveShadow(...)
end

function dp_methods:SetAutoDelete(...)
	return unwrap(self):SetAutoDelete(...)
end

function dp_methods:CenterHorizontal(...)
	return unwrap(self):CenterHorizontal(...)
end

function dp_methods:SetActionFunction(...)
	return unwrap(self):SetActionFunction(...)
end

function dp_methods:Undo(...)
	return unwrap(self):Undo(...)
end

function dp_methods:IsOurChild(...)
	return unwrap(self):IsOurChild(...)
end

function dp_methods:__tostring(...)
	return unwrap(self):__tostring(...)
end

function dp_methods:LerpPositions(...)
	return unwrap(self):LerpPositions(...)
end

function dp_methods:RebuildSpawnIconEx(...)
	return unwrap(self):RebuildSpawnIconEx(...)
end

function dp_methods:SetToolTip(...)
	return unwrap(self):SetToolTip(...)
end

function dp_methods:SetToolTipPanel(...)
	return unwrap(self):SetToolTipPanel(...)
end

function dp_methods:IsChildHovered(...)
	return unwrap(self):IsChildHovered(...)
end

function dp_methods:SetDrawLanguageIDAtLeft(...)
	return unwrap(self):SetDrawLanguageIDAtLeft(...)
end

function dp_methods:UnselectAll(...)
	return unwrap(self):UnselectAll(...)
end

function dp_methods:DragClick(...)
	return unwrap(self):DragClick(...)
end

function dp_methods:SetAlpha(...)
	return unwrap(self):SetAlpha(...)
end

function dp_methods:GetCookieName(...)
	return unwrap(self):GetCookieName(...)
end

function dp_methods:GetAlpha(...)
	return unwrap(self):GetAlpha(...)
end

function dp_methods:SetDrawLanguageID(...)
	return unwrap(self):SetDrawLanguageID(...)
end

function dp_methods:StretchRightTo(...)
	return unwrap(self):StretchRightTo(...)
end

function dp_methods:DockPadding(...)
	return unwrap(self):DockPadding(...)
end

function dp_methods:SetDrawOnTop(...)
	return unwrap(self):SetDrawOnTop(...)
end

function dp_methods:GWEN_SetMin(...)
	return unwrap(self):GWEN_SetMin(...)
end

function dp_methods:GetFont(...)
	return unwrap(self):GetFont(...)
end

function dp_methods:SetVerticalScrollbarEnabled(...)
	return unwrap(self):SetVerticalScrollbarEnabled(...)
end

function dp_methods:DoModal(...)
	return unwrap(self):DoModal(...)
end

function dp_methods:PostMessage(...)
	return unwrap(self):PostMessage(...)
end

function dp_methods:SetCursor(...)
	return unwrap(self):SetCursor(...)
end

function dp_methods:SetMouseInputEnabled(...)
	return unwrap(self):SetMouseInputEnabled(...)
end

function dp_methods:NewObject(...)
	return unwrap(self):NewObject(...)
end

function dp_methods:SizeTo(...)
	return unwrap(self):SizeTo(...)
end

function dp_methods:GetParent(...)
	return unwrap(self):GetParent(...)
end

function dp_methods:DragHoverClick(...)
	return unwrap(self):DragHoverClick(...)
end

function dp_methods:SetHTML(...)
	return unwrap(self):SetHTML(...)
end

function dp_methods:InsertClickableTextStart(...)
	return unwrap(self):InsertClickableTextStart(...)
end

function dp_methods:Droppable(...)
	return unwrap(self):Droppable(...)
end

function dp_methods:DragHover(...)
	return unwrap(self):DragHover(...)
end

function dp_methods:InsertColorChange(...)
	return unwrap(self):InsertColorChange(...)
end

function dp_methods:DrawOutlinedRect(...)
	return unwrap(self):DrawOutlinedRect(...)
end

function dp_methods:SetToFullHeight(...)
	return unwrap(self):SetToFullHeight(...)
end

function dp_methods:SelectAll(...)
	return unwrap(self):SelectAll(...)
end

function dp_methods:GetName(...)
	return unwrap(self):GetName(...)
end

function dp_methods:AnimTail(...)
	return unwrap(self):AnimTail(...)
end

function dp_methods:SizeToContentsX(...)
	return unwrap(self):SizeToContentsX(...)
end

function dp_methods:MoveToBack(...)
	return unwrap(self):MoveToBack(...)
end

function dp_methods:PositionLabel(...)
	return unwrap(self):PositionLabel(...)
end

function dp_methods:GWEN_SetCheckboxText(...)
	return unwrap(self):GWEN_SetCheckboxText(...)
end

function dp_methods:SetParent(...)
	return unwrap(self):SetParent(...)
end

function dp_methods:AnimationThinkInternal(...)
	return unwrap(self):AnimationThinkInternal(...)
end

function dp_methods:DisableLerp(...)
	return unwrap(self):DisableLerp(...)
end

function dp_methods:GetClosestChild(...)
	return unwrap(self):GetClosestChild(...)
end

function dp_methods:MoveRightOf(...)
	return unwrap(self):MoveRightOf(...)
end

function dp_methods:HasParent(...)
	return unwrap(self):HasParent(...)
end

function dp_methods:DragHoverEnd(...)
	return unwrap(self):DragHoverEnd(...)
end

function dp_methods:SetPaintBackgroundEnabled(...)
	return unwrap(self):SetPaintBackgroundEnabled(...)
end

function dp_methods:GetHTMLMaterial(...)
	return unwrap(self):GetHTMLMaterial(...)
end

function dp_methods:SetDragParent(...)
	return unwrap(self):SetDragParent(...)
end

function dp_methods:SetTerm(...)
	return unwrap(self):SetTerm(...)
end

function dp_methods:MouseCapture(...)
	return unwrap(self):MouseCapture(...)
end

function dp_methods:GetChildren(...)
	return unwrap(self):GetChildren(...)
end

function dp_methods:GetSelectionCanvas(...)
	return unwrap(self):GetSelectionCanvas(...)
end

function dp_methods:CenterVertical(...)
	return unwrap(self):CenterVertical(...)
end

function dp_methods:InvalidateChildren(...)
	return unwrap(self):InvalidateChildren(...)
end

function dp_methods:GetNumLines(...)
	return unwrap(self):GetNumLines(...)
end

function dp_methods:AlignLeft(...)
	return unwrap(self):AlignLeft(...)
end

function dp_methods:MakePopup(...)
	return unwrap(self):MakePopup(...)
end

function dp_methods:IsSelected(...)
	return unwrap(self):IsSelected(...)
end

function dp_methods:GetClassName(...)
	return unwrap(self):GetClassName(...)
end

function dp_methods:AlignTop(...)
	return unwrap(self):AlignTop(...)
end

function dp_methods:SetURL(...)
	return unwrap(self):SetURL(...)
end

function dp_methods:LoadControlsFromString(...)
	return unwrap(self):LoadControlsFromString(...)
end

function dp_methods:Dock(...)
	return unwrap(self):Dock(...)
end

function dp_methods:SizeToContents(...)
	return unwrap(self):SizeToContents(...)
end

function dp_methods:SetBGColor(...)
	return unwrap(self):SetBGColor(...)
end

function dp_methods:ColorTo(...)
	return unwrap(self):ColorTo(...)
end

function dp_methods:EndBoxSelection(...)
	return unwrap(self):EndBoxSelection(...)
end

function dp_methods:AddText(...)
	return unwrap(self):AddText(...)
end

function dp_methods:SetTall(...)
	return unwrap(self):SetTall(...)
end

function dp_methods:SetSpawnIcon(...)
	return unwrap(self):SetSpawnIcon(...)
end

function dp_methods:CopySelected(...)
	return unwrap(self):CopySelected(...)
end

function dp_methods:InvalidateLayout(...)
	return unwrap(self):InvalidateLayout(...)
end

function dp_methods:SetTooltip(...)
	return unwrap(self):SetTooltip(...)
end

function dp_methods:GotoTextStart(...)
	return unwrap(self):GotoTextStart(...)
end

function dp_methods:SlideUp(...)
	return unwrap(self):SlideUp(...)
end

function dp_methods:SetTabPosition(...)
	return unwrap(self):SetTabPosition(...)
end

function dp_methods:LocalToScreen(...)
	return unwrap(self):LocalToScreen(...)
end

function dp_methods:PaintAt(...)
	return unwrap(self):PaintAt(...)
end

function dp_methods:DroppedOn(...)
	return unwrap(self):DroppedOn(...)
end

function dp_methods:__newindex(...)
	return unwrap(self):__newindex(...)
end

function dp_methods:SetCaretPos(...)
	return unwrap(self):SetCaretPos(...)
end

function dp_methods:SetDropTarget(...)
	return unwrap(self):SetDropTarget(...)
end

function dp_methods:IsDragging(...)
	return unwrap(self):IsDragging(...)
end

function dp_methods:IsKeyboardInputEnabled(...)
	return unwrap(self):IsKeyboardInputEnabled(...)
end

function dp_methods:SetAnimationEnabled(...)
	return unwrap(self):SetAnimationEnabled(...)
end

function dp_methods:GoToHistoryOffset(...)
	return unwrap(self):GoToHistoryOffset(...)
end

function dp_methods:InsertFade(...)
	return unwrap(self):InsertFade(...)
end

function dp_methods:ApplyGWEN(...)
	return unwrap(self):ApplyGWEN(...)
end

function dp_methods:GetText(...)
	return unwrap(self):GetText(...)
end

function dp_methods:AppendText(...)
	return unwrap(self):AppendText(...)
end

function dp_methods:Valid(...)
	return unwrap(self):Valid(...)
end

function dp_methods:GetTextInset(...)
	return unwrap(self):GetTextInset(...)
end

function dp_methods:MoveBy(...)
	return unwrap(self):MoveBy(...)
end

function dp_methods:GetValue(...)
	return unwrap(self):GetValue(...)
end

function dp_methods:OnDrop(...)
	return unwrap(self):OnDrop(...)
end

function dp_methods:GetSelectedChildren(...)
	return unwrap(self):GetSelectedChildren(...)
end

function dp_methods:OpenURL(...)
	return unwrap(self):OpenURL(...)
end

function dp_methods:CopyWidth(...)
	return unwrap(self):CopyWidth(...)
end

function dp_methods:RequestFocus(...)
	return unwrap(self):RequestFocus(...)
end

function dp_methods:ThisClass(...)
	return unwrap(self):ThisClass(...)
end

function dp_methods:GoBack(...)
	return unwrap(self):GoBack(...)
end

function dp_methods:LoadTGAImage(...)
	return unwrap(self):LoadTGAImage(...)
end

function dp_methods:CopyBase(...)
	return unwrap(self):CopyBase(...)
end

function dp_methods:NoClipping(...)
	return unwrap(self):NoClipping(...)
end

function dp_methods:GWEN_SetDock(...)
	return unwrap(self):GWEN_SetDock(...)
end

function dp_methods:GWEN_SetHorizontalAlign(...)
	return unwrap(self):GWEN_SetHorizontalAlign(...)
end

function dp_methods:StretchBottomTo(...)
	return unwrap(self):StretchBottomTo(...)
end

function dp_methods:NewObjectCallback(...)
	return unwrap(self):NewObjectCallback(...)
end

function dp_methods:GWEN_SetMax(...)
	return unwrap(self):GWEN_SetMax(...)
end

function dp_methods:GWEN_SetMargin(...)
	return unwrap(self):GWEN_SetMargin(...)
end

function dp_methods:GWEN_SetControlName(...)
	return unwrap(self):GWEN_SetControlName(...)
end

function dp_methods:NumSelectedChildren(...)
	return unwrap(self):NumSelectedChildren(...)
end

function dp_methods:RebuildSpawnIcon(...)
	return unwrap(self):RebuildSpawnIcon(...)
end

function dp_methods:GWEN_SetText(...)
	return unwrap(self):GWEN_SetText(...)
end

function dp_methods:GWEN_SetSize(...)
	return unwrap(self):GWEN_SetSize(...)
end

function dp_methods:GWEN_SetPosition(...)
	return unwrap(self):GWEN_SetPosition(...)
end

function dp_methods:SetAllowNonAsciiCharacters(...)
	return unwrap(self):SetAllowNonAsciiCharacters(...)
end

function dp_methods:LoadGWENString(...)
	return unwrap(self):LoadGWENString(...)
end

function dp_methods:GetChildPosition(...)
	return unwrap(self):GetChildPosition(...)
end

function dp_methods:Show(...)
	return unwrap(self):Show(...)
end

function dp_methods:IsSelectionCanvas(...)
	return unwrap(self):IsSelectionCanvas(...)
end

function dp_methods:IsHovered(...)
	return unwrap(self):IsHovered(...)
end

function dp_methods:Clear(...)
	return unwrap(self):Clear(...)
end

function dp_methods:__gc(...)
	return unwrap(self):__gc(...)
end

function dp_methods:MoveToBefore(...)
	return unwrap(self):MoveToBefore(...)
end

function dp_methods:MoveToAfter(...)
	return unwrap(self):MoveToAfter(...)
end

function dp_methods:MoveLeftOf(...)
	return unwrap(self):MoveLeftOf(...)
end

function dp_methods:SetPlayer(...)
	return unwrap(self):SetPlayer(...)
end

function dp_methods:SetEnabled(...)
	return unwrap(self):SetEnabled(...)
end

function dp_methods:DockMargin(...)
	return unwrap(self):DockMargin(...)
end

function dp_methods:SizeToContentsY(...)
	return unwrap(self):SizeToContentsY(...)
end

function dp_methods:MoveBelow(...)
	return unwrap(self):MoveBelow(...)
end

function dp_methods:GetTall(...)
	return unwrap(self):GetTall(...)
end

function dp_methods:Prepare(...)
	return unwrap(self):Prepare(...)
end

function dp_methods:AlphaTo(...)
	return unwrap(self):AlphaTo(...)
end

function dp_methods:SetVisible(...)
	return unwrap(self):SetVisible(...)
end

function dp_methods:ToggleVisible(...)
	return unwrap(self):ToggleVisible(...)
end

function dp_methods:MoveToFront(...)
	return unwrap(self):MoveToFront(...)
end

function dp_methods:SetWrap(...)
	return unwrap(self):SetWrap(...)
end

function dp_methods:SetName(...)
	return unwrap(self):SetName(...)
end

function dp_methods:LoadControlsFromFile(...)
	return unwrap(self):LoadControlsFromFile(...)
end

function dp_methods:HasHierarchicalFocus(...)
	return unwrap(self):HasHierarchicalFocus(...)
end

function dp_methods:SetModel(...)
	return unwrap(self):SetModel(...)
end

function dp_methods:DistanceFrom(...)
	return unwrap(self):DistanceFrom(...)
end

function dp_methods:GetChild(...)
	return unwrap(self):GetChild(...)
end

function dp_methods:SetPaintedManually(...)
	return unwrap(self):SetPaintedManually(...)
end

function dp_methods:Find(...)
	return unwrap(self):Find(...)
end

function dp_methods:GetTextSize(...)
	return unwrap(self):GetTextSize(...)
end

function dp_methods:RunJavascript(...)
	return unwrap(self):RunJavascript(...)
end

function dp_methods:SetKeyboardInputEnabled(...)
	return unwrap(self):SetKeyboardInputEnabled(...)
end

function dp_methods:IsValid(...)
	return unwrap(self):IsValid(...)
end

function dp_methods:SetText(...)
	return unwrap(self):SetText(...)
end

function dp_methods:FocusNext(...)
	return unwrap(self):FocusNext(...)
end

function dp_methods:GetCaretPos(...)
	return unwrap(self):GetCaretPos(...)
end

function dp_methods:ScreenToLocal(...)
	return unwrap(self):ScreenToLocal(...)
end

function dp_methods:SetKeyBoardInputEnabled(...)
	return unwrap(self):SetKeyBoardInputEnabled(...)
end

function dp_methods:IsDraggable(...)
	return unwrap(self):IsDraggable(...)
end

function dp_methods:SetFGColor(...)
	return unwrap(self):SetFGColor(...)
end

function dp_methods:SelectNone(...)
	return unwrap(self):SelectNone(...)
end

function dp_methods:SetContentAlignment(...)
	return unwrap(self):SetContentAlignment(...)
end

function dp_methods:GetValidReceiverSlot(...)
	return unwrap(self):GetValidReceiverSlot(...)
end

function dp_methods:GetTable(...)
	return unwrap(self):GetTable(...)
end

function dp_methods:DeleteCookie(...)
	return unwrap(self):DeleteCookie(...)
end

function dp_methods:SetFocusTopLevel(...)
	return unwrap(self):SetFocusTopLevel(...)
end

function dp_methods:__eq(...)
	return unwrap(self):__eq(...)
end

function dp_methods:SetWorldClicker(...)
	return unwrap(self):SetWorldClicker(...)
end

function dp_methods:MetaName(...)
	return unwrap(self):MetaName(...)
end

function dp_methods:ChildrenSize(...)
	return unwrap(self):ChildrenSize(...)
end

function dp_methods:ResetAllFades(...)
	return unwrap(self):ResetAllFades(...)
end

function dp_methods:SetSelectable(...)
	return unwrap(self):SetSelectable(...)
end

function dp_methods:GetContentSize(...)
	return unwrap(self):GetContentSize(...)
end

function dp_methods:AlignBottom(...)
	return unwrap(self):AlignBottom(...)
end

function dp_methods:GoForward(...)
	return unwrap(self):GoForward(...)
end

function dp_methods:UpdateHTMLTexture(...)
	return unwrap(self):UpdateHTMLTexture(...)
end

function dp_methods:DrawFilledRect(...)
	return unwrap(self):DrawFilledRect(...)
end

function dp_methods:Command(...)
	return unwrap(self):Command(...)
end

function dp_methods:PaintManual(...)
	return unwrap(self):PaintManual(...)
end

function dp_methods:Paste(...)
	return unwrap(self):Paste(...)
end

function dp_methods:SetRenderInScreenshots(...)
	return unwrap(self):SetRenderInScreenshots(...)
end

function dp_methods:SetFGColorEx(...)
	return unwrap(self):SetFGColorEx(...)
end

function dp_methods:MetaID(...)
	return unwrap(self):MetaID(...)
end

function dp_methods:SelectAllText(...)
	return unwrap(self):SelectAllText(...)
end

function dp_methods:DrawDragHover(...)
	return unwrap(self):DrawDragHover(...)
end

function dp_methods:SelectAllOnFocus(...)
	return unwrap(self):SelectAllOnFocus(...)
end

function dp_methods:Queue(...)
	return unwrap(self):Queue(...)
end

function dp_methods:NewAnimation(...)
	return unwrap(self):NewAnimation(...)
end

function dp_methods:HasFocus(...)
	return unwrap(self):HasFocus(...)
end

function dp_methods:SetPaintFunction(...)
	return unwrap(self):SetPaintFunction(...)
end

function dp_methods:SetPopupStayAtBack(...)
	return unwrap(self):SetPopupStayAtBack(...)
end

function dp_methods:DrawSelections(...)
	return unwrap(self):DrawSelections(...)
end

function dp_methods:OnStartDragging(...)
	return unwrap(self):OnStartDragging(...)
end

function dp_methods:FocusPrevious(...)
	return unwrap(self):FocusPrevious(...)
end

function dp_methods:OnStopDragging(...)
	return unwrap(self):OnStopDragging(...)
end

function dp_methods:MoveTo(...)
	return unwrap(self):MoveTo(...)
end

function dp_methods:SetMultiline(...)
	return unwrap(self):SetMultiline(...)
end

function dp_methods:SetCommand(...)
	return unwrap(self):SetCommand(...)
end

function dp_methods:DragMouseRelease(...)
	return unwrap(self):DragMouseRelease(...)
end

function dp_methods:GetWide(...)
	return unwrap(self):GetWide(...)
end

function dp_methods:AlignRight(...)
	return unwrap(self):AlignRight(...)
end

function dp_methods:DrawTexturedRect(...)
	return unwrap(self):DrawTexturedRect(...)
end

function dp_methods:SetSelected(...)
	return unwrap(self):SetSelected(...)
end

function dp_methods:IsMultiline(...)
	return unwrap(self):IsMultiline(...)
end

function dp_methods:SetCookie(...)
	return unwrap(self):SetCookie(...)
end

function dp_methods:GetChildrenInRect(...)
	return unwrap(self):GetChildrenInRect(...)
end

function dp_methods:Receiver(...)
	return unwrap(self):Receiver(...)
end

function dp_methods:IsSelectable(...)
	return unwrap(self):IsSelectable(...)
end

function dp_methods:StartBoxSelection(...)
	return unwrap(self):StartBoxSelection(...)
end

function dp_methods:SetFontInternal(...)
	return unwrap(self):SetFontInternal(...)
end

function dp_methods:SetBGColorEx(...)
	return unwrap(self):SetBGColorEx(...)
end

function dp_methods:SetCookieName(...)
	return unwrap(self):SetCookieName(...)
end

function dp_methods:SetHeight(...)
	return unwrap(self):SetHeight(...)
end

function dp_methods:SetWide(...)
	return unwrap(self):SetWide(...)
end

function dp_methods:StretchToParent(...)
	return unwrap(self):StretchToParent(...)
end

function dp_methods:GetCookieNumber(...)
	return unwrap(self):GetCookieNumber(...)
end

function dp_methods:SetSkin(...)
	return unwrap(self):SetSkin(...)
end

function dp_methods:CopyHeight(...)
	return unwrap(self):CopyHeight(...)
end

function dp_methods:Exec(...)
	return unwrap(self):Exec(...)
end

function dp_methods:ChildCount(...)
	return unwrap(self):ChildCount(...)
end

function dp_methods:SetPos(...)
	return unwrap(self):SetPos(...)
end

