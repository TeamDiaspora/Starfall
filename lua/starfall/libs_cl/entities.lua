-------------------------------------------------------------------------------
-- Clientside Entity functions
-------------------------------------------------------------------------------

assert( SF.Entities )

local ents_lib = SF.Entities.Library
local ents_metatable = SF.Entities.Metatable

--- Entity type
--@class class
--@name Entity
local ents_methods = SF.Entities.Methods
local wrap, unwrap = SF.Entities.Wrap, SF.Entities.Unwrap
local vunwrap = SF.UnwrapObject


-- ------------------------- Internal Library ------------------------- --

--- Gets the entity's owner
-- NOTE: As per Brandon, this uses silly code to implement what is already networked
-- by various PropProtect addons in order to prevent extra network traffic. If
-- no/unsupported PropProtect installed, then we'll just return true because reasons
-- of the CPPI spec being dumb and neglecting to make CPPIGetOwner clientside.
-- @return The entities owner, or nil if not found
function SF.Entities.GetOwner ( entity )
	local valid = SF.Entities.IsValid
	if not valid( entity ) then return end
	
	if entity.IsPlayer and entity:IsPlayer() then
		return entity
	end
	
	--One can hope...
	if CPPI and entity.CPPIGetOwner then
		local owner = entity:CPPIGetOwner()
		if valid( owner ) then return owner end
	end
	
	if NADMOD then
		--I would prefer to do this with entity pointers, but whatcha gunna do?
		local ownerName = NADMOD.PropOwners[entity:EntIndex()]
		return FindPlayerByName(ownerName)
	end

	return SF.instance.player
end

--- Gets the "owner" of the entity
-- @return Owner
function ents_methods:GetOwner ()
	SF.CheckType( self, ents_metatable )
	local ent = unwrap( self )
	return wrap( SF.Entities.GetOwner( ent ) )
end