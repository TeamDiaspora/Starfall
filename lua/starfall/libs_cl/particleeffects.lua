-------------------------------------------------------------------------------
-- Clientside Particle Effect functions
-------------------------------------------------------------------------------

--- Particle Effect Library, to complete the trifecta of pretty things we can do with particles
local pe_library = SF.DefaultEnvironment

local ents_methods = SF.Entities.Methods
local vunwrap = SF.Vectors.Unwrap
local aunwrap = SF.Angles.Unwrap
local eunwrap = SF.Entities.Unwrap

--- Creates a Particle Effect
-- @param particleName String: Name of the particle effect
-- @param position Vector: Start position of the effect
-- @param angles Angle: Orientation of the effect
-- @param parent Entity: Entity to parent the particle to, leave null and particle will not be parented. (Bear in mind, unparented particles can not be removed)
function pe_library.ParticleEffect( particleName, position, angles, parent )
    ParticleEffect(particleName,vunwrap(position),aunwrap(angles),eunwrap(parent))
end

--- Creates a Particle Effect
-- @param particleName String: Name of the particle effect
-- @param attachType <a href="http://wiki.garrysmod.com/page/Enums/PATTACH">PATTACH_Enum</a>: Changes how the particle moves with the given attachmentID
-- @param entity Entity: Entity to attach the particle too based on the attachmentID
-- @param attachmentID Number: Numeric attachment ID of the passed entity to attach this hologram to. To find the names and indicies of all attachments for an entity, try PrintTable( E:GetAttachments() )
function pe_library.ParticleEffectAttach( particleName, attachType, entity, attachmentID )
    ParticleEffectAttach(particleName,attachType,eunwrap(entity),attachmentID)
end

--- Removes all particles parented to an Entity
function ents_methods:StopParticles()
    local ent = eunwrap(self)
    -- Should probably have a prop-protection check in here too but it's not like anything else in starfall has one
    if IsValid(ent) then
        ent:StopParticles()
    end
end

--- Removes all particles parented to an Entity
function ents_methods:StopParticleEmission()
    local ent = eunwrap(self)
    if IsValid(ent) then
        ent:StopParticleEmission()
    end
end
    
--- Removes all particles parented to an entity that use the given particle name
-- @param name String: Name of the particle to remove
function ents_methods:StopParticlesNamed( name )
    local ent = eunwrap(self)
    if IsValid(ent) then
        ent:StopParticlesNamed(name)
    end
end
    
--- Removes all particles attached to this entity with the attachementID and given particle name
-- @param name String: Name of the particle to remove
-- @param attachmentID Number: attachmentID of the entity to remove the particle from
function ents_methods:StopParticlesWithNameAndAttachment( name, attachmentID )
    local ent = eunwrap(self)
    if IsValid(ent) then
        ent:StopParticlesWithNameAndAttachment(name,attachementID )
    end
end
