//Created by Nielk1

local PuntAlert = {}
local runByPunt = 0
local PuntedBy = nil
local PuntedEnt = nil


--[[************************************************************************]]--

hook.Add( "GravGunPunt", "E2GravGunPunt", function( userid, target )
	runByPunt = 1
	PuntedBy = userid
	PuntedEnt = target
	for e,_ in pairs(PuntAlert) do
		if IsValid(e) then
			e:Execute()
		else
			PuntAlert[e] = nil
		end
	end
	PuntedBy = nil
	PuntedEnt = nil
	runByPunt = 0
end )

e2function void runOnPunt(activate)
	if activate ~= 0 then
		PuntAlert[self.entity] = true
	else
		PuntAlert[self.entity] = nil
	end
end
 
e2function entity puntedBy()
	return PuntedBy
end

e2function entity puntedEnt()
	return PuntedEnt
end

e2function number puntClk()
	if not IsValid(PuntedEnt) then return 0 end
	return runByPunt
end

e2function number puntClk(entity target)
	if not IsValid(target) then return 0 end
	if target != PuntedEnt then return 0 end
	return runByPunt
end