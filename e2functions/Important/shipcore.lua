
--[[***********************************************************
*************          Duneds core-reading extension          ************
***********************************************************]]--

e2function number entity:getShieldAmount()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetShieldAmount()
		else
			return 0
		end
	end
end

e2function number entity:getArmorAmount()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetArmorAmount()
		else
			return 0
		end
	end
end

e2function number entity:getHullAmount()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetHullAmount()
		else
			return 0
		end
	end
end

e2function number entity:getShieldPercent()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetShieldPercent()
		else
			return 0
		end
	end
end

e2function number entity:getArmorPercent()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetArmorPercent()
		else
			return 0
		end
	end
end

e2function number entity:getHullPercent()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetHullPercent()
		else
			return 0
		end
	end
end

e2function number entity:getShieldMax()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetShieldMax()
		else
			return 0
		end
	end
end

e2function number entity:getArmorMax()
	if IsValid(this) then
		if this:GetClass() == "ship_core" then
			return this:GetArmorMax()
		else
			return 0
		end
	end
end

e2function number entity:getHullMax()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetHullMax()
		else
			return 0
		end
	end
end

--[[***********************************************************
*************          Fehas core-reading extension          ************
***********************************************************]]--

e2function number entity:getCap()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetCapAmount()
		else
			return 0
		end
	end
end

e2function number entity:getCapPercent()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetCapPercent()
		else
			return 0
		end
	end
end

e2function number entity:getCapMax()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetCapMax()
		else
			return 0
		end
	end
end


--[[***********************************************************
*************          Steeveeo's Core Extensions          ************
***********************************************************]]--


e2function entity entity:getCoreEnt()
	if not IsValid(this) then return nil end
	if this:IsPlayer() or this:IsWorld() then return nil end
	
	return this.SC_CoreEnt or NullEntity()
end

e2function string entity:getShipName()
	if not IsValid(this) then return "" end
	
	local core = this
	if this:GetClass() ~= "ship_core" then
		if IsValid(this.SC_CoreEnt) then
			core = this.SC_CoreEnt
		else
			return ""
		end
	end
	
	return core:GetShipName()
end

e2function void entity:setShipName(string name)
	if not IsValid(this) then return end
	if not isOwner(self,this) then return end
	if this:GetClass() ~= "ship_core" then return end
	
	this:SetShipName(name)
end

--ADMIN ONLY FROM HERE DOWN--

e2function number entity:setHullAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:SetHullAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:setArmorAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:SetArmorAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:setShieldAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:SetShieldAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:setCapAmount(number Amount)
	if !self.player:IsSuperAdmin() then return -1 end
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:SetCapAmount(Amount)
		else
			return -1
		end
	end
end

e2function number entity:combatImmune(number active)
	if (!IsValid(this)) then return -1 end
	
	local owner = self.player
	
	if (owner:IsAdmin() and !this:IsPlayer()) then
		this.SC_Immune = tobool(active)
		return active
	end
	
	return -1
end

--[[**********************************************************
*************Lt.Brandon's core/weapon-reading extension************
***********************************************************]]--
local E2TABLE = {n = {}, s = {}, stypes = {}, ntypes = {}, size = 0, istable = true}
e2function number entity:getSigRad()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetNodeRadius()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShieldRechargeTime()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetShieldRechargeTime()
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShieldRecharge()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:CalculateRecharge(this.Shield.Max, this.Shield.Amount, this:GetShieldRechargeTime())
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShieldRechargeMax()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:CalculateRecharge(this.Shield.Max, this.Shield.Max * 0.25, this:GetShieldRechargeTime())
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getShipMass()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this.total_mass
		else
			return -1
		end
	end
	return -2
end

e2function string entity:getCoreClass()
	if IsValid(this) then 
		if this:GetClass() == "ship_core" then
			return this:GetShipClass()
		else
			return "Unknown/Not a core"
		end
	end
	return "Invalid Entity"
end

e2function number entity:getStationCore()
	if IsValid(this) then 
		if string.find(this:GetClass(), "ship_core") then
			return (1 and this.StationCore) or 0
		else
			return -1
		end
	end
	
	return -2
end

--weapon functions
e2function table entity:getTurretWeapons()
	if IsValid(this) then
		if this:GetClass() == "sc_turret" then
			local tab = table.Copy(E2TABLE)
			
			for i,k in pairs(this.Weapons) do
				tab.n[i] = k
				tab.ntypes[i] = "e"
				tab.size = tab.size + 1
			end
			
			return tab
		end
	end
	
	return table.Copy(E2TABLE)
end

e2function string entity:getWeaponName()
	if IsValid(this) then 
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.PrintName
		else
			return "Unknown/Not a weapon"
		end
	end
	return "Invalid Entity"
end

e2function string entity:getWeaponClass()
	if IsValid(this) then 
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.Data.GUN.CLASS
		else
			return "Unknown/Not a weapon"
		end
	end
	return "Invalid Entity"
end

e2function number entity:getShotsRemaining()
	if IsValid(this) then 
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.ShotsRemaining
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getIsReloading()
	if IsValid(this) then 
		if string.find(this:GetClass(), "sc_weapon*") then
			return this.IsReloading and 1 or 0
		else
			return -1
		end
	end
	return -2
end

e2function number entity:getIsFiring()
	if IsValid(this) then 
		if string.find(this:GetClass(), "sc_weapon*") then
			return this:GetShooting() and 1 or 0
		else
			return -1
		end
	end
	return -2
end