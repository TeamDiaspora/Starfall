//Created by Nielk1

local PhyPickupAlert = {}
local runByPhyPickup = 0
local PhyPickupBy = nil
local PhyPickupEnt = nil


--[[************************************************************************]]--

hook.Add( "PhysgunPickup", "E2PhysgunPickup", function( userid, target )
	runByPhyPickup = 1
	PhyPickupBy = userid
	PhyPickupEnt = target
	for e,_ in pairs(PhyPickupAlert) do
		if IsValid(e) then
			e:Execute()
		else
			PhyPickupAlert[e] = nil
		end
	end
	PhyPickupBy = nil
	PhyPickupEnt = nil
	runByPhyPickup = 0
end )

e2function void runOnPhyPickup(activate)
	if activate ~= 0 then
		PhyPickupAlert[self.entity] = true
	else
		PhyPickupAlert[self.entity] = nil
	end
end
 
e2function entity phyPickupBy()
	return PhyPickupBy
end

e2function entity phyPickupEnt()
	return PhyPickupEnt
end

e2function number phyPickupClk()
	if not IsValid(PhyPickupEnt) then return 0 end
	return runByPhyPickup
end

e2function number phyPickupClk(entity target)
	if not IsValid(target) then return 0 end
	if target != PhyPickupEnt then return 0 end
	return runByPhyPickup
end