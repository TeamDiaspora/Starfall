--AddCSLuaFile("cl_render.lua")
function createEffectsfromE2(self,effect,pos,mag,angles,pos2,scale,entity)
	local exclude = {"ptorpedoimpact", "effect_explosion_scaleable", "nuke_blastwave", "nuke_blastwave_cheap", "nuke_disintegrate", "nuke_effect_air", "nuke_effect_ground", "nuke_vaporize", "warpcore_breach"}
	local ent = self.entity
    if not ent.Delay then
		ent.Delay = 0
    end
    if table.HasValue(exclude, effect) then return nil end
    if angles && pos2 && scale && entity && ent.Delay < CurTime() then
		fx = EffectData()
		fx:SetOrigin(pos)
		fx:SetStart(pos2)
		fx:SetMagnitude(mag)
		fx:SetAngles(angles)
		fx:SetScale(scale)
		fx:SetEntity(entity)
		util.Effect(effect,fx)
		ent.Delay = CurTime() + 1
    elseif !angles && !pos2 && !scale && !entity && ent.Delay < CurTime() then
		fx = EffectData()
		fx:SetOrigin(pos)
		fx:SetMagnitude(mag)
		util.Effect(effect,fx)
		ent.Delay = CurTime() + 1
    elseif !pos2 && !scale && !entity && ent.Delay < CurTime() then
		fx = EffectData()
		fx:SetOrigin(pos)
		fx:SetMagnitude(mag)
		fx:SetAngles(angles)
		util.Effect(effect,fx)
		ent.Delay = CurTime() + 1
	elseif !scale && !entity && ent.Delay < CurTime() then
		fx = EffectData()
		fx:SetOrigin(pos)
		fx:SetMagnitude(mag)
		fx:SetAngles(angles)
		fx:SetStart(pos2)
		util.Effect(effect,fx)
		ent.Delay = CurTime() + 1
	elseif !entity && ent.Delay < CurTime() then
		fx = EffectData()
		fx:SetOrigin(pos)
		fx:SetMagnitude(mag)
		fx:SetAngles(angles)
		fx:SetStart(pos2)
		fx:SetScale(scale)
		util.Effect(effect,fx)
		ent.Delay = CurTime() + 1
    end
    return fx   
end

function createParticle(self,effect,entity)
	local Ent = self.entity
    if not Ent.Count then
		Ent.Count = 0
    end
    if terminate && Ent.Count == 0 then return nil end
    if effect == "finish" && Ent.Count == 0 then return nil end
    if not entity && Ent.Count < 5 then
         ParticleEffectAttach(effect,PATTACH_ABSORIGIN_FOLLOW,Ent,0)
         Ent.Count = Ent.Count + 1
    elseif entity && Ent.Count < 5 then
         ParticleEffectAttach(effect,PATTACH_ABSORIGIN_FOLLOW,entity,0)
         Ent.Count = Ent.Count + 1
    elseif effect == "finish" && Ent.Count > 0 then
         Ent:StopParticles()
         Ent.Count = 0
    return nil
    end
    return Particle
end

       
registerFunction("particle","s","particle",function(self, args)
    local op1= args[2]
    local rv1 = op1[1](self, op1)
    return createParticle(self,rv1)
 end)   
registerFunction("particle","se","particle",function(self, args)
    local op1, op2 = args[2], args[3]
    local rv1, rv2= op1[1](self, op1), op2[1](self, op2)
    return createParticle(self,rv1, rv2)
end)
                
registerFunction("fx","svn","fx", function(self, args)
    local op1, op2, op3 = args[2], args[3], args[4]
    local rv1, rv2, rv3 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3)
    return createEffectsfromE2(self,rv1,Vector(rv2[1],rv2[2],rv2[3]),rv3)
end)

registerFunction("fx","svna","fx", function(self, args)
    local op1, op2, op3, op4 = args[2], args[3], args[4], args[5]
    local rv1, rv2, rv3, rv4 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4)
    return createEffectsfromE2(self,rv1,Vector(rv2[1],rv2[2],rv2[3]),rv3,Angle(rv4[1],rv4[2],rv4[3]))
end)

registerFunction("fx","svnav","fx", function(self, args)
    local op1, op2, op3, op4, op5 = args[2], args[3], args[4], args[5], args[6]
    local rv1, rv2, rv3, rv4, rv5 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4), op5[1](self, op5)
    return createEffectsfromE2(self,rv1,Vector(rv2[1],rv2[2],rv2[3]),rv3,Angle(rv4[1],rv4[2],rv4[3]),Vector(rv5[1],rv5[2],rv5[3]))
end)

registerFunction("fx","svnavn","fx", function(self, args)
    local op1, op2, op3, op4, op5, op6 = args[2], args[3], args[4], args[5], args[6], args[7]
    local rv1, rv2, rv3, rv4, rv5, rv6 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4), op5[1](self, op5), op6[1](self, op6)
    return createEffectsfromE2(self,rv1,Vector(rv2[1],rv2[2],rv2[3]),rv3,Angle(rv4[1],rv4[2],rv4[3]),Vector(rv5[1],rv5[2],rv5[3]), rv6)
end)
	
registerFunction("fx","svnavne","fx", function(self, args)
    local op1, op2, op3, op4, op5, op6, op7 = args[2], args[3], args[4], args[5], args[6], args[7], args[8]
    local rv1, rv2, rv3, rv4, rv5, rv6, rv7 = op1[1](self, op1), op2[1](self, op2), op3[1](self, op3), op4[1](self, op4), op5[1](self, op5), op6[1](self, op6), op7[1](self, op7)
    return createEffectsfromE2(self,rv1,Vector(rv2[1],rv2[2],rv2[3]),rv3,Angle(rv4[1],rv4[2],rv4[3]),Vector(rv5[1],rv5[2],rv5[3]), rv6, rv7)
end)

/******************************************************************************
**************  Duneds Draw extension (Revamped by Steeveeo)  *****************
******************************************************************************/

Sprites = {}

//Setup messages for Net library
util.AddNetworkString("DrawSprite - Client Requesting Sync")
util.AddNetworkString("DrawSprite - Client Sync Complete")
util.AddNetworkString("DrawSprite - Create Sprite")
util.AddNetworkString("DrawSprite - Sync Sprite")
util.AddNetworkString("DrawSprite - Remove Sprite")
util.AddNetworkString("DrawSprite - Remove All Sprites")
util.AddNetworkString("DrawSprite - setSpriteMat")
util.AddNetworkString("DrawSprite - setSpritePos")
util.AddNetworkString("DrawSprite - setSpriteSizeX")
util.AddNetworkString("DrawSprite - setSpriteSizeY")
util.AddNetworkString("DrawSprite - setSpriteColor")
util.AddNetworkString("DrawSprite - setSpriteAlpha")
util.AddNetworkString("DrawSprite - setSpriteIgnoreZ")
util.AddNetworkString("DrawSprite - setSpriteParent")
util.AddNetworkString("DrawSprite - setSpriteEntFilter")

function syncSprites(ply)
	for ent, k in pairs(Sprites) do
		for index, v in pairs(Sprites[ent]) do
			net.Start("DrawSprite - Sync Sprite")
				net.WriteFloat(index)
				net.WriteFloat(ent:EntIndex())
				net.WriteString(Sprites[ent][index]["mat"])
				net.WriteVector(Sprites[ent][index]["pos"])
				net.WriteEntity(Sprites[ent][index]["parent"])
				net.WriteVector(Sprites[ent][index]["color"])
				net.WriteFloat(Sprites[ent][index]["alpha"])
				net.WriteFloat(Sprites[ent][index]["sizex"])
				net.WriteFloat(Sprites[ent][index]["sizey"])
				net.WriteFloat(Sprites[ent][index]["ignoreZ"])
				
				//Write filtered ents
				local entFilter = Sprites[ent][index]["entfilter"]
				if entFilter == nil then
					net.WriteFloat(0)
				else
					net.WriteFloat(#entFilter)
					for h,j in pairs(entFilter) do
						net.WriteFloat(j:EntIndex())
					end
				end
			net.Send(ply)
		end
	end
	
	//Notify client that it can start rendering
	net.Start("DrawSprite - Client Sync Complete")
		//Need something here?
	net.Send(ply)
end

//Client asking for sprites
net.Receive("DrawSprite - Client Requesting Sync", function(len, ply)
	syncSprites(ply)
end)

local function createSprite(self, index, mat, pos, sizex, sizey, color, alpha, parent)
	
	local ent = self.entity

	if not Sprites[ent] then
		Sprites[ent] = { }
	end
	
	if table.Count(Sprites[ent]) < 50 then
		if not Sprites[ent][index] then
			Sprites[ent][index] = {}
		end
		
		Sprites[ent][index]["mat"] = mat
		Sprites[ent][index]["parent"] = parent
		Sprites[ent][index]["color"] = Vector(color[1], color[2], color[3])
		Sprites[ent][index]["alpha"] = alpha
		Sprites[ent][index]["sizex"] = sizex
		Sprites[ent][index]["sizey"] = sizey
		Sprites[ent][index]["pos"] = Vector(pos[1], pos[2], pos[3])
		
		//Send Info
		net.Start("DrawSprite - Create Sprite")
			net.WriteFloat(index)
			net.WriteFloat(ent:EntIndex())
			net.WriteString(mat)
			net.WriteEntity(parent)
			net.WriteVector(Vector(color[1], color[2], color[3]))
			net.WriteFloat(alpha)
			net.WriteFloat(sizex)
			net.WriteFloat(sizey)
			net.WriteVector(Vector(pos[1], pos[2], pos[3]))
		net.Broadcast()
	end
end

e2function void drawSprite(number index, string mat, vector pos, number sizex, number sizey, vector color, number alpha, entity parent)
	return createSprite(self, index, mat, pos, sizex, sizey, color, alpha, parent)
end

e2function void setSpriteEntFilter(number index, array ignoreEnts)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
				Sprites[ent][index]["entfilter"] = ignoreEnts
				
				net.Start("DrawSprite - setSpriteEntFilter")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteFloat(#ignoreEnts)
					for k,v in pairs(ignoreEnts) do
						net.WriteFloat(v:EntIndex())
					end
				net.Broadcast()
			end
		end
	end
end

e2function void setSpritePos(number index, vector pos)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
			
				if pos == Sprites[ent][index]["pos"] then return end
				
				Sprites[ent][index]["pos"] = pos
				
				net.Start("DrawSprite - setSpritePos")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteVector(Vector(pos[1], pos[2], pos[3]))
				net.Broadcast()
			end
		end
	end
end

e2function void setSpriteTexture(number index, string material)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
			
				if material == Sprites[ent][index]["mat"] then return end
				
				Sprites[ent][index]["mat"] = material
				
				net.Start("DrawSprite - setSpriteMat")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteString(mat)
				net.Broadcast()
			end
		end
	end
end

e2function void setSpriteIgnoreZ(number index, number ignoreZ)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
				local doIgnore = 1
				if ignoreZ == 0 then
					doIgnore = 0
				end
			
				if doIgnore == Sprites[ent][index]["ignoreZ"] then return end
				
				Sprites[ent][index]["ignoreZ"] = doIgnore
				
				net.Start("DrawSprite - setSpriteIgnoreZ")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteFloat(doIgnore)
				net.Broadcast()
			end
		end
	end
end

e2function void setSpriteColor(number index, vector color)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
				if !Sprites[ent][index]["color"] then
					Sprites[ent][index]["color"] = Color(255,255,255,255)
				end
				
				Sprites[ent][index]["color"] = Color(color[1], color[2], color[3], Sprites[ent][index]["color"].a or 255)
				
				net.Start("DrawSprite - setSpriteColor")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteVector(Vector(color[1], color[2], color[3]))
				net.Broadcast()
			end
		end
	end
end

e2function void setSpriteAlpha(number index, number alpha)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
				if !Sprites[ent][index]["color"] then
					Sprites[ent][index]["color"] = Color(255,255,255,255)
				end
				
				Sprites[ent][index]["color"].a = alpha
				
				net.Start("DrawSprite - setSpriteAlpha")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteFloat(alpha)
				net.Broadcast()
			end
		end
	end
end

e2function void setSpriteSizeX(number index, number sizeX)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
			
				if sizeX == Sprites[ent][index]["sizex"] then return end
				
				Sprites[ent][index]["sizex"] = sizeX
				
				net.Start("DrawSprite - setSpriteSizeX")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteFloat(sizeX)
				net.Broadcast()
			end
		end
	end
end

e2function void setSpriteSizeY(number index, number sizeY)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
			
				if sizeY == Sprites[ent][index]["sizey"] then return end
				
				Sprites[ent][index]["sizey"] = sizeY
				
				net.Start("DrawSprite - setSpriteSizeY")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteFloat(sizeY)
				net.Broadcast()
			end
		end
	end
end

e2function void spriteParent(number index, entity parent)
	local ent = self.entity
	
	if index > 0 then
		if Sprites[ent] then
			if Sprites[ent][index] then
			
				if parent == Sprites[ent][index]["parent"] then return end
				
				Sprites[ent][index]["parent"] = parent
				
				net.Start("DrawSprite - setSpriteParent")
					net.WriteFloat(index)
					net.WriteFloat(ent:EntIndex())
					net.WriteEntity(parent)
				net.Broadcast()
			end
		end
	end
end
		
e2function void removeSprite(number index)
	local ent = self.entity
	
	if Sprites[ent] then
		if Sprites[ent][index] then
			Sprites[ent][index] = nil
			
			net.Start("DrawSprite - Remove Sprite")
				net.WriteFloat(index)
				net.WriteFloat(ent:EntIndex())
			net.Broadcast()
		end
	end
end

e2function void removeAllSprites()
	local ent = self.entity
	
	if Sprites[ent] then
		Sprites[ent] = nil
		
		net.Start("DrawSprite - Remove All Sprites")
			net.WriteFloat(ent:EntIndex())
		net.Broadcast()
	end
end

//--END OF DRAW SPRITE FUNCTIONS----------------------

function clipPlane(self, indx, array, plane, dis)
local Ent = self.entity
local n = indx
	if not Ent.IdxC then
		Ent.IdxC = { }
	end
	if table.Count(Ent.IdxC) < 50 then
		if not Ent.IdxC[n] then
			local Clip = ents.Create("E2_Light")
				Clip:SetModel("models/beer/wiremod/gate_e2.mdl" )
				Clip:SetColor(255,255,255,255)
				Clip:SetPos(Vector(plane[1], plane[2], plane[3]))
				Clip:SetParent(self.entity)
				Clip:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
				Clip:SetAngles(self.entity:GetAngles())
				Clip.Type = "Clip"
				Clip.tbl = {array, Vector(plane[1], plane[2], plane[3]), Vector(dis[1], dis[2], dis[3]), Clip.Type}
				table.insert(Ent.IdxC, n, Clip)
				Clip:Spawn()
				Clip:Activate()
		end
	end
end		

function DrawBeam(self,indx,name,startpos,endpos,width,TextStart,TextEnd,color,alpha,parent)
local Ent = self.entity
local n = indx
	if not Ent.IdxD then
		Ent.IdxD = { }
	end
		if table.Count(Ent.IdxD) < 5  then
			if not Ent.IdxD[n] then
				local beam = ents.Create("E2_Light")
					beam:SetModel("models/beer/wiremod/gate_e2.mdl" )
					beam:SetColor(255,255,255,255)
					beam:SetPos(Vector(startpos[1], startpos[2], startpos[3]))
					beam:SetParent(Ent)
					beam:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
					beam:SetAngles(Ent:GetAngles())
					beam.Type = "Beam"
					beam.tbl = {name, Vector(startpos[1], startpos[2], startpos[3]), Vector(endpos[1], endpos[2], endpos[3]), width, TextStart, TextEnd, Vector(color[1], color[2], color[3]), alpha, parent, beam.Type}
					table.insert(Ent.IdxD, n, beam)
					beam:Spawn()
					beam:Activate()
			end		
		end
end
function DynLight(self,indx,pos,size,color,brightnes)
local Ent = self.entity
local n = indx
	if not Ent.IdxL then
		Ent.IdxL = { }
	end
		if table.Count(Ent.IdxL) < 50 then
			if not Ent.IdxL[n] then
				local light = ents.Create("E2_Light")
					light:SetModel("models/beer/wiremod/gate_e2.mdl" )
					light:SetColor(255,255,255,255)
					light:SetPos(Vector(pos[1], pos[2], pos[3]))
					light:SetParent(Ent)
					light:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
					light:SetAngles(Ent:GetAngles())
					light.Type = "light"
					light.tbl = {Vector(pos[1], pos[2], pos[3]), size, Vector(color[1], color[2], color[3]), brightnes, light.Type}
					table.insert(Ent.IdxL, n, light)
					light:Spawn()
					light:Activate()
			end
		end
end

function DrawQuad(self,indx,name,pos,Dir,Sizex,Sizey,Color,alpha,ang,parent)
local Ent = self.entity
local n = indx
	if not Ent.IdxQ then
		Ent.IdxQ = { }
	end
		if table.Count(Ent.IdxQ) < 50 then
			if not Ent.IdxQ[n] then
				local Quad = ents.Create("E2_Light")
					Quad:SetModel("models/beer/wiremod/gate_e2.mdl" )
					Quad:SetColor(255,255,255,255)
					Quad:SetPos(Vector(pos[1], pos[2], pos[3]))
					Quad:SetParent(Ent)
					Quad:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
					Quad:SetAngles(Ent:GetAngles())
					Quad.Type = "Quad"
					Quad.tbl = {name,Vector(pos[1], pos[2], pos[3]),Vector(Dir[1],Dir[2],Dir[3]), Sizex, Sizey, Vector(Color[1], Color[2], Color[3]), alpha, ang, parent, Quad.Type}
					table.insert(Ent.IdxQ, n, Quad)
					Quad:Spawn()
					Quad:Activate()
			end
		end
end

e2function void drawQuad(number indx, string name, vector pos, vector Dir, number Sizex, number Sizey, vector Color, number alpha, angle ang, number parent)
	return DrawQuad(self,indx,name,pos,Dir,Sizex,Sizey,Color,alpha,ang,parent)
end

e2function void array:setRenderClipPlane(number indx, vector plane, vector distance)
	return clipPlane(self,indx,this,plane,distance)
end
e2function void setRenderClipDistance(number indx, vector dis)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxC then
			if Ent.IdxC[n] then
				Ent.IdxC[n].tbl[3] = Vector(dis[1], dis[2], dis[3])
			end
		end
	end
end

e2function void setRenderClipDirection(number indx, vector plane)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxC then
			if Ent.IdxC[n] then
				Ent.IdxC[n].tbl[2] = Vector(plane[1], plane[2], plane[3])
			end
		end
	end
end

e2function void dLight(number indx, vector pos, number size, vector color, number brightnes)
	return DynLight(self,indx,pos,size,color,brightnes)
end

e2function void drawBeam(number indx, string name, vector startpos, vector endpos, number width, number TextStart, number TextEnd, vector color, number alpha, number parent)
	return DrawBeam(self,indx,name,startpos,endpos,width,TextStart,TextEnd,color,alpha,parent)
end

e2function void setQuadPos(number indx, vector pos)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[2] = Vector(pos[1], pos[2], pos[3])
			end
		end
	end
end

e2function void setDlightPos(number indx, vector pos)	//This one is made by DLB
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxL then
			if Ent.IdxL[n] then
				Ent.IdxL[n].tbl[1] = Vector(pos[1], pos[2], pos[3])
			end
		end
	end
end

e2function void setQuadDir(number indx, vector dir)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[3] = Vector(dir[1], dir[2], dir[3])
			end
		end
	end
end

e2function void setQuadColor(number indx, vector color)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[6] = Vector(color[1], color[2], color[3])
			end
		end
	end
end

e2function void setDlightColor(number indx, vector color)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxL then
			if Ent.IdxL[n] then
				Ent.IdxL[n].tbl[3] = Vector(color[1], color[2], color[3])
			end
		end
	end
end

e2function void setDlightSize(number indx, number size)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxL then
			if Ent.IdxL[n] then
				Ent.IdxL[n].tbl[2] = size
			end
		end
	end
end

e2function void setDlightBright(number indx, number bright)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxL then
			if Ent.IdxL[n] then
				Ent.IdxL[n].tbl[4] = bright
			end
		end
	end
end

e2function void setQuadAlpha(number indx, number alpha)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[7] = alpha
			end
		end
	end
end

e2function void setQuadSizeX(number indx, number sizex)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[4] = sizex
			end	
		end
	end
end

e2function void setQuadSizeY(number indx, number sizey)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[5] = sizey
			end
		end
	end
end

e2function void setQuadAngle(number indx, angle ang)
local Ent = self.entity
local n = indx
	if (n > 0) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n].tbl[8] = Angle(ang[1],ang[2],ang[3])
			end
		end
	end
end

e2function void removeClip(number indx)
local Ent = self.entity
local n = indx
	if (n > 0 ) then
		if Ent.IdxC then
			if Ent.IdxC[n] then
				Ent.IdxC[n]:Remove()
				table.remove(Ent.IdxC, n)
			end
		end
	end
end

e2function void removeQuad(number indx)
local Ent = self.entity
local n = indx
	if (n > 0 ) then
		if Ent.IdxQ then
			if Ent.IdxQ[n] then
				Ent.IdxQ[n]:Remove()
				table.remove(Ent.IdxQ, n)
			end
		end
	end
end

e2function void removeDlight(number indx)
local Ent = self.entity
local n = indx
	if (n > 0 ) then
		if Ent.IdxL then
			if Ent.IdxL[n] then
				Ent.IdxL[n]:Remove()
				table.remove(Ent.IdxL, n)
			end
		end
	end
end

e2function void removeAllClips()
local Ent = self.entity
	if Ent.IdxC then
		for k,v in pairs(Ent.IdxC) do
			v:Remove()
		end
		table.Empty(Ent.IdxC)
	end
end

e2function void removeAllQuads()
local Ent = self.entity
	if Ent.IdxQ then
		for k,v in pairs(Ent.IdxQ) do
			v:Remove()
		end
		table.Empty(Ent.IdxQ)
	end
end

e2function void removeAllDlights()
local Ent = self.entity
	if Ent.IdxL then
		for k,v in pairs(Ent.IdxL) do
			v:Remove()
		end
		table.Empty(Ent.IdxL)
	end
end

registerCallback("destruct", function(self)
local Ent = self.entity
	if Sprites[Ent] then	
		Sprites[Ent] = nil
		
		net.Start("DrawSprite - Remove All Sprites")
			net.WriteEntity(ent)
		net.Broadcast()
	end
	
	if Ent.IdxD then
		if table.Count(Ent.IdxD) > 0 then
			for k,v in pairs(Ent.IdxD) do
				v:Remove()
			end
		table.Empty(Ent.IdxD)
		end
	end
	if Ent.IdxL then
		if table.Count(Ent.IdxL) > 0 then
			for k,v in pairs(Ent.IdxL) do
				v:Remove()
			end
		table.Empty(Ent.IdxL)
		end
	end
	if Ent.IdxQ then
		if table.Count(Ent.IdxQ) > 0 then
			for k,v in pairs(Ent.IdxQ) do
				v:Remove()
			end
		table.Empty(Ent.IdxQ)
		end
	end
	if Ent.IdxC then
		if table.Count(Ent.IdxC) > 0 then
			for k,v in pairs(Ent.IdxC) do
				v:Remove()
			end
		table.Empty(Ent.IdxC)
		end
	end
end)