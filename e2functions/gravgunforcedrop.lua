-- By Nielk1
e2function void entity:gravGunForceDrop()
	if not validPhysics(this) then return nil end
	if not isOwner(self, this) then return nil end
	DropEntityIfHeld(this)
end