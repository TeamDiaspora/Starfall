AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")
include("starfall/SFLib.lua")
assert(SF, "Starfall didn't load correctly!")
local context = SF.CreateContext()
--String used in the Starfall method 'SetOverlayText.'
ENT.functionName = ""

function ENT:Initialize()
    self.BaseClass.Initialize(self)
    self:SetColor(Color(255, 0, 0, self:GetColor().a))
end

function ENT:CodeSent(ply, files, mainfile)
    self.BaseClass.CodeSent(self, ply, files, mainfile)
    if not self.instance then return end
    self.name = nil

    if self.instance.ppdata.scriptnames and self.instance.mainfile and self.instance.ppdata.scriptnames[self.instance.mainfile] then
        self.name = tostring(self.instance.ppdata.scriptnames[self.instance.mainfile])
    end

    local clr = self:GetColor()
    self:SetColor(Color(255, 255, 255, clr.a))
end

function ENT:Error(msg, traceback)
    self.BaseClass.Error(self, msg, traceback)
    self:SetColor(Color(255, 0, 0, 255))
end

function ENT:OnRemove()
    self.BaseClass.OnRemove(self)
end

duplicator.RegisterEntityClass("starfall_processor", GAMEMODE.MakeEnt, "Data")