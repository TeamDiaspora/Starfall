-------------------------------------------------------------------------------
-- Clientside Draw Library functions
-------------------------------------------------------------------------------

--- Draw Library - Simplified usage of the surface library. 
-- @client
local draw_library, _ = SF.Libraries.Register( "draw" )

local function CanRenderOnClient()
    if (LocalPlayer():InVehicle() and FindPlayerByName(NADMOD.PropOwners[LocalPlayer():GetVehicle():EntIndex()]) or LocalPlayer()) ~= SF.instance.player then return false end
    return true
end
--- Simple draw text at position, but this will expand newlines and tabs. 
-- @param text Text to be drawn.
-- @param font Name of font to draw the text in. See <a href="http://wiki.garrysmod.com/page/surface/CreateFont">surface.CreateFont</a> or <a href="http://wiki.garrysmod.com/page/Default_Fonts">Default Font</a> for a list of default fonts. 
-- @param x The X Coordinate.
-- @param y The Y Coordinate.
-- @param color Color to draw the text in. Uses the <a href="http://wiki.garrysmod.com/page/Structures/Color">Color structure</a>.
-- @param xAlign Where to align the text horizontally. Uses the <a href="http://wiki.garrysmod.com/page/Enums/TEXT">TEXT_Enums</a>.
function draw_library.DrawText(...)
    if not CanRenderOnClient() then return end
    return draw.DrawText(...)
end
--- Returns the height of the specified font in pixels. 
-- @param font Name of the font to get the height of.
-- @return fontHeight the height of the font.
function draw_library.GetFontHeight(...)
    if not CanRenderOnClient() then return end
    return draw.GetFontHeight(...)
end
--- Sets drawing texture to a default white texture (vgui/white). Useful for resetting the drawing texture. 
function draw_library.NoTexture(...)
    if not CanRenderOnClient() then return end
    return draw.NoTexture(...)
end
--- Draws a rounded rectangle. 
-- @param cornerRadius Radius of the rounded corners, works best with a multiple of 2.
-- @param x The x coordinate of the top left of the rectangle.
-- @param y The y coordinate of the top left of the rectangle.
-- @param width The width of the rectangle.
-- @param height The height of the rectangle.
-- @param color The color to fill the rectangle with. Uses the <a href="http://wiki.garrysmod.com/page/Structures/Color">Color structure</a>.
function draw_library.RoundedBox(...)
    if not CanRenderOnClient() then return end
    return draw.RoundedBox(...)
end

function draw_library.RoundedBoxEx(...)
    if not CanRenderOnClient() then return end
    return draw.RoundedBoxEx(...)
end

function draw_library.SimpleText(...)
    if not CanRenderOnClient() then return end
    return draw.SimpleText(...)
end

function draw_library.SimpleTextOutlined(...)
    if not CanRenderOnClient() then return end
    return draw.SimpleTextOutlined(...)
end

function draw_library.Text(...)
    if not CanRenderOnClient() then return end
    return draw.Text(...)
end

function draw_library.TextShadow(...)
    if not CanRenderOnClient() then return end
    return draw.TextShadow(...)
end

function draw_library.TexturedQuad(...)
    if not CanRenderOnClient() then return end
    return draw.TexturedQuad(...)
end

function draw_library.WordBox(...)
    if not CanRenderOnClient() then return end
    return draw.WordBox(...)
end