AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include( "shared.lua" )

include( "starfall/SFLib.lua" )
assert( SF, "Starfall didn't load correctly!" )

local context = SF.CreateContext()

function ENT:Initialize ()
	self.BaseClass.Initialize( self )
	self:SetUseType( 3 )
end

function ENT:Error ( msg, traceback )
    self.BaseClass.Error( self, msg, traceback )
end

function ENT:CodeSent (ply, files, mainfile)
	if ply ~= self.owner then return end
	local update = self.mainfile ~= nil

	self.files = files
	self.mainfile = mainfile
    self.client = true

	if update then
		net.Start( "starfall_update" )
			net.WriteEntity( self )
			for k,v in pairs( files ) do
				net.WriteBit( false )
				net.WriteString( k )
				net.WriteString( util.CRC( v ) )
			end
			net.WriteBit( true )
		net.Broadcast()
	end

	local ppdata = {}
	SF.Preprocessor.ParseDirectives( mainfile, files[ mainfile ], {}, ppdata )
	
	if ppdata.shared then	
        self.server = true
		local ok, instance = SF.Compiler.Compile( files, context, mainfile, ply, { entity = self } )
		if not ok then self:Error(instance) return end
		
		instance.runOnError = function ( inst, ... ) self:Error( ... ) end

		if self.instance then
			self.instance:deinitialize()
			self.instance = nil
		end

		self.instance = instance
		
		local ok, msg, traceback = instance:initialize()
		if not ok then
			self:Error( msg, traceback )
			return
		end
		
		if not self.instance then return end
		
		local _, _, _, a = self:GetColor()
		self:SetColor( Color( 255, 255, 255, a ) )
		self.sharedscreen = true
	end
end

-- Sends a net message to all clients about the use.
function ENT:Use ( activator )
	if activator:IsPlayer() then
		net.Start( "starfall_used" )
			net.WriteEntity( self )
			net.WriteEntity( activator )
		net.Broadcast()
	end
	if self.sharedscreen then
		self:runScriptHook( "starfallUsed", SF.Entities.Wrap( activator ) )
	end
end

function ENT:OnRemove ()
    self.BaseClass.OnRemove( self )
end

duplicator.RegisterEntityClass("starfall_screen", GAMEMODE.MakeEnt, "Data")