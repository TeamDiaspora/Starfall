﻿-------------------------------------------------------------------------------
-- Clientside Effects Library functions
-------------------------------------------------------------------------------
SF.CEffectData = {}

---CEffectData
--@client
local methods, metamethods = SF.Typedef("CEffectData",FindMetaTable("CEffectData"))
local wrap, unwrap = SF.CreateWrapper( metamethods, true, false, FindMetaTable("CEffectData"))

local vwrap, vunwrap = SF.Vectors.Wrap , SF.Vectors.Unwrap
local awrap, aunwrap = SF.Angles.Wrap  , SF.Angles.Unwrap
local ewrap, eunwrap = SF.Entities.Wrap  , SF.Entities.Unwrap

SF.CEffectData.Wrap = wrap
SF.CEffectData.Unwrap = unwrap
SF.CEffectData.Methods = methods
SF.CEffectData.Metamethods = metamethods

-- Get the util library
local util_lib = SF.Util.Library


--- Creates a new CEffectData object to be used with util.Effect. 
-- @name SF.DefaultEnvironment.EffectData
-- @return CEffectData The created CEffectData object.
function SF.DefaultEnvironment.EffectData()
    return wrap(EffectData())
end


---------------------------------------------------------------------------
----------------------ClientSide Util Functions----------------------------
---------------------------------------------------------------------------

--- Creates a effect with the specified data.
-- @name SF.DefaultEnvironment.util.Effect
-- @param effectName The name of the effect to create.
-- @param effectData The effect data describing the effect.
-- @param allowOverride If this is an engine-defined effect, this controls whether or not we can be overridden by Lua defined scripted effects with the same name.
function util_lib.Effect(effectName, effectData, allowOverride)
    if SF.CheckType(effectName,"string") and SF.CheckType(effectData,metamethods) and (allowOverride == nil or SF.CheckType(allowOverride,"boolean")) then
        util.Effect(effectName, unwrap(effectData), allowOverride)
    end
end

---------------------------------------------------------------------------
--------------------------CEffectData Functions----------------------------
---------------------------------------------------------------------------

--- Returns the angles of the effect. 
-- @return Angle The angles of the effect.
function methods:GetAngles()
    return awrap(unwrap(self):GetAngles())
end

--- Returns the attachment ID for the effect. 
-- @return Number The attachment ID of the effect.
function methods:GetAttachment()
    return unwrap(self):GetAttachment()
end

--- Returns byte which represents the color of the effect. 
-- @return Number The attachment ID of the effect.
function methods:GetAttachment()
    return unwrap(self):GetAttachment()
end

--- Returns byte which represents the color of the effect. 
-- @return number The color of the effect.
function methods:GetColor()
    return unwrap(self):GetColor()
end

--- Returns the damage type of the effect.
-- @return Damage type of the effect, see <a href="http://wiki.garrysmod.com/page/Enums/DMG">DMG_ Enums</a>
function methods:GetDamageType() 
    return unwrap(self):GetDamageType()
end

--- Returns the entity index of the entity set for the effect. 
-- @return Number The entity index of the entity set for the effect.
function methods:GetEntIndex() 
    return unwrap(self):GetEntIndex() 
end

--- Returns the entity assigned to the effect. 
-- @return Entity The entity assigned to the effect
function methods:GetEntity() 
    return ewrap(unwrap(self):GetEntity())
end

--- Returns the flags of the effect. 
-- @return Number The flags of the effect.
function methods:GetFlags() 
    return unwrap(self):GetFlags()
end

--- Returns the hit box ID of the effect. 
-- @return Number The hit box ID of the effect.
function methods:GetHitBox() 
    return unwrap(self):GetHitBox()
end

--- Returns the magnitude of the effect. 
-- @return Number The magnitude of the effect.
function methods:GetMagnitude()
    return unwrap(self):GetMagnitude() 
end

--- Returns the material ID of the effect. 
-- @return Number The material ID of the effect.
function methods:GetMaterialIndex() 
    return unwrap(self):GetMaterialIndex() 
end

--- Returns the normalized direction vector of the effect. 
-- @return Vector The normalized direction vector of the effect.
function methods:GetNormal() 
    return vwrap(unwrap(self):GetNormal())
end

--- Returns the origin position of the effect. 
-- @return Vector The origin position of the effect.
function methods:GetOrigin()
    return vwrap(unwrap(self):GetOrigin())
end

--- Returns the radius of the effect. 
-- @return Number The radius of the effect.
function methods:GetRadius()
    return unwrap(self):GetRadius() 
end

--- Returns the scale of the effect. 
-- @return Number The scale of the effect
function methods:GetScale()
    return unwrap(self):GetScale()
end

--- Returns the start position of the effect. 
-- @return Vector The start position of the effect.
function methods:GetStart()
    return vwrap(unwrap(self):GetStart())
end

--- Returns the surface property index of the effect. 
-- @return Number The surface property index of the effect
function methods:GetSurfaceProp()
    return unwrap(self):GetSurfaceProp() 
end

--- Sets the angles of the effect. 
-- @param and Angle The new angles to be set.
function methods:SetAngles(ang)
    unwrap(self):SetAngles(aunwrap(ang))
end

--- Sets the attachment id of the effect to be created with this effect data. 
-- NOTE: This is internally stored as an integer, but only the first 5 bits will be networked. 
-- @param attachment Number New attachment ID of the effect.
function methods:SetAttachment(attachment)
    unwrap(self):SetAttachment(attachment)
end

--- Sets the color of the effect. 
-- @param color Number Color represented by a byte.
function methods:SetColor(color)
    unwrap(self):SetAttachment(attachment)
end

--- Sets the damage type of the effect to be created with this effect data. 
-- @param damageType Number Damage type, see <a href="http://wiki.garrysmod.com/page/Enums/DMG">DMG_ Enums</a>.
function methods:SetDamageType(damageType)
    unwrap(self):SetDamageType(damageType)
end

--- Sets the entity of the effect via its index. 
-- @param entIndex Number The entity index to be set.
function methods:SetEntIndex(entIndex)
    unwrap(self):SetEntIndex(entIndex)
end

--- Sets the entity of the effect to be created with this effect data. 
-- @param entity Entity of the effect, mostly used for parenting.
function methods:SetEntity(entity)
    unwrap(self):SetEntity(eunwrap(entity))
end

--- Sets the flags of the effect. 
-- NOTE: This is internally stored as an integer, but only the first 8 bits will be networked. 
-- @param flags Number The flags of the effect.    PARTICLE_DISPATCH_FROM_ENTITY = 1   PARTICLE_DISPATCH_RESET_PARTICLES = 2
function methods:SetFlags( flags )
    unwrap(self):SetFlags( flags )
end

--- Sets the hit box index of the effect. 
-- NOTE: This is internally stored as an integer, but only the first 11 bits will be networked. 
-- @param hitBoxIndex Number The hit box index of the effect.
function methods:SetHitBox( hitBoxIndex )
    unwrap(self):SetHitBox( hitBoxIndex )
end

--- Sets the magnitude of the effect. 
-- @param magnitude Number The magnitude of the effect.
function methods:SetMagnitude(magnitude)
    unwrap(self):SetMagnitude(magnitude)
end

--- Sets the material index of the effect. 
-- NOTE: This is internally stored as an integer, but only the first 11 bits will be networked. 
-- @param materialIndex Number The material index of the effect.
function methods:SetMaterialIndex(materialIndex)
    unwrap(self):SetMaterialIndex(materialIndex)
end

--- Sets the normalized direction vector of the effect to be created with this effect data. 
-- @param normal Vector The normalized direction vector of the effect.
function methods:SetNormal(normal)
    unwrap(self):SetNormal(vunwrap(normal))
end

--- Sets the origin of the effect to be created with this effect data. 
-- @param origin Vector Origin of the effect.
function methods:SetOrigin(origin)
    unwrap(self):SetOrigin(vunwrap(origin))
end

--- Sets the radius of the effect to be created with this effect data.
-- NOTE: This is clamped internally from 0 to 1023. 
-- @param radius Number Radius of the effect.
function methods:SetRadius(radius)
    unwrap(self):SetRadius(radius)
end

--- Sets the scale of the effect to be created with this effect data. 
-- @param scale Number Scale of the effect.
function methods:SetScale(scale)
    unwrap(self):SetScale(scale)
end

--- Sets the start of the effect to be created with this effect data. 
-- @param start Vector Start of the effect.
function methods:SetStart(start)
    unwrap(self):SetStart(vunwrap(start))
end

--- Sets the surface property index of the effect. 
-- @param surfaceProperties Number The surface property index of the effect.
function methods:SetSurfaceProp(surfaceProperties)
    unwrap(self):SetSurfaceProp(surfaceProperties)
end
