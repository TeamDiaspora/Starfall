
/************************************************************
***************          Fehas aimAt extension          *****************
************************************************************/

/***********************************************************************************************************************
Credits


Myself - I am coding the stuff

adadr - He gave me the code to do this

KJIAD -  He supported me, by both making me actually work on this, and to understand stuff

Magos Mechanicus - He helped me with the final error that made it fail, as he noticed I called normvec local more then once.

Google - They helped me understand what it was doing, well enough to code this

gmod lua wiki - they got a list of functions you can use.
***********************************************************************************************************************/

//The function that check if you aim at target
local function Aim(Origin, OriginDirVec, Target, Acc)
	local delta  = wire_expression2_delta
	local NormVec = {0,0,0}
	local DirVec = Target-Origin
	
	//Length
	local Length = (DirVec[1] * DirVec[1] + DirVec[2] * DirVec[2] + DirVec[3] * DirVec[3]) ^ 0.5
	
	//Normalize
	if (Length > delta) then
		NormVec = { DirVec[1] / Length, DirVec[2] / Length, DirVec[3] / Length }
	else
		NormVec =  { 0, 0, 0 }
	end
	
	//Dot-product
	local Dot = NormVec[1] * OriginDirVec[1] + NormVec[2] * OriginDirVec[2] + NormVec[3] * OriginDirVec[3]
	
	//Check
	if (math.acos(Dot) < math.atan(Acc/Length)) then
		return 1
	else
		return 0
	end
end

e2function number aimAt(vector Origin, vector OriginDirVec, vector Target, number Accuracy)
	return Aim(Origin,OriginDirVec,Target,Accuracy)
end

/************************************************************
**************          Duneds setView extension          ***************
************************************************************/

function viewEntity(self, ply, active, pos, angle)
	local Ent = self.entity
	if ply:IsVehicle()  then
		Ent.Veh = ply
		ply = ply:GetDriver()
	end
	if not Ent.watching then
		Ent.watching = false
	end	
	if active == 1 and Ent.watching == false and ply:IsPlayer() then
		Ent.cam = ents.Create("prop_physics")
		
		//So nobody can blow up the cam
		Ent.cam.SC_Immune = true
		
		Ent.cam:SetModel("models/beer/wiremod/gate_e2.mdl" )
		Ent.cam:SetColor(255,255,255,0)
		Ent.cam:SetPos(Vector(pos[1], pos[2], pos[3]))
		Ent.cam:SetAngles(Angle(angle[1], angle[2], angle[3]))
		Ent.cam.Owner = ply
		Ent.cam:Spawn()
		Ent.cam:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
		local phys = Ent.cam:GetPhysicsObject()
		phys:Sleep()
		Ent.watching = true
		Ent.Ply = ply
		Ent.Ply:SetViewEntity(Ent.cam)
	end
	if Ent.watching == true and ply:IsPlayer() then
		Ent.cam:SetPos(Vector(pos[1], pos[2], pos[3]))
		Ent.cam:SetAngles(Angle(angle[1], angle[2], angle[3]))

	end
		
	if (active == 0 and Ent.watching == true) || (!ply:IsPlayer() and active == 1 and Ent.watching == true) then
		Ent.cam:Remove()
		Ent.Ply:SetViewEntity(Ent.Ply)
		Ent.watching = false
	end
end

e2function void entity:setView(number active, vector pos, angle angle)
	if this != nil and ((this:IsPlayer() and getOwner(self, self.entity) == this) || (this:IsVehicle() and isOwner(self, this))) then
		return viewEntity(self, this, active, pos, angle)
	end	
end

e2function entity getSetViewEnt()
	local Ent = self.entity
	if (Ent.cam and Ent.cam:IsValid()) then
		return Ent.cam
	end
end

registerCallback("destruct", function(self)
	local Ent = self.entity
	if (Ent.cam and Ent.cam:IsValid()) then
		Ent.Ply:SetViewEntity(Ent.Ply)
		Ent.watching = false
		Ent.cam:Remove()
	end
end)

/************************************************************
*************          Someones setEye extension          **************
************************************************************/

--AddCSLuaFile('SetEye.lua')

/********************************************************************************/
registerFunction("setDriverEye", "e:a", "", function(self, args)
    local op1, op2 = args[2], args[3]
    local rv1, rv2 = op1[1](self, op1), op2[1](self, op2)
    if(!rv1 or !IsValid(rv1) or !rv1:IsVehicle()) then return end
    if(!isOwner(self, rv1)) then return end
    local ply = rv1:GetDriver()
    if(!ply:IsValid()) then return end
    ply:SetEyeAngles( Angle(rv2[1],rv2[2],rv2[3]) )
end)

registerFunction("setEye", "a", "", function(self, args)
    local op1 = args[2]
    local rv1 = op1[1](self, op1)
    if(!rv1) then return end
    self.player:SetEyeAngles( Angle(rv1[1],rv1[2],rv1[3]) )
end)

/************************************************************
*******************   Brandon's entity crap     *************
************************************************************/
e2function void entity:setBouyancy(number amount)
	if IsValid(this) and isOwner(self, this) and amount then
		phys = this:GetPhysicsObject()
		if phys and IsValid(phys) then
			phys:SetBuoyancyRatio( amount )
			--phys:Wake()
		end
	end
end

local E2TABLE = {n = {}, s = {}, stypes = {}, ntypes = {}, size = 0, istable = true}
e2function table getModelAttachmentData(string model, number attachment)
	if model and model ~= "" then
		local PAD = list.Get( "SBEP_PartAssemblyData")
		local data = PAD[string.lower(model)]
		if data and data[attachment] then
			local tab = table.Copy(E2TABLE)
			local AttachmentData = {}
			AttachmentData.type = data[attachment].type
			AttachmentData.pos = data[attachment].pos
			AttachmentData.dir = data[attachment].dir

			tab.s = AttachmentData
			tab.stypes = {type = "s", pos = "v", dir = "a"}
			tab.size = 3
			  
			return tab
		end
	end
	return table.Copy(E2TABLE)
end

e2function number getModelAttachmentCount(string model)
	if model and model ~= "" then
		local PAD = list.Get( "SBEP_PartAssemblyData")
		local data = PAD[string.lower(model)]
		if data then
			return #data
		end
	end
	return 0
end

/************************************************************
*******************   DLBs Use extension     *********************
************************************************************/

local function Use(self,ent)
	if (!IsValid(ent) or !isOwner(self, ent)) then return 0 end
	ent:Use(self["player"])
	return 1
end

e2function number entity:use()
	return Use(self,this)
end

/************************************************************
*******************   someones onUse extension     *********************
************************************************************/

registerFunction("onUse","e:","n",function(self, args)
	local op1 = args[2]
	local rv1 = op1[1](self, op1)
	if(!validPhysics(rv1)) then return nil end 
		function Use(activator, caller)
		if activator then
			if activator:IsPlayer() then
				print("im a player")
				return 1
			else
				return 0
			end
		end
	end
end)


/************************************************************
***************   Hides the Outline/InfoBubble  ***************
************************************************************/


e2function void hide(number on)
	self.entity:SetNetworkedBool( "DoNotRenderWire", tobool(on) )
end


/************************************************************
***************  Steeveeo's Extra Extensions  ***************
************************************************************/

//Admin Noclip Control (or self)
e2function void entity:setNoClip(number toggle)
	if !this or !this:IsValid() then return end
	if !this:IsPlayer() then return end
	if this != self.player and !self.player:IsAdmin() then return end
	
	if toggle ~= 0 then
		this:SetMoveType(MOVETYPE_NOCLIP)
	else
		this:SetMoveType(MOVETYPE_WALK)
	end
end

//Sets the relative exit point of a vehicle, this would hook into
//what the Wire Exit Point already does, but that's all local to it,
//so I'm replicating it here. Shouldn't interfere too much, but you
//never know.
//Also, these are only relative, so there's that.
local E2ExitPoints = {}

e2function void entity:vehicleSetExitPoint(vector point)
	if !this or !this:IsValid() then return end
	if !this:IsVehicle() then return end
	if !isOwner(self, this) then return end
	
	E2ExitPoints[this] = Vector(point[1], point[2], point[3])
end

local function MovePlayer( ply, vehicle )
	if E2ExitPoints[vehicle] == nil then return end
	if E2ExitPoints[vehicle]:IsZero() then return end
	
	ply:SetPos(vehicle:LocalToWorld(E2ExitPoints[vehicle]) + Vector(0, 0, 5))
end
hook.Add("PlayerLeaveVehicle", "E2ExitPointFunc", MovePlayer )


//Get the compressor type of a given entity (if it is)
//--Adding this function back in, because it randomly went missing...
e2function string entity:rdGetCompressorType()
	if !this or !this:IsValid() then return "Not a Compressor!" end
	if this:GetClass() ~= "generator_gas" then return "Not a Compressor!" end
	
	if this.caf == nil then return "" end
	if this.custom == nil then return "" end
	if this.custom.resource == nil then return "" end
	
	return this.caf.custom.resource
end

//Get an entity's attachment names. For some reason Wire doesn't include this
//by default.
e2function array entity:getAttachmentNames()
	if !this or !this:IsValid() then return {} end
	
	local out = {}
	
	for k, v in pairs(this:GetAttachments()) do
		table.insert(out, v["name"])
	end
	
	return out
end

//Toggle and Entity's Drag
e2function void entity:propEnableDrag(number enable)
	if !this or !this:IsValid() then return end
	if !isOwner(self, this) then return end
	
	local phys = this:GetPhysicsObject()
	if phys:IsValid() then
		phys:EnableDrag(enable != 0)
	end
end

//Set an NPC or Ragdoll's Eye Angles (Code lifted from EyePoser tool)
//Y U NO WORK ON HOLOS >:(
local function ConvertRelativeToEyesAttachment( Entity, Pos )

	if ( Entity:IsNPC() ) then --or Entity:GetClass() == "gmod_wire_hologram" ) then
		return Pos
	end

	-- Convert relative to eye attachment
	local eyeattachment = Entity:LookupAttachment( "eyes" )
	if ( eyeattachment == 0 ) then return end
	local attachment = Entity:GetAttachment( eyeattachment )
	if ( !attachment ) then return end
	
	local LocalPos, LocalAng = WorldToLocal( Pos, Angle(0,0,0), attachment.Pos, attachment.Ang )
	
	return LocalPos

end
e2function void entity:setEyeTarget(vector pos)
	//Sanity Checks
	if not this:IsValid() then return end
	
	local localPos = ConvertRelativeToEyesAttachment( this, Vector(pos[1], pos[2], pos[3]) )
	if (!localPos) then return end
	
	this:SetEyeTarget( localPos )
end

//Check if player has GodMode (ASSMod)
e2function number entity:hasGod()
	
	if !IsValid(this) then return 0 end
	if !this:IsPlayer() then return 0 end
	
	if this.ASSHasGod then
		return 1
	else
		return 0
	end
	
end

//Set FOV
e2function void entity:setFOV(number FOV, number Time)

	//Clamp values to be reasonable
	FOV = math.Clamp(FOV, 0, 180)

	if IsValid(this) then
		if this:IsPlayer() and this == self.player then
			this:SetFOV(FOV,Time)
		elseif this:IsVehicle() and isOwner(self, this) then
			local Driver = this:GetDriver()
			
			if !IsValid(Driver) then return end
			
			Driver:SetFOV(FOV,Time)
			
			//Add to the FOV reset list
			if !self.FOVSet then self.FOVSet = {} end
			self.FOVSet[this:GetDriver():Nick()] = this:GetDriver()
			
			//Did we already start the check timer?
			if !timer.IsTimer("E2_FOV_RESET_CHECK" .. self.entity:EntIndex()) then
				timer.Create("E2_FOV_RESET_CHECK" .. self.entity:EntIndex(),1,0,function(self)
					//Check if anyone has gotten up from their seat
					for k,v in pairs(self.FOVSet) do
						if !IsValid(v:GetVehicle()) then
							v:SetFOV(0,0)
							self.FOVSet[v:Nick()] = nil
						end
					end
				
				end,self)
			end
		end
		return
	end	
end

registerCallback("destruct", function(self)
	local Ent = self.entity
	local Ply = self.player
	
	//Return owner view to normal
	Ply:SetFOV(0,0)
	
	//Stop looping check timer
	timer.Destroy("E2_FOV_RESET_CHECK" .. self.entity:EntIndex())
end)


/************************************************************
***************  Old McBuilds SetPosition, missing parts recoded by Nielk1, rank check added by Nielk1  ***************
************************************************************/

function Jump(self,v,pos,this)
	if !v:IsPlayer() then
		local phys = v:GetPhysicsObject()
		if(!phys:IsMoveable())then
			phys:EnableMotion(true)
			phys:Wake()
			phys:SetPos(Vector(pos[1],pos[2],pos[3]) + (phys:GetPos() - this:GetPos()))
			phys:EnableMotion(false)
		else
			phys:SetPos(Vector(pos[1],pos[2],pos[3]) + (phys:GetPos() - this:GetPos()))
		end
	else
		local mtype = v:GetMoveType()
		v:SetMoveType(MOVETYPE_NOCLIP)
		v:SetPos(Vector(pos[1],pos[2],pos[3]))
		v:SetMoveType(mtype)
	end
end

function JumpPly(self,pos,this)
	local mtype = this:GetMoveType()
	this:SetMoveType(MOVETYPE_NOCLIP)
	this:SetPos(Vector(pos[1],pos[2],pos[3]))
	this:SetMoveType(mtype)
end

e2function void entity:setPosition(vector pos)        
	if this and IsValid(this) and pos and IsValid(self.player) then
		if !this:IsPlayer() and isOwner(self, this) and validPhysics(this) then
			local tbl = constraint.GetAllConstrainedEntities(this)
			for k,v in pairs(tbl) do
				Jump(self,v,pos,this)
			end
		else
			/* Rewriting for Perms
			if ((this == self) or (this:IsPlayer())) and (self.player:IsAdmin() or self.player:IsSuperAdmin() or self.player:HasLevel(4)) then //For players
				JumpPly(self,pos,this)
			else
				self.player:ChatPrint("The player teleportation function of setPosition is restricted to respected players")
			end
			*/
			
			//Get Permissions Level
			local owner = self.player
			local permLevel = getE2PermissionLevel(owner, self)
			if permLevel == 0 then return end
			
			//Player Movement
			if this:IsPlayer() then
				//Normal Players - Can only move self and people on their PP list
				if permLevel == 1 then
					if this == owner or NADMOD.Users[this:SteamID()].Friends[owner:SteamID()] then
						JumpPly(self, pos, this)
					else
						self.player:ChatPrint("[setPosition] - You can only move other players if you are Respected or higher!")
					end
					
				//Respected+ - Anyone
				elseif permLevel > 1 then
					JumpPly(self, pos, this)
					
				//Default - Warn and don't move
				else
					self.player:ChatPrint("[setPosition] - You can only move other players if you are Respected or higher!")
				end
			end
		end
	end
end


e2function number entity:getUniqueID()
	if !this:IsPlayer() then return end

	local ID = this:UniqueID()
	return ID
end